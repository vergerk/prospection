﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Prospection
{
    public partial class FormFontDialogPdf : Form
    {
        private FontFamily font = new FontFamily("Arial");
        private FontPropertiesPdf fontExample;
        private Font returnedFont = null;
        private const float _size = 16;
        private FontStyle fs;

        private bool loadedFont = false;

        private bool font_search = false;
        private bool size_search = false;
        private bool replaceRegular = true;
        private bool regularIsPresent = false;

        private Color backgroundColorExample = Color.White;
        private Color foregroundColorExample = Color.Black;

        #region Accesseurs

        public Font ReturnedFont
        {
            get { return returnedFont; }
            set { returnedFont = value; }
        }

        public Color BackgroundColorExample
        {
            get { return backgroundColorExample; }
            set { backgroundColorExample = value; }
        }

        public Color ForegroundColorExample
        {
            get { return foregroundColorExample; }
            set { foregroundColorExample = value; }
        }

        #endregion

        public FormFontDialogPdf(FontPropertiesPdf fontExample, Color? backColor = null , Color? foreColor = null, bool colorBtnEnabled = false)
        {
            InitializeComponent();

            loadedFont = false;

            lb_exampleFont.Font = fontExample.GetFont();
            lb_exampleSize.Font = fontExample.GetFont(_size);

            ckb_bold.Checked = fontExample.IsBold;
            ckb_italic.Checked = fontExample.IsItalic;
            ckb_underline.Checked = fontExample.IsUnderline;

            this.fontExample = fontExample;

            if (colorBtnEnabled)
            {
                lb_exampleFont.BackColor = (Color)backColor;
                lb_exampleFont.ForeColor = (Color)foreColor;

                lb_exampleSize.BackColor = (Color)backColor;
                lb_exampleSize.ForeColor = (Color)foreColor;

                btn_fontColors.Enabled = colorBtnEnabled;

                btn_fontColors.BackColor = Color.RoyalBlue;
                btn_fontColors.ForeColor = Color.White;
            }

            lbx_sizes.Items.AddRange(new object[] { 8, 9, 10, 11, 12, 14, 16, 18, 20, 22, 24, 26, 28 });
            try
            {
                lbx_sizes.SelectedItem = (int)Math.Round(fontExample.Size, 0);
            }
            catch
            {
                lbx_sizes.SelectedIndex = 0;
            }

            int familyCount = FontFamily.Families.Count();
            for (int i = 0; i < familyCount; i++ )
            {
                FontFamily f = FontFamily.Families[i];

                // Migradoc ne fonctionne qu'avec les styles "Bold", "Italic", "Underline" et "Regular"
                if (
                    f.IsStyleAvailable(FontStyle.Bold) ||
                    f.IsStyleAvailable(FontStyle.Italic) ||
                    f.IsStyleAvailable(FontStyle.Underline) ||
                    f.IsStyleAvailable(FontStyle.Regular)
                    )
                {
                    lbx_fonts.Items.Add(f.Name);

                    if (FontFamily.Families[i].Name == fontExample.Name)
                    {
                        lbx_fonts.SelectedIndex = i;
                    }
                }
            }
        }

        #region Fonctions

        /// <summary>
        /// Define colours of header cell
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_fontColors_Click(object sender, EventArgs e)
        {
            if (colorDialog.ShowDialog() == DialogResult.OK)
            {
                this.backgroundColorExample = colorDialog.Color;
                this.foregroundColorExample = Commons.ForeColorFromBackColor(backgroundColorExample);

                lb_exampleFont.BackColor = backgroundColorExample;
                lb_exampleSize.BackColor = backgroundColorExample;

                lb_exampleFont.ForeColor = foregroundColorExample;
                lb_exampleSize.ForeColor = foregroundColorExample;
            }
        }

        #endregion

        #region Events

        /// <summary>
        /// Refreshs fontstyle when click on font
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void lbx_fonts_SelectedIndexChanged(object sender, EventArgs e)
        {
            string fontName = lbx_fonts.Text;
            ckb_bold.Enabled = false;
            ckb_italic.Enabled = false;
            ckb_underline.Enabled = false;
            FontStyle fs = FontStyle.Regular;

            replaceRegular = true;
            regularIsPresent = false;

            // remise à l'état inital des contrôles
            foreach (Control c in this.Controls)
            {
                if (c.GetType().Name == "CheckBox")
                {
                    ((CheckBox)c).BackColor = SystemColors.Control;
                    ((CheckBox)c).ForeColor = SystemColors.ControlText;

                    if (loadedFont)
                    {
                        ((CheckBox)c).Checked = false;
                    }
                }
            }

            if (((ListBox)sender).SelectedItem != null)
            {
                // on récupère la police cliquée d'après son nom
                foreach (FontFamily fontFam in FontFamily.Families)
                {
                    if (fontName == fontFam.Name)
                    {
                        font = fontFam;
                        break;
                    }
                }

                // on regarde si la police supporte le style "Regular"
                if (font.IsStyleAvailable(FontStyle.Regular))
                {
                    replaceRegular = false;
                    regularIsPresent = true;
                }

                // on recherche les styles supportés
                foreach (FontStyle style in Enum.GetValues(typeof(FontStyle)))
                {
                    if (
                        (font.IsStyleAvailable(style) && (
                            (style == FontStyle.Bold) ||
                            (style == FontStyle.Italic) ||
                            (style == FontStyle.Underline)
                        ))
                        )
                    {
                        switch (style)
                        {
                            case FontStyle.Bold:
                                ckb_bold.Enabled = true;
                                break;
                            case FontStyle.Italic:
                                ckb_italic.Enabled = true;
                                break;
                            case FontStyle.Underline:
                                ckb_underline.Enabled = true;
                                break;
                        }

                        if (replaceRegular)
                        {
                            fs = replaceRegularStyle(style);
                        }
                    }
                }
            }

            if (!loadedFont)
            {
                fs = this.fontExample.GetFont().Style;
                loadedFont = true;
            }

            lb_exampleFont.Font = new Font(fontName, _size, fs);
            lb_exampleSize.Font = new Font(fontName, (float)Convert.ToDouble(lbx_sizes.SelectedItem), fs);
        }

        /// <summary>
        /// Check if fontstyle Regular exist and find a substitute
        /// </summary>
        /// <param name="replaceFS"></param>
        private FontStyle replaceRegularStyle(FontStyle replaceFS)
        {
            this.fs = FontStyle.Regular;

            switch (replaceFS)
            {
                case FontStyle.Bold:
                    this.fs = FontStyle.Bold;
                    ckb_bold.Checked = true;
                    replaceRegular = false;
                    break;
                case FontStyle.Italic:
                    this.fs = FontStyle.Italic;
                    ckb_italic.Checked = true;
                    replaceRegular = false;
                    break;
                case FontStyle.Underline:
                    this.fs = FontStyle.Italic;
                    ckb_underline.Checked = true;
                    replaceRegular = false;
                    break;
            }

            return this.fs;
        }
        /// <summary>
        /// Refreshs textbox and examples
        /// </summary>
        private void whenSelectedIndexChanged()
        {
            int selectionStartFont = txt_font.SelectionStart;
            int selectionStartSize = txt_size.SelectionStart;

            if (!font_search)
            {
                txt_font.Text = lbx_fonts.SelectedItem.ToString();
            }

            if (!size_search)
            {
                txt_size.Text = lbx_sizes.SelectedItem.ToString();
            }

            txt_font.SelectionStart = selectionStartFont;
            txt_size.SelectionStart = selectionStartSize;

            //this.size = (float)Convert.ToDouble(lbx_sizes.SelectedItem);
            lb_exampleFont.Font = new Font(font, _size, this.fs);
            lb_exampleSize.Font = new Font(font, lb_exampleSize.Font.Size, this.fs);
        }

        /// <summary>
        /// Refreshs example label size 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void lbx_sizes_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (lbx_sizes.SelectedItem != null)
            {
                float size = (float)Convert.ToDouble(lbx_sizes.SelectedItem);
                lb_exampleSize.Font = new Font(font, size, lb_exampleFont.Font.Style);
            }
        }

        /// <summary>
        /// Get focus on listbox hover
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void lbx_listBox_MouseHover(object sender, EventArgs e)
        {
            ListBox s = (ListBox)sender;

            s.Focus();
        }

        /// <summary>
        /// Search in listbox
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txt_refreshListBox_KeyUp(object sender, KeyEventArgs e)
        {
            TextBox t = (TextBox)sender;

            switch (t.Name)
            {
                case "txt_font":
                    lbx_fonts.SelectedIndex = IndexOf(lbx_fonts, t.Text);
                    break;
                case "txt_size":
                    lbx_sizes.SelectedIndex = IndexOf(lbx_sizes, t.Text);
                    break;
            }
        }

        /// <summary>
        /// Deny to refreshs textBox
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txt_refreshListBox_Enter(object sender, EventArgs e)
        {
            TextBox t = (TextBox)sender;

            switch (t.Name)
            {
                case "txt_font":
                    font_search = true;
                    break;
                case "txt_size":
                    size_search = true;
                    break;
            }
        }

        /// <summary>
        /// Allow to refreshs textBox
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txt_refreshListBox_Leave(object sender, EventArgs e)
        {
            TextBox t = (TextBox)sender;

            switch (t.Name)
            {
                case "txt_font":
                    font_search = false;
                    break;
                case "txt_size":
                    size_search = false;
                    break;
            }
        }

        /// <summary>
        /// Search item in listbox (lower characters)
        /// </summary>
        /// <param name="listBox"></param>
        /// <param name="itemSearched"></param>
        /// <returns></returns>
        private int IndexOf(ListBox listBox, string itemSearched)
        {
            int r = -1;

            for (int i = 0; i < listBox.Items.Count; i++)
            {
                if (listBox.Items[i].ToString().ToLower() == itemSearched.ToLower())
                {
                    r = i;
                    break;
                }
            }

            return r;
        }

        /// <summary>
        /// Final font
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_validFont_Click(object sender, EventArgs e)
        {
            bool isChecked = false;

            if (!regularIsPresent)
            {
                if (
                    !ckb_bold.Checked &&
                    !ckb_italic.Checked &&
                    !ckb_underline.Checked
                    )
                {
                    MessageBox.Show(
                        "La police de caractère " + lbx_fonts.SelectedItem + " nécessite un style. Indiquez votre choix parmi les styles disponibles.",
                        "",
                        MessageBoxButtons.OK,
                        MessageBoxIcon.Exclamation
                        );

                    foreach (Control c in this.Controls)
                    {
                        if (
                            c.GetType().Name == "CheckBox" &&
                            c.Enabled
                            )
                        {
                            ((CheckBox)c).BackColor = Color.Red;
                            ((CheckBox)c).ForeColor = Color.White;
                        }
                    }
                }
                else
                {
                    isChecked = true;
                }
            }

            if (regularIsPresent || isChecked)
            {
                this.Close();
                this.DialogResult = DialogResult.OK;
                returnedFont = lb_exampleSize.Font;
                backgroundColorExample = lb_exampleFont.BackColor;
                foregroundColorExample = lb_exampleFont.ForeColor;
            }
        }

        /// <summary>
        /// Refreshs examples - support multiples fontstyles
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ckb_fontStyle_Click(object sender, EventArgs e)
        {
            string name = lb_exampleFont.Font.Name;
            float size = lb_exampleSize.Font.Size;
            Font exampleFont = new Font(name, _size, lb_exampleFont.Font.Style);
            Font exampleSize = new Font(name, size, lb_exampleFont.Font.Style);

            if (ckb_bold.Checked)
            {
                exampleFont = new Font(name, _size, exampleFont.Style | FontStyle.Bold);
                exampleSize = new Font(name, size, exampleSize.Style | FontStyle.Bold);
            }
            else
            {
                exampleFont = new Font(name, _size, exampleFont.Style & ~FontStyle.Bold);
                exampleSize = new Font(name, size, exampleSize.Style & ~FontStyle.Bold);
            }

            if (ckb_italic.Checked)
            {
                exampleFont = new Font(name, _size, exampleFont.Style | FontStyle.Italic);
                exampleSize = new Font(name, size, exampleSize.Style | FontStyle.Italic);
            }
            else
            {
                exampleFont = new Font(name, _size, exampleFont.Style & ~FontStyle.Italic);
                exampleSize = new Font(name, size, exampleSize.Style & ~FontStyle.Italic);
            }

            if (ckb_underline.Checked)
            {
                exampleFont = new Font(name, _size, exampleFont.Style | FontStyle.Underline);
                exampleSize = new Font(name, size, exampleSize.Style | FontStyle.Underline);
            }
            else
            {
                exampleFont = new Font(name, _size, exampleFont.Style & ~FontStyle.Underline);
                exampleSize = new Font(name, size, exampleSize.Style & ~FontStyle.Underline);
            }

            lb_exampleFont.Font = exampleFont;
            lb_exampleSize.Font = exampleSize;
        }

        #endregion
    }
}
