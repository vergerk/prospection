﻿namespace Prospection
{
    partial class FormOptions
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.grpb_pdf = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txt_author = new System.Windows.Forms.TextBox();
            this.btn_saveClose = new System.Windows.Forms.Button();
            this.btn_cancel = new System.Windows.Forms.Button();
            this.grpb_partners = new System.Windows.Forms.GroupBox();
            this.lb_example_phoneSep = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txt_phoneSep = new System.Windows.Forms.TextBox();
            this.grpb_save = new System.Windows.Forms.GroupBox();
            this.cb_saveMethod = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.grpb_pdf.SuspendLayout();
            this.grpb_partners.SuspendLayout();
            this.grpb_save.SuspendLayout();
            this.SuspendLayout();
            // 
            // grpb_pdf
            // 
            this.grpb_pdf.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grpb_pdf.Controls.Add(this.label1);
            this.grpb_pdf.Controls.Add(this.txt_author);
            this.grpb_pdf.Location = new System.Drawing.Point(12, 81);
            this.grpb_pdf.Name = "grpb_pdf";
            this.grpb_pdf.Size = new System.Drawing.Size(509, 63);
            this.grpb_pdf.TabIndex = 0;
            this.grpb_pdf.TabStop = false;
            this.grpb_pdf.Text = "PDF";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 25);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(38, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Auteur";
            // 
            // txt_author
            // 
            this.txt_author.Location = new System.Drawing.Point(50, 22);
            this.txt_author.Name = "txt_author";
            this.txt_author.Size = new System.Drawing.Size(143, 20);
            this.txt_author.TabIndex = 0;
            // 
            // btn_saveClose
            // 
            this.btn_saveClose.Location = new System.Drawing.Point(446, 361);
            this.btn_saveClose.Name = "btn_saveClose";
            this.btn_saveClose.Size = new System.Drawing.Size(75, 23);
            this.btn_saveClose.TabIndex = 1;
            this.btn_saveClose.Text = "OK";
            this.btn_saveClose.UseVisualStyleBackColor = true;
            this.btn_saveClose.Click += new System.EventHandler(this.btn_saveClose_Click);
            // 
            // btn_cancel
            // 
            this.btn_cancel.Location = new System.Drawing.Point(365, 361);
            this.btn_cancel.Name = "btn_cancel";
            this.btn_cancel.Size = new System.Drawing.Size(75, 23);
            this.btn_cancel.TabIndex = 2;
            this.btn_cancel.Text = "Annuler";
            this.btn_cancel.UseVisualStyleBackColor = true;
            this.btn_cancel.Click += new System.EventHandler(this.btn_cancel_Click);
            // 
            // grpb_partners
            // 
            this.grpb_partners.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grpb_partners.Controls.Add(this.lb_example_phoneSep);
            this.grpb_partners.Controls.Add(this.label2);
            this.grpb_partners.Controls.Add(this.txt_phoneSep);
            this.grpb_partners.Location = new System.Drawing.Point(12, 12);
            this.grpb_partners.Name = "grpb_partners";
            this.grpb_partners.Size = new System.Drawing.Size(509, 63);
            this.grpb_partners.TabIndex = 3;
            this.grpb_partners.TabStop = false;
            this.grpb_partners.Text = "Partenaires";
            // 
            // lb_example_phoneSep
            // 
            this.lb_example_phoneSep.AutoSize = true;
            this.lb_example_phoneSep.Location = new System.Drawing.Point(218, 25);
            this.lb_example_phoneSep.Name = "lb_example_phoneSep";
            this.lb_example_phoneSep.Size = new System.Drawing.Size(220, 13);
            this.lb_example_phoneSep.TabIndex = 2;
            this.lb_example_phoneSep.Text = "exemple : 09 01 02 03 04 - +339 01 02 03 04";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 25);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(165, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Téléphone - caractère séparateur";
            // 
            // txt_phoneSep
            // 
            this.txt_phoneSep.Location = new System.Drawing.Point(177, 22);
            this.txt_phoneSep.MaxLength = 1;
            this.txt_phoneSep.Name = "txt_phoneSep";
            this.txt_phoneSep.Size = new System.Drawing.Size(35, 20);
            this.txt_phoneSep.TabIndex = 0;
            this.txt_phoneSep.Text = " ";
            this.txt_phoneSep.TextChanged += new System.EventHandler(this.txt_phoneSep_TextChanged);
            // 
            // grpb_save
            // 
            this.grpb_save.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grpb_save.Controls.Add(this.cb_saveMethod);
            this.grpb_save.Controls.Add(this.label3);
            this.grpb_save.Location = new System.Drawing.Point(12, 150);
            this.grpb_save.Name = "grpb_save";
            this.grpb_save.Size = new System.Drawing.Size(509, 63);
            this.grpb_save.TabIndex = 4;
            this.grpb_save.TabStop = false;
            this.grpb_save.Text = "Enregistrement";
            // 
            // cb_saveMethod
            // 
            this.cb_saveMethod.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cb_saveMethod.Enabled = false;
            this.cb_saveMethod.FormattingEnabled = true;
            this.cb_saveMethod.Items.AddRange(new object[] {
            "XML",
            "MySQL"});
            this.cb_saveMethod.Location = new System.Drawing.Point(115, 22);
            this.cb_saveMethod.Name = "cb_saveMethod";
            this.cb_saveMethod.Size = new System.Drawing.Size(121, 21);
            this.cb_saveMethod.TabIndex = 2;
            this.cb_saveMethod.SelectedIndexChanged += new System.EventHandler(this.cb_saveMethod_SelectedIndexChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 25);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(103, 13);
            this.label3.TabIndex = 1;
            this.label3.Text = "Format des données";
            // 
            // FormOptions
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(533, 396);
            this.Controls.Add(this.grpb_save);
            this.Controls.Add(this.grpb_partners);
            this.Controls.Add(this.btn_cancel);
            this.Controls.Add(this.btn_saveClose);
            this.Controls.Add(this.grpb_pdf);
            this.Name = "FormOptions";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.Text = "Options";
            this.Load += new System.EventHandler(this.FormOptions_Load);
            this.grpb_pdf.ResumeLayout(false);
            this.grpb_pdf.PerformLayout();
            this.grpb_partners.ResumeLayout(false);
            this.grpb_partners.PerformLayout();
            this.grpb_save.ResumeLayout(false);
            this.grpb_save.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox grpb_pdf;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txt_author;
        private System.Windows.Forms.Button btn_saveClose;
        private System.Windows.Forms.Button btn_cancel;
        private System.Windows.Forms.GroupBox grpb_partners;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txt_phoneSep;
        private System.Windows.Forms.Label lb_example_phoneSep;
        private System.Windows.Forms.GroupBox grpb_save;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox cb_saveMethod;
    }
}