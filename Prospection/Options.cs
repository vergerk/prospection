﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Prospection
{
    public class Options
    {
        //Dictionary<string, Color> partnerType = new Dictionary<string, Color>();; -> pas possible de serialiser
        //private List<PartnerType> listPartnersTypes = new List<PartnerType>();

        //private List<DictCheckedTypes> listControlPartnersSort = new List<DictCheckedTypes>();
        private List<string> recentsFiles = new List<string>();

        private PdfProperties pdfPropert = new PdfProperties();

        private char phoneSep = ' ';
        private string placeHolder_searchBar = "Que souhaitez-vous chercher ?";

        private string dgv_orderBy = "";
        private static string extensionFile = "ogepart";

        private SortOrder dgv_orderByASC = SortOrder.Ascending;

        private string author = "";

        private string dataFormat = "XML";

        private const int maxRecentsFiles = 5;

        #region Accesseurs

        public char PhoneSep
        {
            get { return phoneSep; }
            set { phoneSep = value; }
        }

        public string PlaceHolder_searchBar
        {
            get { return placeHolder_searchBar; }
        }

        public string Author
        {
            get { return author; }
            set { author = value; }
        }

        public PdfProperties PdfPropert
        {
            get { return pdfPropert; }
            set { pdfPropert = value; }
        }

        public string Dgv_orderBy
        {
            get { return dgv_orderBy; }
            set { dgv_orderBy = value; }
        }

        public SortOrder Dgv_orderByASC
        {
            get { return dgv_orderByASC; }
            set { dgv_orderByASC = value; }
        }

        public static string ExtensionFile
        {
            get { return extensionFile; }
        }

        public string DataFormat
        {
            get { return dataFormat; }
            set { dataFormat = value; }
        }

        public List<string> RecentsFiles
        {
            get { return recentsFiles; }
            set { recentsFiles = value; }
        }

        public static int MaxRecentsFiles
        {
            get { return Options.maxRecentsFiles; }
        } 

        #endregion

        public Options() { }

        #region Méthodes

        /// <summary>
        /// Check if a partnerType exist in combobox
        /// </summary>
        /// <param name="fullname"></param>
        /// <returns></returns>
        public bool CheckComboBoxPT(string fullname, ToolStripMenuItem tsm)
        {
            bool r = false;

            for (int i = 0; i < tsm.DropDownItems.Count; i++)
            {
                if (tsm.DropDownItems[i].Name == "tsm_" + fullname)
                {
                    r = true;
                    break;
                }
            }

            return r;
        }

        #endregion
    }
}
