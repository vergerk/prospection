﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prospection
{
    public class FontPropertiesPdf
    {
        private string name = "";
        private float size = 0;

        private bool isBold = false;
        private bool isItalic = false;
        private bool isUnderline = false;

        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        public float Size
        {
            get { return size; }
            set { size = value; }
        }

        public bool IsUnderline
        {
            get { return isUnderline; }
            set { isUnderline = value; }
        }

        public bool IsItalic
        {
            get { return isItalic; }
            set { isItalic = value; }
        }

        public bool IsBold
        {
            get { return isBold; }
            set { isBold = value; }
        }

        public FontPropertiesPdf()
        {
            this.name = "Arial";
            this.size = 14;

            this.isBold = false;
            this.isItalic = false;
            this.isUnderline = false;
        }

        public FontPropertiesPdf(string fontName, float fontSize)
        {
            this.name = fontName;
            this.size = fontSize;

            this.isBold = false;
            this.isItalic = false;
            this.isUnderline = false;
        }

        public FontPropertiesPdf(string fontName, float fontSize, bool isBold, bool isItalic, bool isUnderline)
        {
            this.name = fontName;
            this.size = fontSize;

            this.isBold = isBold;
            this.isItalic = isItalic;
            this.isUnderline = isUnderline;
        }

        /// <summary>
        /// Set fontstyle (1 and more)
        /// </summary>
        /// <param name="fontName"></param>
        /// <param name="fontSize"></param>
        /// <param name="sp"></param>
        /// <returns></returns>
        public Font GetFont(float sizeUser = -1)
        {
            float sizeFont = size;

            if (sizeUser != -1)
            {
                sizeFont = sizeUser;
            }

            Font f = new Font(name, sizeFont);

            if (isBold)
            {
                f = new Font(name, sizeFont, f.Style | FontStyle.Bold);
            }

            if (isItalic)
            {
                f = new Font(name, sizeFont, f.Style | FontStyle.Italic);
            }

            if (isUnderline)
            {
                f = new Font(name, sizeFont, f.Style | FontStyle.Underline);
            }

            return f;
        }
    }
}
