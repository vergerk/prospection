﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Prospection
{
    public partial class FormOptions : Form
    {
        FormList refInstance;

        public FormOptions(FormList r)
        {
            InitializeComponent();

            this.refInstance = r;
        }

        private void FormOptions_Load(object sender, EventArgs e)
        {
            txt_author.Text = refInstance.opt.Author;
            txt_phoneSep.Text = refInstance.opt.PhoneSep.ToString();

            cb_saveMethod.SelectedItem = "XML";
        }

        #region Events

        /// <summary>
        /// When phone separator is changed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txt_phoneSep_TextChanged(object sender, EventArgs e)
        {
            lb_example_phoneSep.Text = Commons.FilterTel(lb_example_phoneSep.Text, Convert.ToChar(txt_phoneSep.Text));
        }

        /// <summary>
        /// List of data format
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cb_saveMethod_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cb_saveMethod.SelectedItem.ToString() == "MySQL")
            {
                MessageBox.Show("Fonction non-implémentée pour l'instant.");
                cb_saveMethod.SelectedItem = "XML";
            }
        }

        /// <summary>
        /// Cancel and close window
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_cancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        /// <summary>
        /// Save options and close windows
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_saveClose_Click(object sender, EventArgs e)
        {
            refInstance.opt.Author = txt_author.Text.Trim();
            refInstance.opt.PhoneSep = Convert.ToChar(txt_phoneSep.Text);
            refInstance.opt.DataFormat = cb_saveMethod.SelectedItem.ToString();

            Close();
        }

        #endregion
    }
}
