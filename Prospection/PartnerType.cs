﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prospection
{
    public class PartnerType
    {
        private string name;
        private int backcolor;
        private int forecolor;

        #region Accesseurs

        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        public int Backcolor
        {
            get { return backcolor; }
            set { backcolor = value; }
        }

        public int Forecolor
        {
            get { return forecolor; }
            set { forecolor = value; }
        }

        #endregion

        public PartnerType()
        {
            name = "";
            backcolor = Color.White.ToArgb();
            forecolor = Color.Black.ToArgb();
        }

        public PartnerType(string n, int back, int fore)
        {
            name = n;
            backcolor = back;
            forecolor = fore;
        }
    }
}
