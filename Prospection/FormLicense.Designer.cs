﻿namespace Prospection
{
    partial class FormLicense
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.rtb_license = new System.Windows.Forms.RichTextBox();
            this.SuspendLayout();
            // 
            // rtb_license
            // 
            this.rtb_license.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rtb_license.Location = new System.Drawing.Point(0, 0);
            this.rtb_license.Name = "rtb_license";
            this.rtb_license.ReadOnly = true;
            this.rtb_license.Size = new System.Drawing.Size(501, 412);
            this.rtb_license.TabIndex = 0;
            this.rtb_license.Text = "";
            // 
            // FormLicense
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(501, 412);
            this.Controls.Add(this.rtb_license);
            this.MinimizeBox = false;
            this.Name = "FormLicense";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.Text = "Licence";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.RichTextBox rtb_license;
    }
}