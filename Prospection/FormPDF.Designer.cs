﻿namespace Prospection
{
    partial class FormPDF
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.saveFileDialog = new System.Windows.Forms.SaveFileDialog();
            this.openFileDialogPdf = new System.Windows.Forms.OpenFileDialog();
            this.tc_genPDF = new System.Windows.Forms.TabControl();
            this.tp_dataSelection = new System.Windows.Forms.TabPage();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txt_search = new System.Windows.Forms.TextBox();
            this.ckb_showPrivateData = new System.Windows.Forms.CheckBox();
            this.txt_PdfBeforeList = new System.Windows.Forms.TextBox();
            this.ckb_showPublicData = new System.Windows.Forms.CheckBox();
            this.clb_partnersTypes = new System.Windows.Forms.CheckedListBox();
            this.btn_addPdfBeforeList = new System.Windows.Forms.Button();
            this.txt_PdfAfterList = new System.Windows.Forms.TextBox();
            this.btn_addPdfAfterList = new System.Windows.Forms.Button();
            this.tp_page = new System.Windows.Forms.TabPage();
            this.txt_picturePath = new System.Windows.Forms.TextBox();
            this.btn_browsePicture = new System.Windows.Forms.Button();
            this.lb_picturePath = new System.Windows.Forms.Label();
            this.pb_picturePdf = new System.Windows.Forms.PictureBox();
            this.ckb_horizontalCent = new System.Windows.Forms.CheckBox();
            this.num_bottomMargin = new System.Windows.Forms.NumericUpDown();
            this.num_rightMargin = new System.Windows.Forms.NumericUpDown();
            this.num_topMargin = new System.Windows.Forms.NumericUpDown();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.num_leftMargin = new System.Windows.Forms.NumericUpDown();
            this.lb_sizeFormat = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.cb_orientation = new System.Windows.Forms.ComboBox();
            this.cb_formatPdf = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.tp_format = new System.Windows.Forms.TabPage();
            this.grpb_groupBy = new System.Windows.Forms.GroupBox();
            this.cb_groupBy = new System.Windows.Forms.ComboBox();
            this.cb_Order_GroupBy = new System.Windows.Forms.ComboBox();
            this.ckb_groupBy = new System.Windows.Forms.CheckBox();
            this.btn_fontContentCell = new System.Windows.Forms.Button();
            this.lb_selectedFontCell = new System.Windows.Forms.Label();
            this.lb_testContentCell = new System.Windows.Forms.Label();
            this.btn_fontHeader = new System.Windows.Forms.Button();
            this.lb_selectedFontHeader = new System.Windows.Forms.Label();
            this.lb_testHeader = new System.Windows.Forms.Label();
            this.dgv_partnersPdf = new System.Windows.Forms.DataGridView();
            this.dgc_namePdf = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgc_idPartnerPdf = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgc_partnerTypePdf = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgc_idContactPdf = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgc_isPublicPdf = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgc_contactPdf = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgc_commentPdf = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgc_telPdf = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgc_emailPdf = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgc_addressPdf = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lb_titlePdf = new System.Windows.Forms.Label();
            this.txt_titlePdf = new System.Windows.Forms.TextBox();
            this.btn_gen = new System.Windows.Forms.Button();
            this.colorDialog = new System.Windows.Forms.ColorDialog();
            this.openFileDialogPicture = new System.Windows.Forms.OpenFileDialog();
            this.tc_genPDF.SuspendLayout();
            this.tp_dataSelection.SuspendLayout();
            this.tp_page.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pb_picturePdf)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.num_bottomMargin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.num_rightMargin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.num_topMargin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.num_leftMargin)).BeginInit();
            this.tp_format.SuspendLayout();
            this.grpb_groupBy.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_partnersPdf)).BeginInit();
            this.SuspendLayout();
            // 
            // saveFileDialog
            // 
            this.saveFileDialog.Filter = "Fichiers PDF|*.pdf";
            // 
            // openFileDialogPdf
            // 
            this.openFileDialogPdf.DefaultExt = "pdf";
            this.openFileDialogPdf.Filter = "Fichiers PDF|*.pdf";
            this.openFileDialogPdf.RestoreDirectory = true;
            // 
            // tc_genPDF
            // 
            this.tc_genPDF.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tc_genPDF.Controls.Add(this.tp_dataSelection);
            this.tc_genPDF.Controls.Add(this.tp_page);
            this.tc_genPDF.Controls.Add(this.tp_format);
            this.tc_genPDF.Location = new System.Drawing.Point(12, 12);
            this.tc_genPDF.Name = "tc_genPDF";
            this.tc_genPDF.SelectedIndex = 0;
            this.tc_genPDF.Size = new System.Drawing.Size(1003, 180);
            this.tc_genPDF.TabIndex = 0;
            // 
            // tp_dataSelection
            // 
            this.tp_dataSelection.Controls.Add(this.label2);
            this.tp_dataSelection.Controls.Add(this.label1);
            this.tp_dataSelection.Controls.Add(this.label5);
            this.tp_dataSelection.Controls.Add(this.label3);
            this.tp_dataSelection.Controls.Add(this.txt_search);
            this.tp_dataSelection.Controls.Add(this.ckb_showPrivateData);
            this.tp_dataSelection.Controls.Add(this.txt_PdfBeforeList);
            this.tp_dataSelection.Controls.Add(this.ckb_showPublicData);
            this.tp_dataSelection.Controls.Add(this.clb_partnersTypes);
            this.tp_dataSelection.Controls.Add(this.btn_addPdfBeforeList);
            this.tp_dataSelection.Controls.Add(this.txt_PdfAfterList);
            this.tp_dataSelection.Controls.Add(this.btn_addPdfAfterList);
            this.tp_dataSelection.Location = new System.Drawing.Point(4, 22);
            this.tp_dataSelection.Name = "tp_dataSelection";
            this.tp_dataSelection.Padding = new System.Windows.Forms.Padding(3);
            this.tp_dataSelection.Size = new System.Drawing.Size(995, 154);
            this.tp_dataSelection.TabIndex = 0;
            this.tp_dataSelection.Text = "Sélection des données";
            this.tp_dataSelection.UseVisualStyleBackColor = true;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(3, 3);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(371, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "Vous pouvez incorporer plusieurs PDF, situés avant ou après la liste générée.";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 45);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(90, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "PDF avant la liste";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(3, 114);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(106, 13);
            this.label5.TabIndex = 14;
            this.label5.Text = "Recherche d\'entrées";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(3, 75);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(89, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "PDF après la liste";
            // 
            // txt_search
            // 
            this.txt_search.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txt_search.Location = new System.Drawing.Point(6, 127);
            this.txt_search.Name = "txt_search";
            this.txt_search.Size = new System.Drawing.Size(659, 20);
            this.txt_search.TabIndex = 5;
            this.txt_search.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txt_search_KeyUp);
            // 
            // ckb_showPrivateData
            // 
            this.ckb_showPrivateData.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ckb_showPrivateData.AutoSize = true;
            this.ckb_showPrivateData.Location = new System.Drawing.Point(671, 131);
            this.ckb_showPrivateData.Name = "ckb_showPrivateData";
            this.ckb_showPrivateData.Size = new System.Drawing.Size(153, 17);
            this.ckb_showPrivateData.TabIndex = 7;
            this.ckb_showPrivateData.Text = "Afficher les entrées privées";
            this.ckb_showPrivateData.UseVisualStyleBackColor = true;
            this.ckb_showPrivateData.CheckedChanged += new System.EventHandler(this.ckb_showPublicPrivateData_CheckedChanged);
            // 
            // txt_PdfBeforeList
            // 
            this.txt_PdfBeforeList.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txt_PdfBeforeList.Location = new System.Drawing.Point(99, 42);
            this.txt_PdfBeforeList.Name = "txt_PdfBeforeList";
            this.txt_PdfBeforeList.Size = new System.Drawing.Size(759, 20);
            this.txt_PdfBeforeList.TabIndex = 1;
            // 
            // ckb_showPublicData
            // 
            this.ckb_showPublicData.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ckb_showPublicData.AutoSize = true;
            this.ckb_showPublicData.Checked = true;
            this.ckb_showPublicData.CheckState = System.Windows.Forms.CheckState.Checked;
            this.ckb_showPublicData.Location = new System.Drawing.Point(671, 114);
            this.ckb_showPublicData.Name = "ckb_showPublicData";
            this.ckb_showPublicData.Size = new System.Drawing.Size(164, 17);
            this.ckb_showPublicData.TabIndex = 6;
            this.ckb_showPublicData.Text = "Afficher les entrées publiques";
            this.ckb_showPublicData.UseVisualStyleBackColor = true;
            this.ckb_showPublicData.CheckedChanged += new System.EventHandler(this.ckb_showPublicPrivateData_CheckedChanged);
            // 
            // clb_partnersTypes
            // 
            this.clb_partnersTypes.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.clb_partnersTypes.CheckOnClick = true;
            this.clb_partnersTypes.FormattingEnabled = true;
            this.clb_partnersTypes.Location = new System.Drawing.Point(841, 114);
            this.clb_partnersTypes.Name = "clb_partnersTypes";
            this.clb_partnersTypes.Size = new System.Drawing.Size(148, 34);
            this.clb_partnersTypes.TabIndex = 8;
            this.clb_partnersTypes.ItemCheck += new System.Windows.Forms.ItemCheckEventHandler(this.clb_partnersTypes_ItemCheck);
            this.clb_partnersTypes.MouseUp += new System.Windows.Forms.MouseEventHandler(this.clb_partnersTypes_MouseUp);
            // 
            // btn_addPdfBeforeList
            // 
            this.btn_addPdfBeforeList.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_addPdfBeforeList.Location = new System.Drawing.Point(864, 40);
            this.btn_addPdfBeforeList.Name = "btn_addPdfBeforeList";
            this.btn_addPdfBeforeList.Size = new System.Drawing.Size(125, 23);
            this.btn_addPdfBeforeList.TabIndex = 2;
            this.btn_addPdfBeforeList.Text = "Choisir un document";
            this.btn_addPdfBeforeList.UseVisualStyleBackColor = true;
            this.btn_addPdfBeforeList.Click += new System.EventHandler(this.btn_addPdfBeforeList_Click);
            // 
            // txt_PdfAfterList
            // 
            this.txt_PdfAfterList.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txt_PdfAfterList.Location = new System.Drawing.Point(99, 72);
            this.txt_PdfAfterList.Name = "txt_PdfAfterList";
            this.txt_PdfAfterList.Size = new System.Drawing.Size(759, 20);
            this.txt_PdfAfterList.TabIndex = 3;
            // 
            // btn_addPdfAfterList
            // 
            this.btn_addPdfAfterList.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_addPdfAfterList.Location = new System.Drawing.Point(864, 70);
            this.btn_addPdfAfterList.Name = "btn_addPdfAfterList";
            this.btn_addPdfAfterList.Size = new System.Drawing.Size(125, 23);
            this.btn_addPdfAfterList.TabIndex = 4;
            this.btn_addPdfAfterList.Text = "Choisir un document";
            this.btn_addPdfAfterList.UseVisualStyleBackColor = true;
            this.btn_addPdfAfterList.Click += new System.EventHandler(this.btn_addPdfAfterList_Click);
            // 
            // tp_page
            // 
            this.tp_page.Controls.Add(this.txt_picturePath);
            this.tp_page.Controls.Add(this.btn_browsePicture);
            this.tp_page.Controls.Add(this.lb_picturePath);
            this.tp_page.Controls.Add(this.pb_picturePdf);
            this.tp_page.Controls.Add(this.ckb_horizontalCent);
            this.tp_page.Controls.Add(this.num_bottomMargin);
            this.tp_page.Controls.Add(this.num_rightMargin);
            this.tp_page.Controls.Add(this.num_topMargin);
            this.tp_page.Controls.Add(this.label11);
            this.tp_page.Controls.Add(this.label10);
            this.tp_page.Controls.Add(this.label9);
            this.tp_page.Controls.Add(this.label8);
            this.tp_page.Controls.Add(this.num_leftMargin);
            this.tp_page.Controls.Add(this.lb_sizeFormat);
            this.tp_page.Controls.Add(this.label7);
            this.tp_page.Controls.Add(this.cb_orientation);
            this.tp_page.Controls.Add(this.cb_formatPdf);
            this.tp_page.Controls.Add(this.label6);
            this.tp_page.Location = new System.Drawing.Point(4, 22);
            this.tp_page.Name = "tp_page";
            this.tp_page.Padding = new System.Windows.Forms.Padding(3);
            this.tp_page.Size = new System.Drawing.Size(995, 154);
            this.tp_page.TabIndex = 1;
            this.tp_page.Text = "Mise en page";
            this.tp_page.UseVisualStyleBackColor = true;
            // 
            // txt_picturePath
            // 
            this.txt_picturePath.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txt_picturePath.Location = new System.Drawing.Point(486, 8);
            this.txt_picturePath.Name = "txt_picturePath";
            this.txt_picturePath.Size = new System.Drawing.Size(415, 20);
            this.txt_picturePath.TabIndex = 23;
            // 
            // btn_browsePicture
            // 
            this.btn_browsePicture.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_browsePicture.Location = new System.Drawing.Point(907, 6);
            this.btn_browsePicture.Name = "btn_browsePicture";
            this.btn_browsePicture.Size = new System.Drawing.Size(82, 23);
            this.btn_browsePicture.TabIndex = 24;
            this.btn_browsePicture.Text = "Parcourir";
            this.btn_browsePicture.UseVisualStyleBackColor = true;
            this.btn_browsePicture.Click += new System.EventHandler(this.btn_browsePicture_Click);
            // 
            // lb_picturePath
            // 
            this.lb_picturePath.AutoSize = true;
            this.lb_picturePath.Location = new System.Drawing.Point(405, 12);
            this.lb_picturePath.Name = "lb_picturePath";
            this.lb_picturePath.Size = new System.Drawing.Size(31, 13);
            this.lb_picturePath.TabIndex = 17;
            this.lb_picturePath.Text = "Logo";
            // 
            // pb_picturePdf
            // 
            this.pb_picturePdf.BackColor = System.Drawing.Color.Gainsboro;
            this.pb_picturePdf.Location = new System.Drawing.Point(408, 35);
            this.pb_picturePdf.Name = "pb_picturePdf";
            this.pb_picturePdf.Size = new System.Drawing.Size(581, 116);
            this.pb_picturePdf.TabIndex = 16;
            this.pb_picturePdf.TabStop = false;
            // 
            // ckb_horizontalCent
            // 
            this.ckb_horizontalCent.AutoSize = true;
            this.ckb_horizontalCent.Location = new System.Drawing.Point(9, 65);
            this.ckb_horizontalCent.Name = "ckb_horizontalCent";
            this.ckb_horizontalCent.Size = new System.Drawing.Size(117, 17);
            this.ckb_horizontalCent.TabIndex = 15;
            this.ckb_horizontalCent.Text = "Centrage horizontal";
            this.ckb_horizontalCent.UseVisualStyleBackColor = true;
            // 
            // num_bottomMargin
            // 
            this.num_bottomMargin.Location = new System.Drawing.Point(99, 128);
            this.num_bottomMargin.Name = "num_bottomMargin";
            this.num_bottomMargin.Size = new System.Drawing.Size(81, 20);
            this.num_bottomMargin.TabIndex = 12;
            this.num_bottomMargin.Value = new decimal(new int[] {
            2,
            0,
            0,
            0});
            // 
            // num_rightMargin
            // 
            this.num_rightMargin.Location = new System.Drawing.Point(276, 128);
            this.num_rightMargin.Name = "num_rightMargin";
            this.num_rightMargin.Size = new System.Drawing.Size(84, 20);
            this.num_rightMargin.TabIndex = 14;
            this.num_rightMargin.Value = new decimal(new int[] {
            2,
            0,
            0,
            0});
            // 
            // num_topMargin
            // 
            this.num_topMargin.Location = new System.Drawing.Point(9, 128);
            this.num_topMargin.Name = "num_topMargin";
            this.num_topMargin.Size = new System.Drawing.Size(84, 20);
            this.num_topMargin.TabIndex = 11;
            this.num_topMargin.Value = new decimal(new int[] {
            2,
            0,
            0,
            0});
            // 
            // label11
            // 
            this.label11.Location = new System.Drawing.Point(276, 112);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(84, 13);
            this.label11.TabIndex = 9;
            this.label11.Text = "Droite";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label10
            // 
            this.label10.Location = new System.Drawing.Point(9, 112);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(84, 13);
            this.label10.TabIndex = 8;
            this.label10.Text = "Haut";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label9
            // 
            this.label9.Location = new System.Drawing.Point(99, 112);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(81, 13);
            this.label9.TabIndex = 7;
            this.label9.Text = "Bas";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label8
            // 
            this.label8.Location = new System.Drawing.Point(186, 112);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(84, 13);
            this.label8.TabIndex = 6;
            this.label8.Text = "Gauche";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // num_leftMargin
            // 
            this.num_leftMargin.Location = new System.Drawing.Point(186, 128);
            this.num_leftMargin.Name = "num_leftMargin";
            this.num_leftMargin.Size = new System.Drawing.Size(84, 20);
            this.num_leftMargin.TabIndex = 13;
            this.num_leftMargin.Value = new decimal(new int[] {
            2,
            0,
            0,
            0});
            // 
            // lb_sizeFormat
            // 
            this.lb_sizeFormat.AutoSize = true;
            this.lb_sizeFormat.Location = new System.Drawing.Point(233, 12);
            this.lb_sizeFormat.Name = "lb_sizeFormat";
            this.lb_sizeFormat.Size = new System.Drawing.Size(81, 13);
            this.lb_sizeFormat.TabIndex = 4;
            this.lb_sizeFormat.Text = "{0} cm × {1} cm";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(6, 39);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(58, 13);
            this.label7.TabIndex = 3;
            this.label7.Text = "Orientation";
            // 
            // cb_orientation
            // 
            this.cb_orientation.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cb_orientation.FormattingEnabled = true;
            this.cb_orientation.Location = new System.Drawing.Point(66, 36);
            this.cb_orientation.Name = "cb_orientation";
            this.cb_orientation.Size = new System.Drawing.Size(161, 21);
            this.cb_orientation.TabIndex = 10;
            // 
            // cb_formatPdf
            // 
            this.cb_formatPdf.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cb_formatPdf.FormattingEnabled = true;
            this.cb_formatPdf.Location = new System.Drawing.Point(66, 9);
            this.cb_formatPdf.Name = "cb_formatPdf";
            this.cb_formatPdf.Size = new System.Drawing.Size(161, 21);
            this.cb_formatPdf.TabIndex = 9;
            this.cb_formatPdf.SelectedIndexChanged += new System.EventHandler(this.cb_formatPdf_SelectedIndexChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(6, 12);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(39, 13);
            this.label6.TabIndex = 0;
            this.label6.Text = "Format";
            // 
            // tp_format
            // 
            this.tp_format.Controls.Add(this.grpb_groupBy);
            this.tp_format.Controls.Add(this.ckb_groupBy);
            this.tp_format.Controls.Add(this.btn_fontContentCell);
            this.tp_format.Controls.Add(this.lb_selectedFontCell);
            this.tp_format.Controls.Add(this.lb_testContentCell);
            this.tp_format.Controls.Add(this.btn_fontHeader);
            this.tp_format.Controls.Add(this.lb_selectedFontHeader);
            this.tp_format.Controls.Add(this.lb_testHeader);
            this.tp_format.Location = new System.Drawing.Point(4, 22);
            this.tp_format.Name = "tp_format";
            this.tp_format.Padding = new System.Windows.Forms.Padding(3);
            this.tp_format.Size = new System.Drawing.Size(995, 154);
            this.tp_format.TabIndex = 2;
            this.tp_format.Text = "Formatage";
            this.tp_format.UseVisualStyleBackColor = true;
            // 
            // grpb_groupBy
            // 
            this.grpb_groupBy.Controls.Add(this.cb_groupBy);
            this.grpb_groupBy.Controls.Add(this.cb_Order_GroupBy);
            this.grpb_groupBy.Enabled = false;
            this.grpb_groupBy.Location = new System.Drawing.Point(719, 24);
            this.grpb_groupBy.Name = "grpb_groupBy";
            this.grpb_groupBy.Size = new System.Drawing.Size(267, 52);
            this.grpb_groupBy.TabIndex = 34;
            this.grpb_groupBy.TabStop = false;
            this.grpb_groupBy.Text = "Grouper par";
            // 
            // cb_groupBy
            // 
            this.cb_groupBy.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cb_groupBy.FormattingEnabled = true;
            this.cb_groupBy.Location = new System.Drawing.Point(6, 19);
            this.cb_groupBy.Name = "cb_groupBy";
            this.cb_groupBy.Size = new System.Drawing.Size(128, 21);
            this.cb_groupBy.TabIndex = 19;
            this.cb_groupBy.SelectedIndexChanged += new System.EventHandler(this.GroupBy_sortDGV);
            // 
            // cb_Order_GroupBy
            // 
            this.cb_Order_GroupBy.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cb_Order_GroupBy.FormattingEnabled = true;
            this.cb_Order_GroupBy.Items.AddRange(new object[] {
            "Ascendant (A > Z)",
            "Descendant (Z > A)"});
            this.cb_Order_GroupBy.Location = new System.Drawing.Point(140, 19);
            this.cb_Order_GroupBy.Name = "cb_Order_GroupBy";
            this.cb_Order_GroupBy.Size = new System.Drawing.Size(121, 21);
            this.cb_Order_GroupBy.TabIndex = 20;
            this.cb_Order_GroupBy.SelectedIndexChanged += new System.EventHandler(this.SortDGV_groupBy_OrderBy);
            // 
            // ckb_groupBy
            // 
            this.ckb_groupBy.AutoSize = true;
            this.ckb_groupBy.Location = new System.Drawing.Point(719, 6);
            this.ckb_groupBy.Name = "ckb_groupBy";
            this.ckb_groupBy.Size = new System.Drawing.Size(137, 17);
            this.ckb_groupBy.TabIndex = 18;
            this.ckb_groupBy.Text = "Regrouper les éléments";
            this.ckb_groupBy.UseVisualStyleBackColor = true;
            this.ckb_groupBy.CheckedChanged += new System.EventHandler(this.ckb_groupBy_CheckedChanged);
            // 
            // btn_fontContentCell
            // 
            this.btn_fontContentCell.Location = new System.Drawing.Point(230, 6);
            this.btn_fontContentCell.Name = "btn_fontContentCell";
            this.btn_fontContentCell.Size = new System.Drawing.Size(218, 23);
            this.btn_fontContentCell.TabIndex = 17;
            this.btn_fontContentCell.Text = "Contenu des cellules";
            this.btn_fontContentCell.UseVisualStyleBackColor = true;
            this.btn_fontContentCell.Click += new System.EventHandler(this.btn_fontContentCell_Click);
            // 
            // lb_selectedFontCell
            // 
            this.lb_selectedFontCell.Location = new System.Drawing.Point(231, 32);
            this.lb_selectedFontCell.Name = "lb_selectedFontCell";
            this.lb_selectedFontCell.Size = new System.Drawing.Size(217, 13);
            this.lb_selectedFontCell.TabIndex = 27;
            this.lb_selectedFontCell.Text = "Nom de la police";
            this.lb_selectedFontCell.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lb_testContentCell
            // 
            this.lb_testContentCell.Location = new System.Drawing.Point(231, 50);
            this.lb_testContentCell.Name = "lb_testContentCell";
            this.lb_testContentCell.Size = new System.Drawing.Size(217, 33);
            this.lb_testContentCell.TabIndex = 26;
            this.lb_testContentCell.Text = "AaBbCc";
            this.lb_testContentCell.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btn_fontHeader
            // 
            this.btn_fontHeader.Location = new System.Drawing.Point(6, 6);
            this.btn_fontHeader.Name = "btn_fontHeader";
            this.btn_fontHeader.Size = new System.Drawing.Size(218, 23);
            this.btn_fontHeader.TabIndex = 16;
            this.btn_fontHeader.Text = "En-têtes de colonnes";
            this.btn_fontHeader.UseVisualStyleBackColor = true;
            this.btn_fontHeader.Click += new System.EventHandler(this.btn_fontHeader_Click);
            // 
            // lb_selectedFontHeader
            // 
            this.lb_selectedFontHeader.Location = new System.Drawing.Point(7, 33);
            this.lb_selectedFontHeader.Name = "lb_selectedFontHeader";
            this.lb_selectedFontHeader.Size = new System.Drawing.Size(217, 13);
            this.lb_selectedFontHeader.TabIndex = 24;
            this.lb_selectedFontHeader.Text = "Nom de la police";
            this.lb_selectedFontHeader.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lb_testHeader
            // 
            this.lb_testHeader.Location = new System.Drawing.Point(7, 47);
            this.lb_testHeader.Name = "lb_testHeader";
            this.lb_testHeader.Size = new System.Drawing.Size(217, 36);
            this.lb_testHeader.TabIndex = 23;
            this.lb_testHeader.Text = "AaBbCc";
            this.lb_testHeader.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // dgv_partnersPdf
            // 
            this.dgv_partnersPdf.AllowUserToAddRows = false;
            this.dgv_partnersPdf.AllowUserToDeleteRows = false;
            this.dgv_partnersPdf.AllowUserToOrderColumns = true;
            this.dgv_partnersPdf.AllowUserToResizeRows = false;
            this.dgv_partnersPdf.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgv_partnersPdf.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgv_partnersPdf.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_partnersPdf.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dgc_namePdf,
            this.dgc_idPartnerPdf,
            this.dgc_partnerTypePdf,
            this.dgc_idContactPdf,
            this.dgc_isPublicPdf,
            this.dgc_contactPdf,
            this.dgc_commentPdf,
            this.dgc_telPdf,
            this.dgc_emailPdf,
            this.dgc_addressPdf});
            this.dgv_partnersPdf.Location = new System.Drawing.Point(12, 198);
            this.dgv_partnersPdf.MultiSelect = false;
            this.dgv_partnersPdf.Name = "dgv_partnersPdf";
            this.dgv_partnersPdf.ReadOnly = true;
            this.dgv_partnersPdf.RowHeadersVisible = false;
            this.dgv_partnersPdf.ShowEditingIcon = false;
            this.dgv_partnersPdf.Size = new System.Drawing.Size(1003, 316);
            this.dgv_partnersPdf.TabIndex = 0;
            this.dgv_partnersPdf.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_partnersPdf_CellContentClick);
            this.dgv_partnersPdf.ColumnHeaderMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.OrderBy_sortDGV);
            // 
            // dgc_namePdf
            // 
            this.dgc_namePdf.HeaderText = "Nom";
            this.dgc_namePdf.Name = "dgc_namePdf";
            this.dgc_namePdf.ReadOnly = true;
            this.dgc_namePdf.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic;
            // 
            // dgc_idPartnerPdf
            // 
            this.dgc_idPartnerPdf.HeaderText = "idPartner";
            this.dgc_idPartnerPdf.Name = "dgc_idPartnerPdf";
            this.dgc_idPartnerPdf.ReadOnly = true;
            this.dgc_idPartnerPdf.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dgc_idPartnerPdf.Visible = false;
            // 
            // dgc_partnerTypePdf
            // 
            this.dgc_partnerTypePdf.HeaderText = "Structure";
            this.dgc_partnerTypePdf.Name = "dgc_partnerTypePdf";
            this.dgc_partnerTypePdf.ReadOnly = true;
            this.dgc_partnerTypePdf.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic;
            // 
            // dgc_idContactPdf
            // 
            this.dgc_idContactPdf.HeaderText = "idContact";
            this.dgc_idContactPdf.Name = "dgc_idContactPdf";
            this.dgc_idContactPdf.ReadOnly = true;
            this.dgc_idContactPdf.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dgc_idContactPdf.Visible = false;
            // 
            // dgc_isPublicPdf
            // 
            this.dgc_isPublicPdf.HeaderText = "isPublic";
            this.dgc_isPublicPdf.Name = "dgc_isPublicPdf";
            this.dgc_isPublicPdf.ReadOnly = true;
            this.dgc_isPublicPdf.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dgc_isPublicPdf.Visible = false;
            // 
            // dgc_contactPdf
            // 
            this.dgc_contactPdf.HeaderText = "Contact";
            this.dgc_contactPdf.Name = "dgc_contactPdf";
            this.dgc_contactPdf.ReadOnly = true;
            this.dgc_contactPdf.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic;
            // 
            // dgc_commentPdf
            // 
            this.dgc_commentPdf.HeaderText = "Commentaire";
            this.dgc_commentPdf.Name = "dgc_commentPdf";
            this.dgc_commentPdf.ReadOnly = true;
            this.dgc_commentPdf.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic;
            // 
            // dgc_telPdf
            // 
            this.dgc_telPdf.HeaderText = "Téléphone";
            this.dgc_telPdf.Name = "dgc_telPdf";
            this.dgc_telPdf.ReadOnly = true;
            this.dgc_telPdf.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic;
            // 
            // dgc_emailPdf
            // 
            this.dgc_emailPdf.HeaderText = "Courriel";
            this.dgc_emailPdf.Name = "dgc_emailPdf";
            this.dgc_emailPdf.ReadOnly = true;
            this.dgc_emailPdf.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic;
            // 
            // dgc_addressPdf
            // 
            this.dgc_addressPdf.HeaderText = "Adresse";
            this.dgc_addressPdf.Name = "dgc_addressPdf";
            this.dgc_addressPdf.ReadOnly = true;
            this.dgc_addressPdf.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic;
            // 
            // lb_titlePdf
            // 
            this.lb_titlePdf.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lb_titlePdf.AutoSize = true;
            this.lb_titlePdf.Location = new System.Drawing.Point(356, 525);
            this.lb_titlePdf.Name = "lb_titlePdf";
            this.lb_titlePdf.Size = new System.Drawing.Size(93, 13);
            this.lb_titlePdf.TabIndex = 20;
            this.lb_titlePdf.Text = "Titre du document";
            // 
            // txt_titlePdf
            // 
            this.txt_titlePdf.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.txt_titlePdf.Location = new System.Drawing.Point(455, 522);
            this.txt_titlePdf.Name = "txt_titlePdf";
            this.txt_titlePdf.Size = new System.Drawing.Size(429, 20);
            this.txt_titlePdf.TabIndex = 21;
            // 
            // btn_gen
            // 
            this.btn_gen.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_gen.Location = new System.Drawing.Point(890, 520);
            this.btn_gen.Name = "btn_gen";
            this.btn_gen.Size = new System.Drawing.Size(125, 23);
            this.btn_gen.TabIndex = 22;
            this.btn_gen.Text = "Générer le PDF";
            this.btn_gen.UseVisualStyleBackColor = true;
            this.btn_gen.Click += new System.EventHandler(this.btn_gen_Click);
            // 
            // openFileDialogPicture
            // 
            this.openFileDialogPicture.Filter = "Fichiers JPG/PNG|*.jpg;*.png";
            this.openFileDialogPicture.RestoreDirectory = true;
            // 
            // FormPDF
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1027, 554);
            this.Controls.Add(this.lb_titlePdf);
            this.Controls.Add(this.txt_titlePdf);
            this.Controls.Add(this.btn_gen);
            this.Controls.Add(this.tc_genPDF);
            this.Controls.Add(this.dgv_partnersPdf);
            this.MinimumSize = new System.Drawing.Size(550, 500);
            this.Name = "FormPDF";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.Text = "Génération du PDF";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FormPDF_FormClosed);
            this.Load += new System.EventHandler(this.FormPDF_Load);
            this.tc_genPDF.ResumeLayout(false);
            this.tp_dataSelection.ResumeLayout(false);
            this.tp_dataSelection.PerformLayout();
            this.tp_page.ResumeLayout(false);
            this.tp_page.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pb_picturePdf)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.num_bottomMargin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.num_rightMargin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.num_topMargin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.num_leftMargin)).EndInit();
            this.tp_format.ResumeLayout(false);
            this.tp_format.PerformLayout();
            this.grpb_groupBy.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_partnersPdf)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.SaveFileDialog saveFileDialog;
        private System.Windows.Forms.OpenFileDialog openFileDialogPdf;
        private System.Windows.Forms.TabControl tc_genPDF;
        private System.Windows.Forms.TabPage tp_page;
        private System.Windows.Forms.ComboBox cb_orientation;
        private System.Windows.Forms.ComboBox cb_formatPdf;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label lb_sizeFormat;
        private System.Windows.Forms.NumericUpDown num_bottomMargin;
        private System.Windows.Forms.NumericUpDown num_rightMargin;
        private System.Windows.Forms.NumericUpDown num_topMargin;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.NumericUpDown num_leftMargin;
        private System.Windows.Forms.TabPage tp_dataSelection;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txt_search;
        private System.Windows.Forms.CheckBox ckb_showPrivateData;
        private System.Windows.Forms.TextBox txt_PdfBeforeList;
        private System.Windows.Forms.CheckBox ckb_showPublicData;
        private System.Windows.Forms.DataGridView dgv_partnersPdf;
        private System.Windows.Forms.CheckedListBox clb_partnersTypes;
        private System.Windows.Forms.Button btn_addPdfBeforeList;
        private System.Windows.Forms.TextBox txt_PdfAfterList;
        private System.Windows.Forms.Button btn_addPdfAfterList;
        private System.Windows.Forms.Label lb_titlePdf;
        private System.Windows.Forms.TextBox txt_titlePdf;
        private System.Windows.Forms.Button btn_gen;
        private System.Windows.Forms.ColorDialog colorDialog;
        private System.Windows.Forms.CheckBox ckb_horizontalCent;
        private System.Windows.Forms.TabPage tp_format;
        private System.Windows.Forms.GroupBox grpb_groupBy;
        private System.Windows.Forms.ComboBox cb_groupBy;
        private System.Windows.Forms.ComboBox cb_Order_GroupBy;
        private System.Windows.Forms.CheckBox ckb_groupBy;
        private System.Windows.Forms.Button btn_fontContentCell;
        private System.Windows.Forms.Label lb_selectedFontCell;
        private System.Windows.Forms.Label lb_testContentCell;
        private System.Windows.Forms.Button btn_fontHeader;
        private System.Windows.Forms.Label lb_selectedFontHeader;
        private System.Windows.Forms.Label lb_testHeader;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgc_namePdf;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgc_idPartnerPdf;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgc_partnerTypePdf;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgc_idContactPdf;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgc_isPublicPdf;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgc_contactPdf;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgc_commentPdf;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgc_telPdf;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgc_emailPdf;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgc_addressPdf;
        private System.Windows.Forms.TextBox txt_picturePath;
        private System.Windows.Forms.Button btn_browsePicture;
        private System.Windows.Forms.Label lb_picturePath;
        private System.Windows.Forms.PictureBox pb_picturePdf;
        private System.Windows.Forms.OpenFileDialog openFileDialogPicture;
    }
}