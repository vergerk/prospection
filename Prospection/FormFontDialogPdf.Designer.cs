﻿namespace Prospection
{
    partial class FormFontDialogPdf
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txt_font = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txt_size = new System.Windows.Forms.TextBox();
            this.lb_exampleFont = new System.Windows.Forms.Label();
            this.lbx_sizes = new System.Windows.Forms.ListBox();
            this.lbx_fonts = new System.Windows.Forms.ListBox();
            this.btn_validFont = new System.Windows.Forms.Button();
            this.btn_cancelFont = new System.Windows.Forms.Button();
            this.lb_exampleSize = new System.Windows.Forms.Label();
            this.btn_fontColors = new System.Windows.Forms.Button();
            this.colorDialog = new System.Windows.Forms.ColorDialog();
            this.ckb_bold = new System.Windows.Forms.CheckBox();
            this.ckb_italic = new System.Windows.Forms.CheckBox();
            this.ckb_underline = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // txt_font
            // 
            this.txt_font.Location = new System.Drawing.Point(12, 25);
            this.txt_font.Name = "txt_font";
            this.txt_font.Size = new System.Drawing.Size(177, 20);
            this.txt_font.TabIndex = 0;
            this.txt_font.Enter += new System.EventHandler(this.txt_refreshListBox_Enter);
            this.txt_font.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txt_refreshListBox_KeyUp);
            this.txt_font.Leave += new System.EventHandler(this.txt_refreshListBox_Leave);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(42, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Police :";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(192, 9);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(38, 13);
            this.label3.TabIndex = 8;
            this.label3.Text = "Taille :";
            // 
            // txt_size
            // 
            this.txt_size.Location = new System.Drawing.Point(195, 25);
            this.txt_size.Name = "txt_size";
            this.txt_size.Size = new System.Drawing.Size(57, 20);
            this.txt_size.TabIndex = 2;
            this.txt_size.Enter += new System.EventHandler(this.txt_refreshListBox_Enter);
            this.txt_size.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txt_refreshListBox_KeyUp);
            this.txt_size.Leave += new System.EventHandler(this.txt_refreshListBox_Leave);
            // 
            // lb_exampleFont
            // 
            this.lb_exampleFont.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lb_exampleFont.Location = new System.Drawing.Point(12, 186);
            this.lb_exampleFont.Name = "lb_exampleFont";
            this.lb_exampleFont.Size = new System.Drawing.Size(162, 51);
            this.lb_exampleFont.TabIndex = 10;
            this.lb_exampleFont.Text = "Abcdefg";
            this.lb_exampleFont.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbx_sizes
            // 
            this.lbx_sizes.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.lbx_sizes.FormattingEnabled = true;
            this.lbx_sizes.Location = new System.Drawing.Point(195, 51);
            this.lbx_sizes.Name = "lbx_sizes";
            this.lbx_sizes.Size = new System.Drawing.Size(57, 95);
            this.lbx_sizes.TabIndex = 3;
            this.lbx_sizes.SelectedIndexChanged += new System.EventHandler(this.lbx_sizes_SelectedIndexChanged);
            this.lbx_sizes.MouseHover += new System.EventHandler(this.lbx_listBox_MouseHover);
            // 
            // lbx_fonts
            // 
            this.lbx_fonts.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.lbx_fonts.FormattingEnabled = true;
            this.lbx_fonts.Location = new System.Drawing.Point(12, 51);
            this.lbx_fonts.Name = "lbx_fonts";
            this.lbx_fonts.Size = new System.Drawing.Size(177, 95);
            this.lbx_fonts.TabIndex = 1;
            this.lbx_fonts.SelectedIndexChanged += new System.EventHandler(this.lbx_fonts_SelectedIndexChanged);
            this.lbx_fonts.MouseHover += new System.EventHandler(this.lbx_listBox_MouseHover);
            // 
            // btn_validFont
            // 
            this.btn_validFont.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_validFont.Location = new System.Drawing.Point(239, 188);
            this.btn_validFont.Name = "btn_validFont";
            this.btn_validFont.Size = new System.Drawing.Size(86, 49);
            this.btn_validFont.TabIndex = 9;
            this.btn_validFont.Text = "Valider";
            this.btn_validFont.UseVisualStyleBackColor = true;
            this.btn_validFont.Click += new System.EventHandler(this.btn_validFont_Click);
            // 
            // btn_cancelFont
            // 
            this.btn_cancelFont.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_cancelFont.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btn_cancelFont.Location = new System.Drawing.Point(239, 159);
            this.btn_cancelFont.Name = "btn_cancelFont";
            this.btn_cancelFont.Size = new System.Drawing.Size(86, 23);
            this.btn_cancelFont.TabIndex = 8;
            this.btn_cancelFont.Text = "Annuler";
            this.btn_cancelFont.UseVisualStyleBackColor = true;
            // 
            // lb_exampleSize
            // 
            this.lb_exampleSize.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lb_exampleSize.Location = new System.Drawing.Point(180, 186);
            this.lb_exampleSize.Name = "lb_exampleSize";
            this.lb_exampleSize.Size = new System.Drawing.Size(53, 51);
            this.lb_exampleSize.TabIndex = 13;
            this.lb_exampleSize.Text = "A";
            this.lb_exampleSize.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btn_fontColors
            // 
            this.btn_fontColors.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btn_fontColors.Enabled = false;
            this.btn_fontColors.Location = new System.Drawing.Point(12, 159);
            this.btn_fontColors.Name = "btn_fontColors";
            this.btn_fontColors.Size = new System.Drawing.Size(221, 23);
            this.btn_fontColors.TabIndex = 7;
            this.btn_fontColors.Text = "Couleurs";
            this.btn_fontColors.UseVisualStyleBackColor = true;
            this.btn_fontColors.Click += new System.EventHandler(this.btn_fontColors_Click);
            // 
            // ckb_bold
            // 
            this.ckb_bold.AutoSize = true;
            this.ckb_bold.Enabled = false;
            this.ckb_bold.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ckb_bold.Location = new System.Drawing.Point(258, 51);
            this.ckb_bold.Name = "ckb_bold";
            this.ckb_bold.Size = new System.Drawing.Size(52, 17);
            this.ckb_bold.TabIndex = 4;
            this.ckb_bold.Text = "Gras";
            this.ckb_bold.UseVisualStyleBackColor = true;
            this.ckb_bold.Click += new System.EventHandler(this.ckb_fontStyle_Click);
            // 
            // ckb_italic
            // 
            this.ckb_italic.AutoSize = true;
            this.ckb_italic.Enabled = false;
            this.ckb_italic.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ckb_italic.Location = new System.Drawing.Point(258, 74);
            this.ckb_italic.Name = "ckb_italic";
            this.ckb_italic.Size = new System.Drawing.Size(62, 17);
            this.ckb_italic.TabIndex = 5;
            this.ckb_italic.Text = "Oblique";
            this.ckb_italic.UseVisualStyleBackColor = true;
            this.ckb_italic.Click += new System.EventHandler(this.ckb_fontStyle_Click);
            // 
            // ckb_underline
            // 
            this.ckb_underline.AutoSize = true;
            this.ckb_underline.Enabled = false;
            this.ckb_underline.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ckb_underline.Location = new System.Drawing.Point(258, 97);
            this.ckb_underline.Name = "ckb_underline";
            this.ckb_underline.Size = new System.Drawing.Size(67, 17);
            this.ckb_underline.TabIndex = 6;
            this.ckb_underline.Text = "Souligné";
            this.ckb_underline.UseVisualStyleBackColor = true;
            this.ckb_underline.Click += new System.EventHandler(this.ckb_fontStyle_Click);
            // 
            // FormFontDialogPdf
            // 
            this.AcceptButton = this.btn_validFont;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btn_cancelFont;
            this.ClientSize = new System.Drawing.Size(337, 246);
            this.Controls.Add(this.ckb_underline);
            this.Controls.Add(this.ckb_italic);
            this.Controls.Add(this.ckb_bold);
            this.Controls.Add(this.btn_fontColors);
            this.Controls.Add(this.lb_exampleSize);
            this.Controls.Add(this.btn_cancelFont);
            this.Controls.Add(this.btn_validFont);
            this.Controls.Add(this.lb_exampleFont);
            this.Controls.Add(this.lbx_sizes);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txt_size);
            this.Controls.Add(this.lbx_fonts);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txt_font);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormFontDialogPdf";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.Text = "Police de caractères";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txt_font;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txt_size;
        private System.Windows.Forms.Label lb_exampleFont;
        private System.Windows.Forms.ListBox lbx_sizes;
        private System.Windows.Forms.ListBox lbx_fonts;
        private System.Windows.Forms.Button btn_validFont;
        private System.Windows.Forms.Button btn_cancelFont;
        private System.Windows.Forms.Label lb_exampleSize;
        private System.Windows.Forms.Button btn_fontColors;
        private System.Windows.Forms.ColorDialog colorDialog;
        private System.Windows.Forms.CheckBox ckb_bold;
        private System.Windows.Forms.CheckBox ckb_italic;
        private System.Windows.Forms.CheckBox ckb_underline;
    }
}