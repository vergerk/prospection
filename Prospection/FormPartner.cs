﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Reflection;

namespace Prospection
{
    public partial class FormPartner : Form
    {
        // http://www.zdnet.fr/actualites/c-comment-ouvrir-des-instances-de-formulaires-windows-2125884.htm?p=2
        public FormList refInstance = null;

        private bool erasePreviousData = false; // écrase les précédentes informations si TRUE au lieu d'enregistrer une nouvelle entreprise
        private bool isChanged_form; // indique si le formulaire a été modifié depuis son chargement (supprime les boites inutiles si FALSE)
        private bool changeOK = false;

        private string idPartnerInProgress;
        private List<ContactList_partner> contactList;

        /// <summary>
        /// When one create a partner
        /// </summary>
        public FormPartner(FormList refInstance)
        {
            InitializeComponent();

            this.refInstance = refInstance;
            FillPartnersTypes();

            idPartnerInProgress = Commons.RandomId();
        }

        /// <summary>
        /// When one edit a partner
        /// </summary>
        /// <param name="partner"></param>
        public FormPartner(Partner partner, FormList refInstance)
        {
            InitializeComponent();

            this.refInstance = refInstance;

            FillPartnersTypes();

            erasePreviousData = true;

            tb_namePartner.Text = partner.NamePartner;
            cb_partnerType.SelectedItem = partner.PartnerType;
            idPartnerInProgress = partner.IdPartner;

            foreach (ContactList_partner clp in partner.ListContact)
            {
                dgv_listeContact.Rows.Add(
                    clp.IdContact,
                    clp.IsPublic,
                    clp.Contact,
                    clp.Comment,
                    clp.Tel,
                    clp.Email,
                    clp.Address
                    );
            }
        } 

        #region FormPartner

        /// <summary>
        /// When the form closes
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FormPartner_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (isChanged_form)
            {
                // on affiche pas la boite de dialogue si l'utilisateur a cliqué sur le bouton "Enregistrer"
                if (!changeOK)
                {
                    DialogResult choix = MessageBox.Show("Voulez-vous vraiment fermer ce formulaire sans sauvegarder ?", "", MessageBoxButtons.OKCancel, MessageBoxIcon.Question);

                    if (choix == DialogResult.OK)
                    {
                        // A VOIR
                        // https://stackoverflow.com/questions/8832230/open-a-new-window-but-not-always-a-new-instance-of-it

                        // appel du précédent formulaire en utilisant son adresse mémoire
                        refInstance.RefreshDGV();
                        refInstance.Show();
                    }
                    else
                    {
                        // on empêche la fermeture
                        e.Cancel = true;
                    }
                }
            }
            else
            {
                refInstance.RefreshDGV();
                refInstance.Show();
            }
        }

        /// <summary>
        /// Loading of form
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FormPartner_Load(object sender, EventArgs e)
        {
            isChanged_form = false;
        }

        #endregion

        #region Save

        /// <summary>
        /// Add partner in collection and close form
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_savePartner_Click(object sender, EventArgs e)
        {
            if (IsValidPartner())
            {
                AddPartner(sender, e);
                changeOK = true;

                this.Close();
            }
            else
            {
                MessageBox.Show(
                    "Des informations sont manquantes pour valider cette saisie.",
                    "Attention",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error
                    );
            }
        }

        /// <summary>
        /// Add partner in collection and clear form
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_addPartner_Click(object sender, EventArgs e)
        {
            if (IsValidPartner())
            {
                AddPartner(sender, e);
                changeOK = true;

                ClearData(sender, e);
            }
            else
            {
                MessageBox.Show(
                    "Des informations sont manquantes pour valider cette saisie.",
                    "Attention",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error
                    );
            }
            
        }

        /// <summary>
        /// Add/edit partner in FormList
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void AddPartner(object sender, EventArgs e)
        {
            changeOK = true;

            GenContactList();

            if (erasePreviousData)
            {
                refInstance.EditingPartner(
                    new Partner(
                        idPartnerInProgress,
                        tb_namePartner.Text,
                        (refInstance.comm.SelectPartnerType(cb_partnerType.SelectedItem.ToString())).Name,
                        contactList
                    ));
            }
            else
            {
                refInstance.AddingPartner(
                    new Partner(
                        tb_namePartner.Text,
                        (refInstance.comm.SelectPartnerType(cb_partnerType.SelectedItem.ToString())).Name,
                        contactList
                    ));
            }

            confirmSavePartner();
        }

        /// <summary>
        /// Add a type of partner
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_addPartnerType_Click(object sender, EventArgs e)
        {
            FormPartnerType pt = new FormPartnerType(refInstance);
            pt.ShowDialog();

            FillPartnersTypes();
        }

        /// <summary>
        /// Show confirmation of save partner
        /// </summary>
        /// <param name="partnerName"></param>
        private void confirmSavePartner()
        {
            lb_confirmSavePartner.Text = string.Format("Le partenaire {0} a bien été enregistré.", tb_namePartner.Text);
            lb_confirmSavePartner.Visible = true;

            tm_Partner.Interval = 5000;
            tm_Partner.Enabled = true;
            tm_Partner.Start();
        }

        #endregion

        #region Control & format
        
        /// <summary>
        /// Generate a new list of contact
        /// </summary>
        private void GenContactList()
        {
            string idContact = "";
            int length_listContact = dgv_listeContact.RowCount;
            DataGridViewRow row = new DataGridViewRow();
            bool isEmptyRow = true;

            contactList = new List<ContactList_partner>();

            for (int i = 0; i < length_listContact; i++)
            {
                row = dgv_listeContact.Rows[i];

                // on vérifie si au moins une cellule est remplie. Si oui, on considère la ligne valide sinon on l'a passe
                foreach (DataGridViewCell c in row.Cells)
                {
                    if (c.Value != null)
                    {
                        if ((c.ValueType.Name == "Object") && (c.Value != ""))
                        {
                            if (((string)c.Value).Trim() != "")
                            {
                                isEmptyRow = false;
                                break;
                            }
                        }

                        if ((c.ValueType.Name != "Boolean") && (c.OwningColumn.Name != "dgc_idContact"))
                        {
                            isEmptyRow = false;
                            break;
                        }
                    }
                }

                // si la ligne n'est pas vide, on l'ajoute à la liste des contacts
                if (!isEmptyRow)
                {
                    if (
                        dgv_listeContact.Rows[i].Cells["dgc_idContact"].Value == null ||
                        !erasePreviousData
                        )
                    {
                        idContact = Commons.RandomId();
                    }
                    else
                    {
                        idContact = dgv_listeContact.Rows[i].Cells["dgc_idContact"].Value.ToString();
                    }

                    contactList.Add(new ContactList_partner(
                        idContact,
                        Convert.ToBoolean(dgv_listeContact.Rows[i].Cells["dgc_isPublic"].Value),
                        ConvertNullToString(dgv_listeContact.Rows[i].Cells["dgc_contact"].Value).Trim(),
                        ConvertNullToString(dgv_listeContact.Rows[i].Cells["dgc_comment"].Value).Trim(),
                        Commons.FilterTel(ConvertNullToString(dgv_listeContact.Rows[i].Cells["dgc_tel"].Value), refInstance.opt.PhoneSep).Trim(),
                        ConvertNullToString(dgv_listeContact.Rows[i].Cells["dgc_email"].Value).Trim(),
                        ConvertNullToString(dgv_listeContact.Rows[i].Cells["dgc_address"].Value).Trim()
                        ));
                }

                isEmptyRow = true;
            }
        }

        /// <summary>
        /// Clear form
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ClearData(object sender, EventArgs e)
        {
            // contrôles
            foreach (Control control in this.Controls)
            {
                switch (control.GetType().ToString())
                {
                    case "System.Windows.Forms.TextBox":
                        control.ResetText();
                        break;
                    case "System.Windows.Forms.DataGridView":
                        ((DataGridView)control).Rows.Clear();
                        break;
                    case "System.Windows.Forms.ComboBox":

                        if (cb_partnerType.Items.Count > 0)
                        {
                            cb_partnerType.SelectedIndex = 0;
                        }
                        break;
                }
            }
        }

        /// <summary>
        /// Fill ComboBox with list of partners type
        /// </summary>
        private void FillPartnersTypes()
        {
            cb_partnerType.Enabled = true;

            // cas où la combobox a déjà des lignes
            if (refInstance.OgpFile.ListPartnersTypes.Count > 0)
            {
                cb_partnerType.Items.Clear();

                foreach (PartnerType p in refInstance.OgpFile.ListPartnersTypes)
                {
                    cb_partnerType.Items.Add(p.Name);
                }

                cb_partnerType.SelectedIndex = 0;

                EnableDisableBtn(btn_addPartnerType, false);

                EnableDisableBtn(lb_partnerType, true);
                cb_partnerType.Visible = true;
                cb_partnerType.Enabled = true;
                EnableDisableBtn(btn_addPartnerType_def, true);
            }
            else
            {
                EnableDisableBtn(btn_addPartnerType, true);

                EnableDisableBtn(lb_partnerType, false);
                cb_partnerType.Visible = false;
                cb_partnerType.Enabled = false;
                EnableDisableBtn(btn_addPartnerType_def, false);
            }
        }

        /// <summary>
        /// Enable or disable button on form
        /// </summary>
        /// <param name="c"></param>
        /// <param name="s"></param>
        private void EnableDisableBtn(Control c, bool s)
        {
            c.Visible = s;
            c.Enabled = s;
        }

        /// <summary>
        /// Convert null value to empty string
        /// </summary>
        /// <param name="o"></param>
        /// <returns></returns>
        private string ConvertNullToString(object o)
        {
            string r = "";

            if (o != null)
            {
                r = o.ToString();
            }

            return r;
        }

        /// <summary>
        /// Verify if partner form is valid
        /// </summary>
        /// <returns></returns>
        public bool IsValidPartner()
        {
            bool r = false;
            DataGridViewRow row = new DataGridViewRow();

            if (
                (dgv_listeContact.RowCount > 0) &&
                (dgv_listeContact.NewRowIndex != 0) &&
                (cb_partnerType.SelectedItem != null) &&
                (tb_namePartner.Text.Trim().Length != 0)
                )
            {
                r = true;
            }

            return r;
        }

        #endregion

        #region Events

        /// <summary>
        /// Necessary to "copy/paste"
        /// https://stackoverflow.com/a/3697966
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgv_listeContact_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            DataGridViewTextBoxEditingControl tb = (DataGridViewTextBoxEditingControl)e.Control;
            tb.KeyDown += new KeyEventHandler(dgv_listeContact_KeyDown);

            e.Control.KeyDown += new KeyEventHandler(dgv_listeContact_KeyDown);
        }

        /// <summary>
        /// Copy/paste in cell
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgv_listeContact_KeyDown(object sender, KeyEventArgs e)
        {
            if (
                dgv_listeContact.SelectedCells.Count == 1 &&
                e.Control == true &&
                e.KeyCode == Keys.V
                )
            {
                ((DataGridView)sender).SelectedCells[0].Value = Clipboard.GetText();
            }
        }

        /// <summary>
        /// Timer event for reset label
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tm_Partner_Tick(object sender, EventArgs e)
        {
            lb_confirmSavePartner.Visible = false;
        }

        #endregion
    }
}
