﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;

namespace Prospection
{
    public class Commons
    {
        private FormList refInstance;

        // on ne contrôle pas le contenu de ces colonnes
        private static string[] noControlProp = { "ListContact", "IdPartner", "IdContact", "IsPublic" };

        public static string[] NoControlProp
        {
            get { return Commons.noControlProp; }
            set { Commons.noControlProp = value; }
        }
        
        public Commons(FormList r) 
        {
            refInstance = r;
        }

        #region Divers

        /// <summary>
        /// Loads embedded dll
        /// </summary>
        /*public static void AssemblyDll(object f, Type className)
        {
            f = Convert.ChangeType(f, className);

            // http://adamthetech.com/2011/06/embed-dll-files-within-an-exe-c-sharp-winforms/

            AppDomain.CurrentDomain.AssemblyResolve += (sender, args) =>
            {
                string resourceName = new AssemblyName(args.Name).Name + ".dll";
                string resource = Array.Find(f.GetType().Assembly.GetManifestResourceNames(), element => element.EndsWith(resourceName));

                using (var stream = Assembly.GetExecutingAssembly().GetManifestResourceStream(resource))
                {
                    Byte[] assemblyData = new Byte[stream.Length];
                    stream.Read(assemblyData, 0, assemblyData.Length);
                    return Assembly.Load(assemblyData);
                }
            };
        }*/

        /// <summary>
        /// Generate a random Id
        /// </summary>
        /// <param name="maxLength"></param>
        /// <returns></returns>
        public static string RandomId(int maxLength = 5)
        {
            string pattern = "abcdefghijklmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            string r = "";
            Random rand = new Random(int.Parse(Guid.NewGuid().ToString().Substring(0, 8), System.Globalization.NumberStyles.HexNumber));

            for (int i = 0; i < maxLength; i++)
            {
                r += pattern[rand.Next(pattern.Length - 1)];
            }

            return r;
        }

        /// <summary>
        /// Filter tel number for search
        /// </summary>
        /// <param name="tel"></param>
        /// <param name="sep"></param>
        /// <returns></returns>
        public static string FilterTel(string tel, char sep)
        {
            string fiteredTel = "";
            string newStr = "";
            string resStr = "";

            Regex reg = new Regex("(0|\\+33)[1-9]((.*?)[0-9]{2}){4}", RegexOptions.Multiline);
            char[] arr = { '+', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9' };

            // on vérifie si le numéro de téléphone apparait
            if (reg.IsMatch(tel))
            {
                // on sépare le numéro de téléphone du texte possible
                foreach (Match r in reg.Matches(tel))
                {
                    fiteredTel = "";
                    resStr = r.ToString();
                    // on conserve que les chiffres et caractère "+" (+33..)
                    // on rentre dans la boucle que si le numéro n'est pas national (10) ou avec indicatif international (12)
                    if (resStr.Length != 10 && resStr.Length != 12)
                    {
                        for (int i = 0; i < resStr.Length; i++)
                        {
                            if (Array.Exists(arr, element => element == resStr[i]))
                            {
                                if ((i == 0 && resStr[i] == '+') || resStr[i] != '+')
                                {
                                    fiteredTel += resStr[i];
                                }
                            }
                        }
                    }
                    else
                    {
                        fiteredTel = resStr;
                    }

                    newStr = "";

                    // telTemp = 0102030405 ou +33102030405
                    // on ajoute maintenant le caractère séparateur
                    for (int i = 0; i < fiteredTel.Length; i++)
                    {
                        // si le premier caractère est celui d'un numéro avec indicatif international, 
                        // on passe sur le second groupe hors indicatif
                        // +331 02 03 04 05
                        //   01 02 03 04 05
                        if (fiteredTel[0] == '+' && i == 0)
                        {
                            i = 4;
                            newStr += fiteredTel.Substring(0, 3);
                        }

                        newStr += fiteredTel.Substring(i, 2);

                        // le séparateur uniquement si on a pas atteint la fin de la chaine
                        if (i < fiteredTel.Length - 2)
                        {
                            newStr += sep;
                        }

                        i++;
                    }

                    // on remplace l'ancienne chaine par la nouvelle
                    tel = tel.Replace(resStr, newStr);
                }
            }

            return tel;
        }

        /// <summary>
        /// Define foreground color (black/white) based on background color
        /// </summary>
        /// <param name="c"></param>
        /// <returns></returns>
        public static Color ForeColorFromBackColor(Color c)
        {
            Color t = Color.Black;

            // méthode YIQ
            // https://24ways.org/2010/calculating-color-contrast/

            if ((((c.R * 299) + (c.G * 587) + (c.B * 114)) / 1000) >= 128)
            {
                t = Color.Black;
            }
            else
            {
                t = Color.White;
            }

            return t;
        }

        /// <summary>
        /// Returns column name of DGV by Header Text
        /// </summary>
        /// <param name="dgv"></param>
        /// <param name="headerText"></param>
        /// <returns></returns>
        public string DataGridViewColumnName_By_HeaderText(DataGridView dgv, string headerText)
        {
            string r = "";

            foreach (DataGridViewColumn dgc in dgv.Columns)
            {
                if (dgc.HeaderText == headerText)
                {
                    r = dgc.Name;
                    break;
                }
            }

            return r;
        }

        /// <summary>
        /// Replaces carriage return in datagridview by space
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        public static string ReplaceCarriageReturn(string text)
        {
            return text.Replace("\n", " ");
        }

        #endregion

        #region Gestion des types de partenaires

        /// <summary>
        /// Add a type in list
        /// </summary>
        /// <param name="p"></param>
        public void AddPartnerType(PartnerType p)
        {
            refInstance.OgpFile.ListPartnersTypes.Add(p);
        }

        /// <summary>
        /// Select a type of partner by the name
        /// </summary>
        /// <param name="name"></param>
        public PartnerType SelectPartnerType(string name)
        {
            PartnerType r = null;

            foreach (PartnerType pt in refInstance.OgpFile.ListPartnersTypes)
            {
                if (pt.Name == name)
                {
                    r = pt;
                    break;
                }
            }

            return r;
        }

        /// <summary>
        /// Method for search data in partners
        /// </summary>
        /// <param name="s"></param>
        /// <param name="searchList"></param>
        /// <returns></returns>
        public static List<Partner> SearchInPartners(string s, List<Partner> searchList)
        {
            List<Partner> pl = new List<Partner>();
            List<ContactList_partner> clp;
            bool isFoundContact = false;
            bool isFoundPartner = false;

            s = s.ToLower();

            // information que sur le partenaire
            foreach (Partner partner in searchList)
            {
                clp = new List<ContactList_partner>();
                isFoundPartner = false;

                foreach (var v in typeof(Partner).GetProperties())
                {
                    // on ne contrôle pas certaines propriétés
                    if (!Array.Exists(noControlProp, element => element == v.Name))
                    {
                        if (((string)v.GetValue(partner, null)).ToLower().Contains(s))
                        {
                            pl.Add(partner);
                            isFoundPartner = true;
                            break;
                        }
                    }
                }

                // si la recherche sur les infos partenaires n'a pas abouti, on passe à ses contacts
                if (!isFoundPartner)
                {
                    foreach (ContactList_partner contact in partner.ListContact)
                    {
                        foreach (var vc in typeof(ContactList_partner).GetProperties())
                        {
                            // on ne contrôle pas certaines propriétés
                            if (!Array.Exists(noControlProp, element => element == vc.Name))
                            {
                                if (((string)vc.GetValue(contact, null)).ToLower().Contains(s))
                                {
                                    //pl.Add(partner);
                                    clp.Add(contact);
                                    isFoundContact = true;
                                    break;
                                }
                            }
                        }
                    }

                    if (isFoundContact)
                    {
                        pl.Add(new Partner(
                            partner.IdPartner,
                            partner.NamePartner,
                            partner.PartnerType,
                            clp
                            ));

                        isFoundContact = false;
                    }
                }
            }

            return pl;
        }

        #endregion

        #region opérations E/S

        /// <summary>
        /// Load XML in variable
        /// </summary>
        /// <param name="className"></param>
        /// <param name="fileName"></param>
        /// <param name="o"></param>
        public static object LoadFile(Type className, string filename, object f, string format = "")
        {
            object o = null;
            string filenameWExt = filename + "." + format;

            if (format == "")
            {
                filenameWExt = filename;
            }

            // on vérifie si le fichier existe
            if (File.Exists(filenameWExt))
            {
                try
                {
                    XmlSerializer xs = new XmlSerializer(className);
                    using (StreamReader rd = new StreamReader(filenameWExt))
                    {
                        o = xs.Deserialize(rd);
                    }
                }
                catch (Exception e)
                {
                    MessageBox.Show(
                        "Il y a eu une erreur lors du chargement.\n" +
                        "Informations complémentaires : \n" +
                        e.InnerException.Message
                        ,
                        ""
                        ,
                        MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1
                        );
                }
            }

            // on renvoie la valeur initiale de la variable
            if (o == null)
            {
                o = f;
            }

            return o;
        }

        /// <summary>
        /// Save in XML file
        /// </summary>
        /// <param name="className"></param>
        /// <param name="fileName"></param>
        /// <param name="o"></param>
        public static void SaveInFile(Type className, string fileName, object o, string format)
        {
            try
            {
                // supprime le point si aucune extension est indiqué
                string path = "";

                if (format == "")
                {
                    path = fileName;
                }
                else
                {
                    path = fileName + "." + format;
                }

                XmlSerializer xs = new XmlSerializer(className);
                using (StreamWriter wr = new StreamWriter(path))
                {
                    xs.Serialize(wr, o);
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(
                        "Il y a eu une erreur lors de la sauvegarde :\r\n" +
                        e.InnerException.Message
                        ,
                        "!!!"
                        ,
                        MessageBoxButtons.OK,
                        MessageBoxIcon.Error
                    );
            }
        }

        /// <summary>
        /// Converts the first config.xml format to v 1.0.0.1 format
        /// </summary>
        /// <param name="className"></param>
        /// <param name="fileName"></param>
        /// <param name="o"></param>
        public static object ConvertFromOldVersion(Type className, object var, XmlDocument xmlSource)
        {
            xmlSource.Save("temp.ogeTemp");

            var = Commons.LoadFile(className, "temp", var, "ogeTemp");

            if (File.Exists("temp.ogeTemp") && var != null)
            {
                File.Delete("temp.ogeTemp");
            }

            return var;
        }

        /// <summary>
        /// Extracts a node and create a new file with this
        /// </summary>
        /// <param name="node"></param>
        /// <param name="fileName"></param>
        /// <param name="className">Class name of the future variable</param>
        /// <returns></returns>
        public static XmlDocument ExtractXmlNode(string node, string fileName, Type className)
        {
            string newNodeName = "";

            // le noeud racine n'est pas le nom de la variable ou de la classe dans certains cas
            // List<[nomClasse]> -> ArrayOf[nomClasse]
            if (className.Name.Contains("List"))
            {
                string namespaceAssembly = System.Reflection.Assembly.GetExecutingAssembly().EntryPoint.DeclaringType.Namespace;
                string regex = string.Format("{0}{1}", namespaceAssembly, @".\w*");
                Regex reg = new Regex(regex, RegexOptions.None);

                if (reg.IsMatch(className.FullName))
                {
                    string classList = reg.Matches(className.FullName)[0].ToString().Remove(0, namespaceAssembly.Length + 1);
                    newNodeName = "ArrayOf" + classList;
                }
            }

            XmlDocument newDoc = new XmlDocument();
            XmlDocument doc = new XmlDocument();
            doc.Load(fileName);

            // on vérifie si la déclaration existe. Si non, on l'ajoute au nouveau fichier
            if (doc.FirstChild.NodeType == XmlNodeType.XmlDeclaration)
            {
                XmlDeclaration dec = doc.FirstChild as XmlDeclaration;
                newDoc = XmlDoc_AppendOrCreate(newDoc, dec);
            }

            // on récupère le noeud qui nous intéresse dans l'ancien fichier pour en faire la racine
            XmlNode nodeXml = doc.SelectSingleNode("//" + node);
            if (nodeXml != null)
            {
                // ajout de -> xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema"
                newDoc = XmlDoc_AppendOrCreate(newDoc, nodeXml, newNodeName);

                newDoc.DocumentElement.SetAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
                newDoc.DocumentElement.SetAttribute("xmlns:xsd", "http://www.w3.org/2001/XMLSchema");
            }

            return newDoc;
        }

        /// <summary>
        /// Creates personalized nodes
        /// </summary>
        /// <param name="xd"></param>
        /// <param name="xn"></param>
        /// <param name="nodeNewName"></param>
        /// <returns></returns>
        public static XmlDocument XmlDoc_AppendOrCreate(XmlDocument xd, XmlNode xn, string nodeNewName = "")
        {
            string nodeName = xn.Name;

            if (nodeNewName != "")
            {
                nodeName = nodeNewName;
            }

            XmlNode newNode = xd.CreateNode(xn.NodeType, nodeName, xn.NamespaceURI);

            if (xn.InnerXml != "")
            {
                newNode.InnerXml = xn.InnerXml;
            }
            else
            {
                newNode.InnerText = xn.InnerText;
            }

            xd.AppendChild(newNode);

            return xd;
        }

        #endregion
    }
}
