﻿namespace Prospection
{
    partial class FormList
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormList));
            this.dgv_partners = new System.Windows.Forms.DataGridView();
            this.dgc_name = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgc_idPartner = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgc_partnerType = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgc_idContact = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgc_isPublic = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgc_contact = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgc_comment = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgc_tel = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgc_email = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgc_address = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cms_cell = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.tsm_copyClipboard = new System.Windows.Forms.ToolStripMenuItem();
            this.tsm_content = new System.Windows.Forms.ToolStripMenuItem();
            this.tsm_sendEmail = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.tsm_removeEntry = new System.Windows.Forms.ToolStripMenuItem();
            this.tsm_menu = new System.Windows.Forms.MenuStrip();
            this.tsm_operations = new System.Windows.Forms.ToolStripMenuItem();
            this.tsm_newFile = new System.Windows.Forms.ToolStripMenuItem();
            this.tsm_openFile = new System.Windows.Forms.ToolStripMenuItem();
            this.tsm_saveList = new System.Windows.Forms.ToolStripMenuItem();
            this.tsm_recentsFiles = new System.Windows.Forms.ToolStripMenuItem();
            this.tsm_emptyRecentsFiles = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.tsm_addPartner = new System.Windows.Forms.ToolStripMenuItem();
            this.tsm_manageTypes = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.tsm_pdf = new System.Windows.Forms.ToolStripMenuItem();
            this.tsm_aboutThis = new System.Windows.Forms.ToolStripMenuItem();
            this.tsm_options = new System.Windows.Forms.ToolStripMenuItem();
            this.tsm_sortDGV = new System.Windows.Forms.ToolStripMenuItem();
            this.tst_search = new System.Windows.Forms.ToolStripTextBox();
            this.tslb_partnersCount = new System.Windows.Forms.ToolStripStatusLabel();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.tslb_elementsFound = new System.Windows.Forms.ToolStripStatusLabel();
            this.tslb_elementsVisible = new System.Windows.Forms.ToolStripStatusLabel();
            this.openFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.saveFileDialog = new System.Windows.Forms.SaveFileDialog();
            this.saveNewFile = new System.Windows.Forms.SaveFileDialog();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_partners)).BeginInit();
            this.cms_cell.SuspendLayout();
            this.tsm_menu.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // dgv_partners
            // 
            this.dgv_partners.AllowUserToAddRows = false;
            this.dgv_partners.AllowUserToDeleteRows = false;
            this.dgv_partners.AllowUserToOrderColumns = true;
            this.dgv_partners.AllowUserToResizeRows = false;
            this.dgv_partners.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgv_partners.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgv_partners.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dgv_partners.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_partners.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dgc_name,
            this.dgc_idPartner,
            this.dgc_partnerType,
            this.dgc_idContact,
            this.dgc_isPublic,
            this.dgc_contact,
            this.dgc_comment,
            this.dgc_tel,
            this.dgc_email,
            this.dgc_address});
            this.dgv_partners.ContextMenuStrip = this.cms_cell;
            this.dgv_partners.Location = new System.Drawing.Point(12, 27);
            this.dgv_partners.MultiSelect = false;
            this.dgv_partners.Name = "dgv_partners";
            this.dgv_partners.RowHeadersVisible = false;
            this.dgv_partners.Size = new System.Drawing.Size(951, 567);
            this.dgv_partners.TabIndex = 1;
            this.dgv_partners.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dg_partnersList_CellDoubleClick);
            this.dgv_partners.ColumnHeaderMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgv_partners_ColumnHeaderMouseClick);
            this.dgv_partners.KeyUp += new System.Windows.Forms.KeyEventHandler(this.dg_partnersList_KeyUp);
            this.dgv_partners.MouseDown += new System.Windows.Forms.MouseEventHandler(this.dg_partnersList_MouseDown);
            // 
            // dgc_name
            // 
            this.dgc_name.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this.dgc_name.DefaultCellStyle = dataGridViewCellStyle1;
            this.dgc_name.HeaderText = "Nom";
            this.dgc_name.Name = "dgc_name";
            this.dgc_name.ReadOnly = true;
            this.dgc_name.Width = 54;
            // 
            // dgc_idPartner
            // 
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this.dgc_idPartner.DefaultCellStyle = dataGridViewCellStyle2;
            this.dgc_idPartner.HeaderText = "idPartner";
            this.dgc_idPartner.Name = "dgc_idPartner";
            this.dgc_idPartner.ReadOnly = true;
            this.dgc_idPartner.Visible = false;
            // 
            // dgc_partnerType
            // 
            this.dgc_partnerType.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this.dgc_partnerType.DefaultCellStyle = dataGridViewCellStyle3;
            this.dgc_partnerType.HeaderText = "Structure";
            this.dgc_partnerType.Name = "dgc_partnerType";
            this.dgc_partnerType.ReadOnly = true;
            this.dgc_partnerType.Width = 75;
            // 
            // dgc_idContact
            // 
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this.dgc_idContact.DefaultCellStyle = dataGridViewCellStyle4;
            this.dgc_idContact.HeaderText = "idContact";
            this.dgc_idContact.Name = "dgc_idContact";
            this.dgc_idContact.ReadOnly = true;
            this.dgc_idContact.Visible = false;
            // 
            // dgc_isPublic
            // 
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this.dgc_isPublic.DefaultCellStyle = dataGridViewCellStyle5;
            this.dgc_isPublic.HeaderText = "isPublic";
            this.dgc_isPublic.Name = "dgc_isPublic";
            this.dgc_isPublic.ReadOnly = true;
            this.dgc_isPublic.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dgc_isPublic.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dgc_isPublic.Visible = false;
            // 
            // dgc_contact
            // 
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this.dgc_contact.DefaultCellStyle = dataGridViewCellStyle6;
            this.dgc_contact.HeaderText = "Contact";
            this.dgc_contact.Name = "dgc_contact";
            this.dgc_contact.ReadOnly = true;
            // 
            // dgc_comment
            // 
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this.dgc_comment.DefaultCellStyle = dataGridViewCellStyle7;
            this.dgc_comment.HeaderText = "Commentaire";
            this.dgc_comment.Name = "dgc_comment";
            this.dgc_comment.ReadOnly = true;
            // 
            // dgc_tel
            // 
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this.dgc_tel.DefaultCellStyle = dataGridViewCellStyle8;
            this.dgc_tel.HeaderText = "Téléphone";
            this.dgc_tel.Name = "dgc_tel";
            this.dgc_tel.ReadOnly = true;
            // 
            // dgc_email
            // 
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this.dgc_email.DefaultCellStyle = dataGridViewCellStyle9;
            this.dgc_email.HeaderText = "Courriel";
            this.dgc_email.Name = "dgc_email";
            this.dgc_email.ReadOnly = true;
            // 
            // dgc_address
            // 
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this.dgc_address.DefaultCellStyle = dataGridViewCellStyle10;
            this.dgc_address.HeaderText = "Adresse";
            this.dgc_address.Name = "dgc_address";
            this.dgc_address.ReadOnly = true;
            // 
            // cms_cell
            // 
            this.cms_cell.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsm_copyClipboard,
            this.tsm_content,
            this.tsm_sendEmail,
            this.toolStripSeparator2,
            this.tsm_removeEntry});
            this.cms_cell.Name = "cms_cellule";
            this.cms_cell.ShowItemToolTips = false;
            this.cms_cell.Size = new System.Drawing.Size(195, 98);
            this.cms_cell.Click += new System.EventHandler(this.cms_cell_Click);
            // 
            // tsm_copyClipboard
            // 
            this.tsm_copyClipboard.Name = "tsm_copyClipboard";
            this.tsm_copyClipboard.Size = new System.Drawing.Size(194, 22);
            this.tsm_copyClipboard.Text = "Copie presse-papier";
            // 
            // tsm_content
            // 
            this.tsm_content.Name = "tsm_content";
            this.tsm_content.Size = new System.Drawing.Size(194, 22);
            this.tsm_content.Text = "(aucun contenu)";
            this.tsm_content.TextDirection = System.Windows.Forms.ToolStripTextDirection.Horizontal;
            // 
            // tsm_sendEmail
            // 
            this.tsm_sendEmail.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tsm_sendEmail.Image = global::Prospection.Properties.Resources.email;
            this.tsm_sendEmail.Name = "tsm_sendEmail";
            this.tsm_sendEmail.Size = new System.Drawing.Size(194, 22);
            this.tsm_sendEmail.Text = "Envoyer un courriel";
            this.tsm_sendEmail.Click += new System.EventHandler(this.tsm_sendEmail_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(191, 6);
            // 
            // tsm_removeEntry
            // 
            this.tsm_removeEntry.Name = "tsm_removeEntry";
            this.tsm_removeEntry.Size = new System.Drawing.Size(194, 22);
            this.tsm_removeEntry.Text = "Supprimer cette entrée";
            this.tsm_removeEntry.Click += new System.EventHandler(this.RemovePartner);
            // 
            // tsm_menu
            // 
            this.tsm_menu.BackColor = System.Drawing.Color.Transparent;
            this.tsm_menu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsm_operations,
            this.tsm_aboutThis,
            this.tsm_options,
            this.tsm_sortDGV,
            this.tst_search});
            this.tsm_menu.Location = new System.Drawing.Point(0, 0);
            this.tsm_menu.Name = "tsm_menu";
            this.tsm_menu.Size = new System.Drawing.Size(975, 27);
            this.tsm_menu.TabIndex = 3;
            this.tsm_menu.Text = "menuStrip1";
            // 
            // tsm_operations
            // 
            this.tsm_operations.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tsm_operations.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsm_newFile,
            this.tsm_openFile,
            this.tsm_saveList,
            this.tsm_recentsFiles,
            this.toolStripSeparator3,
            this.tsm_addPartner,
            this.tsm_manageTypes,
            this.toolStripSeparator1,
            this.tsm_pdf});
            this.tsm_operations.Name = "tsm_operations";
            this.tsm_operations.Size = new System.Drawing.Size(77, 23);
            this.tsm_operations.Text = "Opérations";
            // 
            // tsm_newFile
            // 
            this.tsm_newFile.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tsm_newFile.Name = "tsm_newFile";
            this.tsm_newFile.Size = new System.Drawing.Size(186, 22);
            this.tsm_newFile.Text = "Nouveau fichier";
            this.tsm_newFile.Click += new System.EventHandler(this.tsm_newOrOpenFile_Click);
            // 
            // tsm_openFile
            // 
            this.tsm_openFile.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tsm_openFile.Name = "tsm_openFile";
            this.tsm_openFile.Size = new System.Drawing.Size(186, 22);
            this.tsm_openFile.Text = "Ouvrir un fichier";
            this.tsm_openFile.Click += new System.EventHandler(this.tsm_newOrOpenFile_Click);
            // 
            // tsm_saveList
            // 
            this.tsm_saveList.Name = "tsm_saveList";
            this.tsm_saveList.Size = new System.Drawing.Size(186, 22);
            this.tsm_saveList.Text = "Sauver la liste";
            this.tsm_saveList.Click += new System.EventHandler(this.tsm_saveList_Click);
            // 
            // tsm_recentsFiles
            // 
            this.tsm_recentsFiles.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tsm_recentsFiles.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsm_emptyRecentsFiles});
            this.tsm_recentsFiles.Name = "tsm_recentsFiles";
            this.tsm_recentsFiles.Size = new System.Drawing.Size(186, 22);
            this.tsm_recentsFiles.Text = "Fichiers récents";
            // 
            // tsm_emptyRecentsFiles
            // 
            this.tsm_emptyRecentsFiles.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tsm_emptyRecentsFiles.Enabled = false;
            this.tsm_emptyRecentsFiles.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tsm_emptyRecentsFiles.ForeColor = System.Drawing.Color.DimGray;
            this.tsm_emptyRecentsFiles.Name = "tsm_emptyRecentsFiles";
            this.tsm_emptyRecentsFiles.Size = new System.Drawing.Size(185, 22);
            this.tsm_emptyRecentsFiles.Text = "(aucun fichier récent)";
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(183, 6);
            // 
            // tsm_addPartner
            // 
            this.tsm_addPartner.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tsm_addPartner.Name = "tsm_addPartner";
            this.tsm_addPartner.Size = new System.Drawing.Size(186, 22);
            this.tsm_addPartner.Text = "Ajouter un partenaire";
            this.tsm_addPartner.Click += new System.EventHandler(this.tsm_addPartner_Click);
            // 
            // tsm_manageTypes
            // 
            this.tsm_manageTypes.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tsm_manageTypes.Name = "tsm_manageTypes";
            this.tsm_manageTypes.Size = new System.Drawing.Size(186, 22);
            this.tsm_manageTypes.Text = "Gérer les structures";
            this.tsm_manageTypes.Click += new System.EventHandler(this.tsm_manageTypes_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(183, 6);
            // 
            // tsm_pdf
            // 
            this.tsm_pdf.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tsm_pdf.Name = "tsm_pdf";
            this.tsm_pdf.Size = new System.Drawing.Size(186, 22);
            this.tsm_pdf.Text = "Générer un PDF";
            this.tsm_pdf.Click += new System.EventHandler(this.tsm_pdf_Click);
            // 
            // tsm_aboutThis
            // 
            this.tsm_aboutThis.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.tsm_aboutThis.Name = "tsm_aboutThis";
            this.tsm_aboutThis.Size = new System.Drawing.Size(95, 23);
            this.tsm_aboutThis.Text = "A propos de ...";
            this.tsm_aboutThis.Click += new System.EventHandler(this.tsm_aboutThis_Click);
            // 
            // tsm_options
            // 
            this.tsm_options.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.tsm_options.Name = "tsm_options";
            this.tsm_options.Size = new System.Drawing.Size(61, 23);
            this.tsm_options.Text = "Options";
            this.tsm_options.Click += new System.EventHandler(this.tsm_options_Click);
            // 
            // tsm_sortDGV
            // 
            this.tsm_sortDGV.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.tsm_sortDGV.CheckOnClick = true;
            this.tsm_sortDGV.Enabled = false;
            this.tsm_sortDGV.Name = "tsm_sortDGV";
            this.tsm_sortDGV.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.tsm_sortDGV.ShowShortcutKeys = false;
            this.tsm_sortDGV.Size = new System.Drawing.Size(79, 23);
            this.tsm_sortDGV.Text = "Trier la liste";
            this.tsm_sortDGV.DropDownItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.tsm_sortRow_DropDownItemClicked);
            // 
            // tst_search
            // 
            this.tst_search.AutoSize = false;
            this.tst_search.BackColor = System.Drawing.SystemColors.Window;
            this.tst_search.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tst_search.ForeColor = System.Drawing.Color.Gray;
            this.tst_search.Name = "tst_search";
            this.tst_search.Size = new System.Drawing.Size(100, 23);
            this.tst_search.Text = "Que souhaitez-vous chercher ?";
            this.tst_search.Enter += new System.EventHandler(this.tst_search_MouseEnter);
            this.tst_search.Leave += new System.EventHandler(this.tst_search_Leave);
            this.tst_search.KeyUp += new System.Windows.Forms.KeyEventHandler(this.tst_search_KeyUp);
            this.tst_search.MouseLeave += new System.EventHandler(this.tst_search_MouseLeave);
            this.tst_search.MouseHover += new System.EventHandler(this.tst_search_MouseEnter);
            // 
            // tslb_partnersCount
            // 
            this.tslb_partnersCount.Name = "tslb_partnersCount";
            this.tslb_partnersCount.Size = new System.Drawing.Size(83, 17);
            this.tslb_partnersCount.Text = "partnersCount";
            this.tslb_partnersCount.Visible = false;
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tslb_partnersCount,
            this.tslb_elementsFound,
            this.tslb_elementsVisible});
            this.statusStrip1.Location = new System.Drawing.Point(0, 597);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(975, 22);
            this.statusStrip1.TabIndex = 0;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // tslb_elementsFound
            // 
            this.tslb_elementsFound.Name = "tslb_elementsFound";
            this.tslb_elementsFound.Size = new System.Drawing.Size(89, 17);
            this.tslb_elementsFound.Text = "elementsFound";
            this.tslb_elementsFound.Visible = false;
            // 
            // tslb_elementsVisible
            // 
            this.tslb_elementsVisible.Name = "tslb_elementsVisible";
            this.tslb_elementsVisible.Size = new System.Drawing.Size(89, 17);
            this.tslb_elementsVisible.Text = "elementsVisible";
            this.tslb_elementsVisible.Visible = false;
            // 
            // openFileDialog
            // 
            this.openFileDialog.DefaultExt = "ogepart";
            this.openFileDialog.Filter = "Fichier OGEPART|*.ogepart";
            // 
            // saveFileDialog
            // 
            this.saveFileDialog.CheckFileExists = true;
            this.saveFileDialog.DefaultExt = "ogepart";
            this.saveFileDialog.Filter = "Fichiers OGEPART|*.ogepart";
            this.saveFileDialog.RestoreDirectory = true;
            // 
            // saveNewFile
            // 
            this.saveNewFile.DefaultExt = "ogepart";
            this.saveNewFile.Filter = "Fichiers OGEPART|*.ogepart";
            this.saveNewFile.RestoreDirectory = true;
            // 
            // FormList
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(975, 619);
            this.Controls.Add(this.dgv_partners);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.tsm_menu);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.tsm_menu;
            this.Name = "FormList";
            this.Text = "OgePart";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FormList_FormClosed);
            this.Load += new System.EventHandler(this.FormList_Load);
            this.SizeChanged += new System.EventHandler(this.FormList_SizeChanged);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_partners)).EndInit();
            this.cms_cell.ResumeLayout(false);
            this.tsm_menu.ResumeLayout(false);
            this.tsm_menu.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dgv_partners;
        private System.Windows.Forms.ContextMenuStrip cms_cell;
        private System.Windows.Forms.ToolStripMenuItem tsm_copyClipboard;
        private System.Windows.Forms.ToolStripMenuItem tsm_content;
        private System.Windows.Forms.MenuStrip tsm_menu;
        private System.Windows.Forms.ToolStripMenuItem tsm_addPartner;
        private System.Windows.Forms.ToolStripMenuItem tsm_sortDGV;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripMenuItem tsm_removeEntry;
        private System.Windows.Forms.ToolStripMenuItem tsm_manageTypes;
        private System.Windows.Forms.ToolStripMenuItem tsm_sendEmail;
        private System.Windows.Forms.ToolStripMenuItem tsm_aboutThis;
        private System.Windows.Forms.ToolStripStatusLabel tslb_partnersCount;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripTextBox tst_search;
        private System.Windows.Forms.ToolStripStatusLabel tslb_elementsFound;
        private System.Windows.Forms.ToolStripStatusLabel tslb_elementsVisible;
        private System.Windows.Forms.ToolStripMenuItem tsm_operations;
        private System.Windows.Forms.ToolStripMenuItem tsm_pdf;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem tsm_options;
        private System.Windows.Forms.ToolStripMenuItem tsm_openFile;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.OpenFileDialog openFileDialog;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgc_name;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgc_idPartner;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgc_partnerType;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgc_idContact;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgc_isPublic;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgc_contact;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgc_comment;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgc_tel;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgc_email;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgc_address;
        private System.Windows.Forms.ToolStripMenuItem tsm_newFile;
        private System.Windows.Forms.ToolStripMenuItem tsm_recentsFiles;
        private System.Windows.Forms.ToolStripMenuItem tsm_emptyRecentsFiles;
        private System.Windows.Forms.SaveFileDialog saveFileDialog;
        private System.Windows.Forms.ToolStripMenuItem tsm_saveList;
        private System.Windows.Forms.SaveFileDialog saveNewFile;
    }
}

