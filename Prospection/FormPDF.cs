﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using System.IO;
using MigraDoc.DocumentObjectModel;
using MigraDoc.Rendering;
using PdfSharp;
using PdfSharp.Pdf;
using PdfSharp.Pdf.IO;

namespace Prospection
{
    public partial class FormPDF : Form
    {
        private FormList refInstance = null;
        private string[] noControlHeader = { "idPartner", "idContact", "isPublic" };

        FontPropertiesPdf headerFontProps = new FontPropertiesPdf();
        FontPropertiesPdf cellFontProps = new FontPropertiesPdf();

        private PageSize[] pageSizes = (PageSize[])Enum.GetValues(typeof(PageSize));
        private Dictionary<string, string> fontStyle = new Dictionary<string, string>();
        private FormFontDialogPdf fontDialogPdf;
        private PdfPage pagePreview;

        private bool isLoaded_Form = false;

        private string partnerType = "";
        
        private string orderBy_columnName = "";
        private string orderBy_Asc = "";

        private string groupBy_columnName = "";

        public FormPDF(FormList r, string searchText)
        {
            InitializeComponent();

            refInstance = r;

            //Commons.AssemblyDll(this, typeof(FormPDF));

            // partner type
            // On vérifie si des partenaires existent. Si non, on les obtiens à partir de la liste des partenaires
            if (refInstance.OgpFile.ListPartnersTypes.Count > 0)
            {
                foreach (PartnerType pt in refInstance.OgpFile.ListPartnersTypes)
                {
                    clb_partnersTypes.Items.Add(pt.Name, true);
                }
            }
            else
            {
                foreach (Partner p in refInstance.OgpFile.MainList_partner)
                {
                    string ptName = p.PartnerType.ToString();

                    if (clb_partnersTypes.FindStringExact(ptName) == -1)
                    {
                        clb_partnersTypes.Items.Add(ptName, true);
                    }
                }
            }

            if (searchText != refInstance.opt.PlaceHolder_searchBar)
            {
                txt_search.Text = searchText;
            }

            // éléments désactivés pour la version en cours
            txt_titlePdf.Visible = false;
            lb_titlePdf.Visible = false;

            txt_picturePath.Visible = false;
            lb_picturePath.Visible = false;
            pb_picturePdf.Visible = false;
            btn_browsePicture.Visible = false;

            ckb_horizontalCent.Visible = false;
        }

        /// <summary>
        /// When the FormPdf loads
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FormPDF_Load(object sender, EventArgs e)
        {
            for (int i = 0; i < dgv_partnersPdf.ColumnCount; i++)
            {
                if (!Array.Exists(noControlHeader, element => element == dgv_partnersPdf.Columns[i].HeaderText))
                {
                    cb_groupBy.Items.Add(dgv_partnersPdf.Columns[i].HeaderText);
                }
            }

            cb_groupBy.SelectedItem = "Structure";

            cb_Order_GroupBy.SelectedIndex = 0;

            orderBy_columnName = refInstance.comm.DataGridViewColumnName_By_HeaderText(dgv_partnersPdf, refInstance.opt.Dgv_orderBy);
            groupBy_columnName = refInstance.comm.DataGridViewColumnName_By_HeaderText(dgv_partnersPdf, cb_groupBy.SelectedItem.ToString());

            PdfProperties();
            SetProperties();

            ManageDgv_forPdf();

            isLoaded_Form = true;
        }

        #region PDF

        /// <summary>
        /// Add PDF before generated list
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_addPdfBeforeList_Click(object sender, EventArgs e)
        {
            if (openFileDialogPdf.ShowDialog() == DialogResult.OK)
            {
                txt_PdfBeforeList.Text = openFileDialogPdf.FileName;
            }
        }

        /// <summary>
        /// Add PDF after generated list
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_addPdfAfterList_Click(object sender, EventArgs e)
        {
            if (openFileDialogPdf.ShowDialog() == DialogResult.OK)
            {
                txt_PdfAfterList.Text = openFileDialogPdf.FileName;
            }
        }

        /// <summary>
        /// PDF generation
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_gen_Click(object sender, EventArgs e)
        {
            if (saveFileDialog.ShowDialog() == DialogResult.OK)
            {
                string filename = saveFileDialog.FileName;

                int j = 0;
                List<ColumnsProp> colsNames = new List<ColumnsProp>();
                Dictionary<int, string> colsDgv = new Dictionary<int, string>();
                string colName_XML = "";

                // liste des colonnes affichées
                foreach (DataGridViewColumn col in dgv_partnersPdf.Columns)
                {
                    if (Convert.ToBoolean(dgv_partnersPdf.Rows[0].Cells[col.Name].Value))
                    {
                        colsDgv.Add(col.DisplayIndex, col.Name);
                    }
                }

                // ordonnancement des colonnes par leur ordre d'affichage
                var colAscend = from pair in colsDgv orderby pair.Key ascending select pair;

                foreach (KeyValuePair<int, string> pair in colAscend)
                {
                    switch (pair.Value)
                    {
                        case "dgc_namePdf":
                            colName_XML = "NamePartner";
                            break;
                        case "dgc_partnerTypePdf":
                            colName_XML = "PartnerType";
                            break;
                        case "dgc_contactPdf":
                            colName_XML = "Contact";
                            break;
                        case "dgc_commentPdf":
                            colName_XML = "Comment";
                            break;
                        case "dgc_telPdf":
                            colName_XML = "Tel";
                            break;
                        case "dgc_emailPdf":
                            colName_XML = "Email";
                            break;
                        case "dgc_addressPdf":
                            colName_XML = "Address";
                            break;
                    }

                    colsNames.Add(new ColumnsProp(
                        j,
                        pair.Value,
                        dgv_partnersPdf.Columns[pair.Value].HeaderText,
                        colName_XML,
                        0
                        ));
                    j++;
                }

                refInstance.opt.PdfPropert.ColsNames = colsNames;

                try
                {
                    SaveProperties();

                    Pdf pdfDoc = new Pdf(dgv_partnersPdf, refInstance.opt);

                    // Create a MigraDoc document
                    Document document = pdfDoc.CreateDocument();
                    document.UseCmykColor = true;

                #if DEBUG
                    // for debugging only...
                    MigraDoc.DocumentObjectModel.IO.DdlWriter.WriteToFile(document, "MigraDoc.mdddl");
                    document = MigraDoc.DocumentObjectModel.IO.DdlReader.DocumentFromFile("MigraDoc.mdddl");
                #endif

                    // Create a renderer for PDF that uses Unicode font encoding
                    PdfDocumentRenderer pdfRenderer = new PdfDocumentRenderer(true);

                    // Set the MigraDoc document
                    pdfRenderer.Document = document;

                    // Create the PDF document
                    pdfRenderer.RenderDocument();

                    pdfRenderer = ImportPdf(pdfRenderer, txt_PdfBeforeList.Text, 0);
                    pdfRenderer = ImportPdf(pdfRenderer, txt_PdfAfterList.Text, pdfRenderer.PdfDocument.PageCount);

                    // Save the PDF document...
                    pdfRenderer.Save(filename);

                    // ...and start a viewer.
                    Process.Start(filename);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(
                        "Une erreur a eu lieu :\n" + ex.Message,
                        "",
                        MessageBoxButtons.OK,
                        MessageBoxIcon.Error
                        );

                    Console.WriteLine(ex.Message);
                    Console.ReadLine();
                }
            }
        }

        /// <summary>
        /// Add a PDF
        /// </summary>
        /// <param name="document"></param>
        /// <param name="source_filename"></param>
        /// <returns></returns>
        private PdfDocumentRenderer ImportPdf(PdfDocumentRenderer document, string source_filename, int position)
        {
            // d'après http://www.pdfsharp.net/wiki/ConcatenateDocuments-sample.ashx

            if (source_filename.Trim().Length != 0)
            {
                if (File.Exists(source_filename))
                {
                    PdfDocument PdfToAdd = PdfReader.Open(source_filename, PdfDocumentOpenMode.Import);

                    // on parcourt chaque page pour l'ajout
                    int count = PdfToAdd.PageCount;
                    for (int i = 0; i < count; i++)
                    {
                        PdfPage page = PdfToAdd.Pages[i];
                        document.PdfDocument.InsertPage(position, page);
                        position++;
                    }
                }
            }

            return document;
        }

        /// <summary>
        /// Define possibles properties
        /// </summary>
        private void PdfProperties()
        {
            // format
            cb_formatPdf.Items.AddRange(new object[] {
                PageFormat.A0,
                PageFormat.A1,
                PageFormat.A2,
                PageFormat.A3,
                PageFormat.A4,
                PageFormat.A5,
                PageFormat.A6,
                PageFormat.B5,
                PageFormat.Ledger,
                PageFormat.Legal,
                PageFormat.Letter,
                PageFormat.P11x17
            });

            cb_formatPdf.SelectedItem = PageFormat.A4;

            // orientation
            cb_orientation.Items.AddRange(new object[] {
                PageOrientation.Portrait,
                PageOrientation.Landscape
            });
            cb_orientation.SelectedIndex = 0;

            lb_selectedFontHeader.Text = string.Format("{0}, {1} pt", lb_testHeader.Font.FontFamily.Name, Math.Round(lb_testHeader.Font.Size, 0));
            lb_selectedFontCell.Text = string.Format("{0}, {1} pt", lb_testContentCell.Font.FontFamily.Name, Math.Round(lb_testContentCell.Font.Size, 0));
        }

        /// <summary>
        /// Record properties in options
        /// </summary>
        private void SaveProperties()
        {
            // margins
            refInstance.opt.PdfPropert.TopMargin = (double)num_topMargin.Value;
            refInstance.opt.PdfPropert.BottomMargin = (double)num_bottomMargin.Value;
            refInstance.opt.PdfPropert.LeftMargin = (double)num_leftMargin.Value;
            refInstance.opt.PdfPropert.RightMargin = (double)num_rightMargin.Value;

            // format
            refInstance.opt.PdfPropert.PageFormat = (PageFormat)cb_formatPdf.SelectedItem;

            refInstance.opt.PdfPropert.HorizontalCentering = ckb_horizontalCent.Checked;

            // orientation
            refInstance.opt.PdfPropert.Orientation = (PageOrientation)cb_orientation.SelectedItem;

            // font
            refInstance.opt.PdfPropert.FontHeaderProps = headerFontProps;
            refInstance.opt.PdfPropert.ForeColorHeader = lb_testHeader.ForeColor.ToArgb();
            refInstance.opt.PdfPropert.BackColorHeader = lb_testHeader.BackColor.ToArgb();

            refInstance.opt.PdfPropert.FontCellProps = cellFontProps;

            // logo
            if (File.Exists(txt_picturePath.Text.Trim()))
            {
                string picture = txt_picturePath.Text.Trim();
                refInstance.opt.PdfPropert.PicturePath = picture;

                // conversion des pixels en centimètres
                using (Bitmap bp = new Bitmap(picture)) // utilisation de "using" : http://www.dotnetdojo.com/instruction-indispensable-en-csharp-using-idisposable/
                {
                    refInstance.opt.PdfPropert.PictureHeight_Cm = bp.Height * 2.54 / bp.HorizontalResolution;
                }
            }

            // misc
            refInstance.opt.PdfPropert.Title = txt_titlePdf.Text;
            refInstance.opt.PdfPropert.IsGrouped = ckb_groupBy.Checked;
            refInstance.opt.PdfPropert.GroupBy_value = cb_groupBy.SelectedItem.ToString();
        }

        /// <summary>
        /// Get properties from options
        /// </summary>
        private void SetProperties()
        {
            if (File.Exists(Application.StartupPath + "\\" + "config.xml"))
            {
                // margins
                num_topMargin.Value = (decimal)refInstance.opt.PdfPropert.TopMargin;
                num_bottomMargin.Value = (decimal)refInstance.opt.PdfPropert.BottomMargin;
                num_leftMargin.Value = (decimal)refInstance.opt.PdfPropert.LeftMargin;
                num_rightMargin.Value = (decimal)refInstance.opt.PdfPropert.RightMargin;

                // format
                cb_formatPdf.SelectedText = refInstance.opt.PdfPropert.PageFormat.ToString();

                ckb_horizontalCent.Checked = refInstance.opt.PdfPropert.HorizontalCentering;

                // orientation
                cb_orientation.SelectedItem = (PageOrientation)refInstance.opt.PdfPropert.Orientation;

                // font
                lb_testHeader.Font = refInstance.opt.PdfPropert.FontHeaderProps.GetFont();
                lb_testHeader.ForeColor = System.Drawing.Color.FromArgb(refInstance.opt.PdfPropert.ForeColorHeader);
                lb_testHeader.BackColor = System.Drawing.Color.FromArgb(refInstance.opt.PdfPropert.BackColorHeader);

                headerFontProps = refInstance.opt.PdfPropert.FontHeaderProps;
                cellFontProps = refInstance.opt.PdfPropert.FontCellProps;

                lb_testContentCell.Font = refInstance.opt.PdfPropert.FontCellProps.GetFont();

                lb_selectedFontHeader.Text = string.Format("{0}, {1} pt", lb_testHeader.Font.FontFamily.Name, Math.Round(lb_testHeader.Font.Size, 0));
                lb_selectedFontCell.Text = string.Format("{0}, {1} pt", lb_testContentCell.Font.FontFamily.Name, Math.Round(lb_testContentCell.Font.Size, 0));
            }
        }

        #endregion

        #region Datagridview

        /// <summary>
        /// Show data in DGV
        /// </summary>
        /// <param name="searchContent"></param>
        private void ManageDgv_forPdf()
        {
            bool visibleContact = true;
            bool partnerTypeChecked = false;
            List<Partner> list = new List<Partner>();
            DataGridViewRow dgr_ckb = new DataGridViewRow();

            string lastPartnerType = "";
            string partnerType = "";

            DataGridViewRow rowCkbCell = new DataGridViewRow();

            // utilisation de cellules à cocher pour savoir quoi afficher
            // si la ligne existe, on ne l'a supprime pas
            if (dgv_partnersPdf.Rows.Count > 0)
            {
                if (dgv_partnersPdf.Rows[0].Cells[0].GetType() == typeof(DataGridViewCheckBoxCell))
                {
                    rowCkbCell = dgv_partnersPdf.Rows[0];
                }
            }

            dgv_partnersPdf.Rows.Clear();
            
            if (!string.IsNullOrWhiteSpace(txt_search.Text)) // recherche les partenaires correspondant à la recherche et retourne la liste
            {
                list = Commons.SearchInPartners(txt_search.Text, refInstance.OgpFile.MainList_partner);
            }
            else
            {
                list = refInstance.OgpFile.MainList_partner;
            }

            foreach (Partner p in list)
            {
                partnerType = p.PartnerType;

                // optimisation dans le cas où les entrées qui se suivent sont du même type de partenaire
                if (lastPartnerType != partnerType)
                {
                    partnerTypeChecked = IsChecked_PartnersTypes(partnerType);
                }

                if (partnerTypeChecked)
                {
                    lastPartnerType = partnerType;
                    // traitement de la liste, issue de la recherche ou non
                    foreach (ContactList_partner l in p.ListContact)
                    {
                        if (l.IsPublic)
                        {
                            if (!ckb_showPublicData.Checked)
                            {
                                visibleContact = false;
                            }
                        }
                        else
                        {
                            if (!ckb_showPrivateData.Checked)
                            {
                                visibleContact = false;
                            }
                        }

                        if (visibleContact)
                        {
                            dgv_partnersPdf.Rows.Add(
                                p.NamePartner,
                                p.IdPartner,
                                p.PartnerType,
                                l.IdContact,
                                l.IsPublic,
                                Commons.ReplaceCarriageReturn(l.Contact),
                                Commons.ReplaceCarriageReturn(l.Comment),
                                Commons.ReplaceCarriageReturn(l.Tel),
                                Commons.ReplaceCarriageReturn(l.Email),
                                Commons.ReplaceCarriageReturn(l.Address)
                                );
                        }

                        visibleContact = true;
                    }
                }
            }

            // tri sur 2 colonnes uniquement si le groupement est coché
            SortDGV_groupBy_OrderBy();

            if (rowCkbCell.Cells.Count == 0)
            {
                DataGridViewCheckBoxCell ckb;

                for (int i = 0; i < dgv_partnersPdf.ColumnCount; i++)
                {
                    ckb = new DataGridViewCheckBoxCell(false);
                    ckb.Value = false;
                    ckb.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;

                    // on ne coche pas certaines colonnes
                    if (!Array.Exists(noControlHeader, element => element == dgv_partnersPdf.Columns[i].HeaderText))
                    {
                        ckb.Value = true;
                    }

                    rowCkbCell.Cells.Add(ckb);
                }

            }
            dgv_partnersPdf.Rows.Insert(0, rowCkbCell);
        }

        /// <summary>
        /// Order and group data in DGV
        /// </summary>
        private void SortDGV_groupBy_OrderBy(string orderBy_override = "", string orderBy_Asc_override = "")
        {
            if (isLoaded_Form)
            {
                DataGridViewRow rowCkbCell = new DataGridViewRow();

                // utilisation de cellules à cocher pour savoir quoi afficher
                // si la ligne existe, on l'a retire pour pouvoir faire le tri
                if (dgv_partnersPdf.Rows.Count > 0)
                {
                    if (dgv_partnersPdf.Rows[0].Cells[0].GetType() == typeof(DataGridViewCheckBoxCell))
                    {
                        rowCkbCell = dgv_partnersPdf.Rows[0];
                        dgv_partnersPdf.Rows.RemoveAt(0);
                    }
                }

                // sélection du tri multi-colonne ou non
                if (ckb_groupBy.Checked)
                {
                    dgv_partnersPdf.Sort(new SortTwoCols(groupBy_columnName, cb_Order_GroupBy.SelectedItem.ToString(), orderBy_columnName, orderBy_Asc));
                }
                else // sans groupement, tri sur une seule colonne
                {
                    ListSortDirection sortDirection = ListSortDirection.Ascending;

                    if (orderBy_Asc_override == "")
                    {
                        if (dgv_partnersPdf.SortOrder != SortOrder.Ascending)
                        {
                            sortDirection = ListSortDirection.Descending;
                        }
                    }
                    else
                    {
                        if (orderBy_Asc_override != "Ascending")
                        {
                            sortDirection = ListSortDirection.Descending;
                        }
                    }

                    dgv_partnersPdf.Sort(dgv_partnersPdf.Columns[orderBy_columnName], sortDirection);
                }

                // état de base des colonnes
                foreach (DataGridViewColumn c in dgv_partnersPdf.Columns)
                {
                    c.HeaderCell.SortGlyphDirection = SortOrder.None;
                }

                if (orderBy_columnName != "")
                {
                    // modification de l'icône de tri
                    if (orderBy_Asc == SortOrder.Ascending.ToString())
                    {
                        dgv_partnersPdf.Columns[orderBy_columnName].HeaderCell.SortGlyphDirection = SortOrder.Ascending;
                    }
                    else
                    {
                        dgv_partnersPdf.Columns[orderBy_columnName].HeaderCell.SortGlyphDirection = SortOrder.Descending;
                    }
                }

                if (rowCkbCell.Cells.Count != 0)
                {
                    if (rowCkbCell.Cells[0].GetType() == typeof(DataGridViewCheckBoxCell))
                    {
                        dgv_partnersPdf.Rows.Insert(0, rowCkbCell);
                    }
                }
            }
        }

        /// <summary>
        /// Check if partner type is selected
        /// </summary>
        /// <param name="partnerType"></param>
        /// <returns></returns>
        private bool IsChecked_PartnersTypes(string partnerType)
        {
            bool r = false;

            for (int i = 0; i < clb_partnersTypes.CheckedItems.Count; i++)
            {
                if (partnerType == clb_partnersTypes.CheckedItems[i].ToString())
                {
                    r = true;
                    break;
                }
            }

            return r;
        }

        #endregion

        #region Events

        /// <summary>
        /// Event for search in partners
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txt_search_KeyUp(object sender, KeyEventArgs e)
        {
            ManageDgv_forPdf();
        }

        /// <summary>
        /// When Checked ListBox is changed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void clb_partnersTypes_ItemCheck(object sender, ItemCheckEventArgs e)
        {
            // ItemChecked ne s'active que lorsqu'on s'apprête à changer la coche, pas après.
            if (isLoaded_Form)
            {
                partnerType = ((CheckedListBox)sender).SelectedItem.ToString();
            }
        }

        /// <summary>
        /// When Checked ListBox is changed, to modify ManageDgv_forPdf();
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void clb_partnersTypes_MouseUp(object sender, MouseEventArgs e)
        {
            // en complément d'ItemCheck, cette fonction est utilisée que lorsqu'on relâche le bouton de la souris, donc après que la ligne soit cochée/décochée
            if (isLoaded_Form && partnerType != "")
            {
                ManageDgv_forPdf();
                partnerType = "";
            }
        }

        /// <summary>
        /// Check if DGV must show public or private data
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ckb_showPublicPrivateData_CheckedChanged(object sender, EventArgs e)
        {
            ManageDgv_forPdf();
        }

        /// <summary>
        /// Check/uncheck box and change forecolor cell
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgv_partnersPdf_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            bool state = false;
            System.Drawing.Color c;

            if (e.RowIndex >= 0 && e.ColumnIndex >= 0)
            {
                if (
                    (dgv_partnersPdf.Rows[e.RowIndex].Cells[e.ColumnIndex].GetType() == typeof(DataGridViewCheckBoxCell)) &&
                    (!Array.Exists(noControlHeader, element => element == dgv_partnersPdf.Columns[e.ColumnIndex].HeaderText))
                    )
                {
                    state = (bool)((DataGridViewCheckBoxCell)dgv_partnersPdf.Rows[e.RowIndex].Cells[e.ColumnIndex]).Value;

                    if (state) // si la case était coché, on l'a décoche et on applique la couleur grise
                    {
                        state = false;
                        c = System.Drawing.Color.Gray;
                    }
                    else
                    {
                        state = true;
                        c = System.Drawing.Color.Black;
                    }

                    // application des nouveaux paramètres
                    ((DataGridViewCheckBoxCell)dgv_partnersPdf.Rows[e.RowIndex].Cells[e.ColumnIndex]).Value = state;
                    dgv_partnersPdf.Columns[e.ColumnIndex].DefaultCellStyle.ForeColor = c;
                }
            }
        }

        /// <summary>
        /// Change page format + size preview
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cb_formatPdf_SelectedIndexChanged(object sender, EventArgs e)
        {
            pagePreview = new PdfPage();
            string format = cb_formatPdf.SelectedItem.ToString();

            foreach (PageSize pageSize in pageSizes)
            {
                if (pageSize == PageSize.Undefined)
                {
                    continue;
                }

                if (pageSize.ToString() == format)
                {
                    pagePreview.Size = pageSize;
                    break;
                }
            }

            lb_sizeFormat.Text = string.Format("{0} cm × {1} cm", Math.Round(pagePreview.Height.Centimeter, 1), Math.Round(pagePreview.Width.Centimeter, 1));
            refInstance.opt.PdfPropert.HeightPage = Math.Round(pagePreview.Height.Centimeter, 2);
            refInstance.opt.PdfPropert.WidthPage = Math.Round(pagePreview.Width.Centimeter, 2);
        }

        /// <summary>
        /// Select font for cell content + preview
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_fontContentCell_Click(object sender, EventArgs e)
        {
            fontDialogPdf = new FormFontDialogPdf(cellFontProps);

            if (fontDialogPdf.ShowDialog() == DialogResult.OK)
            {
                lb_testContentCell.Font = fontDialogPdf.ReturnedFont;

                cellFontProps = new FontPropertiesPdf(lb_testContentCell.Font.Name, lb_testContentCell.Font.Size, lb_testContentCell.Font.Bold, lb_testContentCell.Font.Italic, lb_testContentCell.Font.Underline);

                lb_selectedFontCell.Text = string.Format("{0}, {1} pt", fontDialogPdf.ReturnedFont.FontFamily.Name, Math.Round(fontDialogPdf.ReturnedFont.SizeInPoints, 0));
            }
        }

        /// <summary>
        /// Select font for header + preview
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_fontHeader_Click(object sender, EventArgs e)
        {
            fontDialogPdf = new FormFontDialogPdf(headerFontProps, lb_testHeader.BackColor, lb_testHeader.ForeColor, true);

            if (fontDialogPdf.ShowDialog() == DialogResult.OK)
            {
                lb_testHeader.Font = fontDialogPdf.ReturnedFont;

                headerFontProps = new FontPropertiesPdf(lb_testHeader.Font.Name, lb_testHeader.Font.Size, lb_testHeader.Font.Bold, lb_testHeader.Font.Italic, lb_testHeader.Font.Underline);

                lb_selectedFontHeader.Text = string.Format("{0}, {1} pt", fontDialogPdf.ReturnedFont.FontFamily.Name, Math.Round(fontDialogPdf.ReturnedFont.SizeInPoints, 0));
                lb_testHeader.BackColor = fontDialogPdf.BackgroundColorExample;
                lb_testHeader.ForeColor = fontDialogPdf.ForegroundColorExample;
            }
        }

        /// <summary>
        /// Record PDF properties
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FormPDF_FormClosed(object sender, FormClosedEventArgs e)
        {
            SaveProperties();
        }

        /// <summary>
        /// Activate "Group by"
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ckb_groupBy_CheckedChanged(object sender, EventArgs e)
        {
            grpb_groupBy.Enabled = ckb_groupBy.Checked;

            GroupBy_sortDGV(sender, e);
        }

        /// <summary>
        /// Sort DGV from groupBy value
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GroupBy_sortDGV(object sender, EventArgs e)
        {
            System.Drawing.Font f = new System.Drawing.Font(dgv_partnersPdf.Font, dgv_partnersPdf.Font.Style);

            foreach (DataGridViewColumn c in dgv_partnersPdf.Columns)
            {
                if (cb_groupBy.SelectedItem.ToString() == c.HeaderText && ckb_groupBy.Checked)
                {
                    groupBy_columnName = c.Name;

                    // on met la colonne de groupement en gras
                    dgv_partnersPdf.Columns[c.Name].DefaultCellStyle.Font = new System.Drawing.Font(f, f.Style | FontStyle.Bold);
                }
                else
                {
                    // retrait du gras
                    dgv_partnersPdf.Columns[c.Name].DefaultCellStyle.Font = new System.Drawing.Font(f, f.Style & FontStyle.Regular);
                }
            }

            SortDGV_groupBy_OrderBy();
        }

        /// <summary>
        /// Sort DGV from orderBy value
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OrderBy_sortDGV(object sender, DataGridViewCellMouseEventArgs e)
        {
            orderBy_columnName = dgv_partnersPdf.Columns[e.ColumnIndex].Name;

            if (orderBy_Asc == "")
            {
                orderBy_Asc = dgv_partnersPdf.SortOrder.ToString();
            }

            switch (orderBy_Asc)
            {
                case "Ascending":
                    orderBy_Asc = "Descending";
                    break;
                default:
                    orderBy_Asc = "Ascending";
                    break;
            }

            SortDGV_groupBy_OrderBy(orderBy_columnName, orderBy_Asc);
        }

        /// <summary>
        /// Refreshs DGV with the new selection order
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SortDGV_groupBy_OrderBy(object sender, EventArgs e)
        {
            SortDGV_groupBy_OrderBy();
        }

        /// <summary>
        /// Select a logo
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_browsePicture_Click(object sender, EventArgs e)
        {
            if (openFileDialogPicture.ShowDialog() == DialogResult.OK)
            {
                txt_picturePath.Text = openFileDialogPicture.FileName;
                pb_picturePdf.ImageLocation = openFileDialogPicture.FileName;
            }
        }

        #endregion
    }
}
