﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Prospection
{
    public partial class FormAboutThis : Form
    {
        private string link_sourceCode = "https://bitbucket.org/vergerk/prospection";
        private string link_license = "https://www.gnu.org/licenses/old-licenses/gpl-2.0.html";
        private string link_webSite = "www.vergerk.fr";
        private string license = "GNU GPL v2";

        /*
         * Tableau multidimensionnel
         * ==> https://msdn.microsoft.com/fr-fr/library/aa288453(v=vs.71).aspx
         * 
         * string[,] contributors = new string[,] { {"Jean-Paul A.", "2015", "http://jpa.net"}, {"Evelyne S.", "22/05/2012", "http://www.evy-dev.info"} };
         */
        private string[,] contributors = new string[,] { 
            
        };

        public FormAboutThis()
        {
            InitializeComponent();

            ResizeRectangle();

            lb_name.Text = Application.ProductName + "\n" +
                           "Outil de Gestion des Partenaires";

            rtb_credits.Text =
                Application.ProductName + " " + Application.ProductVersion + " - " + license + "\n" +
                "Développeur initial :\n" +
                "Khevin VERGER - " + link_webSite + ".\n\n" +

                "Toute modification ou/et exploitation sous une autre licence ou contraire à la licence actuelle n'est pas autorisée.\n\n" +
                "Code source : " + link_sourceCode + "\n" +
                "Licence " + license + " : " + link_license + "\n\n" +

                ShowContributors() +

                "PDFsharp / Migradoc 1.32.4334.0 - empira Software GmbH - licence MIT : http://www.pdfsharp.net/Licensing.ashx \n" +
                "Connecteur MySQL pour application .Net 6.9.9 - Oracle - licence GPL GNU v2 : https://dev.mysql.com/downloads/connector/net/";
        }

        /// <summary>
        /// Click on link
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void rtb_credits_LinkClicked(object sender, LinkClickedEventArgs e)
        {
            System.Diagnostics.Process.Start(e.LinkText);
        }

        /// <summary>
        /// Exit
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_exit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        /// <summary>
        /// Returns contributors for RichTextBox
        /// </summary>
        /// <returns></returns>
        private string ShowContributors()
        {
            string r = "";

            if (contributors.Length > 0)
            {
                r += "Contributeurs :\n";
                for (int i = 0; i < contributors.Rank ; i++)
                {
                    r += string.Format("{0} - {1} - {2}\n", contributors[i,0], contributors[i,1], contributors[i,2]);
                }

                r += "\n";
            }

            return r;
        }

        /// <summary>
        /// Open window with licence
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_license_Click(object sender, EventArgs e)
        {
            new FormLicense(Prospection.Properties.Resources.gpl_2, license).ShowDialog();
        }

        /// <summary>
        /// Forbidden the closure without reason
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FormAboutThis_FormClosing(object sender, FormClosingEventArgs e)
        {
            // Bogue : la fermeture de la fenêtre "Licence" entraine la fermeture de "A propos de ..."
            if (e.CloseReason == CloseReason.None)
            {
                e.Cancel = true;
            }
        }

        /// <summary>
        /// Resize rectangle with form width
        /// </summary>
        private void ResizeRectangle()
        {
            pb_rectangle.Width = this.Width;
        }
    }
}
