﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Prospection
{
    // d'après https://social.msdn.microsoft.com/Forums/en-US/ba506459-fcd4-43b8-8a7e-c8e9e7900f51/c-sort-a-datagridview-by-two-columns?forum=csharplanguage
    class SortTwoCols : System.Collections.IComparer
    {
        private string column_groupBy = "";
        private string column_groupBy_Asc = "Ascendant";

        private string column_orderBy = "";
        private string column_orderBy_Asc = "Ascending";

        public SortTwoCols(string col_groupBy = "", string col_groupBy_Asc = "", string col_orderBy = "", string col_orderBy_Asc = "")
        {
            column_groupBy = col_groupBy;
            column_orderBy = col_orderBy;

            if (col_groupBy_Asc.Contains("Descendant"))
            {
                column_groupBy_Asc = col_groupBy_Asc;
            }

            if (col_orderBy_Asc.Contains("Descending"))
            {
                column_orderBy_Asc = col_orderBy_Asc;
            }
        }

        public int Compare(object x, object y)
        {
            DataGridViewRow DataGridViewRow1 = (DataGridViewRow)x;
            DataGridViewRow DataGridViewRow2 = (DataGridViewRow)y;

            // Try to sort based on column_1
            int CompareResult = System.String.Compare(
                DataGridViewRow1.Cells[column_groupBy].Value.ToString(),
                DataGridViewRow2.Cells[column_groupBy].Value.ToString(),
                StringComparison.CurrentCultureIgnoreCase);

            if (column_groupBy_Asc.Contains("Descendant"))
            {
                switch (CompareResult)
                {
                    case 1:
                        CompareResult = -1;
                        break;
                    case -1:
                        CompareResult = 1;
                        break;
                }
            }

            // If column_1 are equal, sort based on column_2.
            if (
                (CompareResult == 0) &&
                (column_groupBy != column_orderBy) &&
                (column_orderBy != "")
                )
            {
                CompareResult = System.String.Compare(
                    DataGridViewRow1.Cells[column_orderBy].Value.ToString(),
                    DataGridViewRow2.Cells[column_orderBy].Value.ToString());

                if (column_orderBy_Asc.Contains("Descending"))
                {
                    switch (CompareResult)
                    {
                        case 1:
                            CompareResult = -1;
                            break;
                        case -1:
                            CompareResult = 1;
                            break;
                    }
                }
            }

            return CompareResult;
        }
    }
}
