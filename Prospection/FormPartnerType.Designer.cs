﻿namespace Prospection
{
    partial class FormPartnerType
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cb_listType = new System.Windows.Forms.ComboBox();
            this.colorDialog_partnerType = new System.Windows.Forms.ColorDialog();
            this.tb_nameType = new System.Windows.Forms.TextBox();
            this.btn_add = new System.Windows.Forms.Button();
            this.btn_modify = new System.Windows.Forms.Button();
            this.btn_delete = new System.Windows.Forms.Button();
            this.btn_addOrModify = new System.Windows.Forms.Button();
            this.btn_color = new System.Windows.Forms.Button();
            this.btn_cancelMod = new System.Windows.Forms.Button();
            this.lb_preview = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // cb_listType
            // 
            this.cb_listType.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cb_listType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cb_listType.FormattingEnabled = true;
            this.cb_listType.Location = new System.Drawing.Point(12, 12);
            this.cb_listType.Name = "cb_listType";
            this.cb_listType.Size = new System.Drawing.Size(179, 21);
            this.cb_listType.TabIndex = 0;
            this.cb_listType.SelectedIndexChanged += new System.EventHandler(this.cb_listType_SelectedIndexChanged);
            // 
            // tb_nameType
            // 
            this.tb_nameType.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.tb_nameType.Enabled = false;
            this.tb_nameType.Location = new System.Drawing.Point(12, 100);
            this.tb_nameType.Name = "tb_nameType";
            this.tb_nameType.Size = new System.Drawing.Size(179, 20);
            this.tb_nameType.TabIndex = 4;
            // 
            // btn_add
            // 
            this.btn_add.Location = new System.Drawing.Point(12, 39);
            this.btn_add.Name = "btn_add";
            this.btn_add.Size = new System.Drawing.Size(75, 23);
            this.btn_add.TabIndex = 1;
            this.btn_add.Text = "Ajouter";
            this.btn_add.UseVisualStyleBackColor = true;
            this.btn_add.Click += new System.EventHandler(this.btn_add_Click);
            // 
            // btn_modify
            // 
            this.btn_modify.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_modify.Enabled = false;
            this.btn_modify.Location = new System.Drawing.Point(93, 39);
            this.btn_modify.Name = "btn_modify";
            this.btn_modify.Size = new System.Drawing.Size(98, 23);
            this.btn_modify.TabIndex = 2;
            this.btn_modify.Text = "Modifier";
            this.btn_modify.UseVisualStyleBackColor = true;
            this.btn_modify.Click += new System.EventHandler(this.btn_modify_Click);
            // 
            // btn_delete
            // 
            this.btn_delete.Enabled = false;
            this.btn_delete.Location = new System.Drawing.Point(197, 39);
            this.btn_delete.Name = "btn_delete";
            this.btn_delete.Size = new System.Drawing.Size(75, 23);
            this.btn_delete.TabIndex = 3;
            this.btn_delete.Text = "Supprimer";
            this.btn_delete.UseVisualStyleBackColor = true;
            this.btn_delete.Click += new System.EventHandler(this.btn_delete_Click);
            // 
            // btn_addOrModify
            // 
            this.btn_addOrModify.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_addOrModify.Enabled = false;
            this.btn_addOrModify.Location = new System.Drawing.Point(169, 126);
            this.btn_addOrModify.Name = "btn_addOrModify";
            this.btn_addOrModify.Size = new System.Drawing.Size(103, 23);
            this.btn_addOrModify.TabIndex = 6;
            this.btn_addOrModify.Text = "Ajouter / modifier";
            this.btn_addOrModify.UseVisualStyleBackColor = true;
            this.btn_addOrModify.Click += new System.EventHandler(this.btn_addOrModify_Click);
            // 
            // btn_color
            // 
            this.btn_color.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_color.BackColor = System.Drawing.Color.RoyalBlue;
            this.btn_color.Enabled = false;
            this.btn_color.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_color.ForeColor = System.Drawing.Color.White;
            this.btn_color.Location = new System.Drawing.Point(197, 98);
            this.btn_color.Name = "btn_color";
            this.btn_color.Size = new System.Drawing.Size(75, 23);
            this.btn_color.TabIndex = 5;
            this.btn_color.Text = "Couleur";
            this.btn_color.UseVisualStyleBackColor = false;
            this.btn_color.Click += new System.EventHandler(this.btn_color_Click);
            // 
            // btn_cancelMod
            // 
            this.btn_cancelMod.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btn_cancelMod.Enabled = false;
            this.btn_cancelMod.Location = new System.Drawing.Point(12, 126);
            this.btn_cancelMod.Name = "btn_cancelMod";
            this.btn_cancelMod.Size = new System.Drawing.Size(83, 23);
            this.btn_cancelMod.TabIndex = 7;
            this.btn_cancelMod.Text = "Annuler";
            this.btn_cancelMod.UseVisualStyleBackColor = true;
            this.btn_cancelMod.Click += new System.EventHandler(this.btn_cancelMod_Click);
            // 
            // lb_preview
            // 
            this.lb_preview.BackColor = System.Drawing.Color.White;
            this.lb_preview.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lb_preview.ForeColor = System.Drawing.Color.Black;
            this.lb_preview.Location = new System.Drawing.Point(197, 12);
            this.lb_preview.Name = "lb_preview";
            this.lb_preview.Size = new System.Drawing.Size(75, 21);
            this.lb_preview.TabIndex = 11;
            this.lb_preview.Text = "Texte";
            this.lb_preview.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // FormPartnerType
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 161);
            this.Controls.Add(this.lb_preview);
            this.Controls.Add(this.btn_cancelMod);
            this.Controls.Add(this.btn_color);
            this.Controls.Add(this.btn_addOrModify);
            this.Controls.Add(this.btn_delete);
            this.Controls.Add(this.btn_modify);
            this.Controls.Add(this.btn_add);
            this.Controls.Add(this.tb_nameType);
            this.Controls.Add(this.cb_listType);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormPartnerType";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.Text = "Gestion des structures";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormPartnerType_FormClosing);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox cb_listType;
        private System.Windows.Forms.ColorDialog colorDialog_partnerType;
        private System.Windows.Forms.TextBox tb_nameType;
        private System.Windows.Forms.Button btn_add;
        private System.Windows.Forms.Button btn_modify;
        private System.Windows.Forms.Button btn_delete;
        private System.Windows.Forms.Button btn_addOrModify;
        private System.Windows.Forms.Button btn_color;
        private System.Windows.Forms.Button btn_cancelMod;
        private System.Windows.Forms.Label lb_preview;
    }
}