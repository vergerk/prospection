﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using MigraDoc.DocumentObjectModel;
using MigraDoc.DocumentObjectModel.Shapes;
using MigraDoc.DocumentObjectModel.Tables;
using System.Globalization;
using System.Diagnostics;
using System.IO;

namespace Prospection
{
    public class Pdf
    {
        private Document document;
        private Table table;
        private List<ColumnsProp> colsNames;
        private System.Windows.Forms.DataGridView dgv;

        private Options opt;

        private Section section = new Section();

        private PageFormat pageFormat = PageFormat.A4;
        private Orientation orientation;

        private string title;
        private string picturePath = "";

        private string groupBy_colName = "";

        // RGB colors
        private static Color TableBorder = new Color(81, 125, 192);

        private Color headerBackColor = new Color(255, 255, 255);
        private Color headerForeColor = new Color(0, 0, 0);

        public Pdf(System.Windows.Forms.DataGridView dgv, Options opt)
        {
            //Commons.AssemblyDll(this, typeof(Pdf));

            this.title = opt.PdfPropert.Title;
            this.picturePath = opt.PdfPropert.PicturePath;

            this.dgv = dgv;

            this.opt = opt;
            
            this.pageFormat = (PageFormat)opt.PdfPropert.PageFormat;
            this.orientation = (Orientation)opt.PdfPropert.Orientation;
            
            this.colsNames = opt.PdfPropert.ColsNames;

            System.Drawing.Color back = System.Drawing.Color.FromArgb(opt.PdfPropert.BackColorHeader);
            System.Drawing.Color fore = System.Drawing.Color.FromArgb(opt.PdfPropert.ForeColorHeader);
            this.headerBackColor = new Color(back.A, back.R, back.G, back.B);
            this.headerForeColor = new Color(fore.A, fore.R, fore.G, fore.B);

            if (opt.PdfPropert.IsGrouped)
            {
                this.groupBy_colName = opt.PdfPropert.GroupBy_value;
            }
        }

        public Document CreateDocument()
        {
            // Create a new MigraDoc document
            this.document = new Document();
            this.document.Info.Title = this.title;
            //this.document.Info.Subject = "Demonstrates how to create an invoice.";
            this.document.Info.Author = opt.Author;

            DefineStyles();

            CreatePage();

            FillContent();

            return this.document;
        }

        void DefineStyles()
        {
            // Get the predefined style Normal.
            Style style = this.document.Styles["Normal"];
            // Because all styles are derived from Normal, the next line changes the 
            // font of the whole document. Or, more exactly, it changes the font of
            // all styles and paragraphs that do not redefine the font.
            //style.Font.Name = opt.PdfPropert.FontHeaderProps.Name;
            //style.Font.Size = opt.PdfPropert.FontHeaderProps.Size;

            if (picturePath != "")
            {
                style = this.document.Styles[StyleNames.Header];
                style.ParagraphFormat.AddTabStop("16cm", TabAlignment.Left);
            }
            /*
            style = this.document.Styles[StyleNames.Footer];
            style.ParagraphFormat.AddTabStop("8cm", TabAlignment.Center);*/

            // Create a new style called Table based on style Normal
            style = this.document.Styles.AddStyle("Table", "Normal");
            style.Font.Name = opt.PdfPropert.FontCellProps.Name;
            style.Font.Size = opt.PdfPropert.FontCellProps.Size;

            // Create a new style called Reference based on style Normal
            style = this.document.Styles.AddStyle("Reference", "Normal");
            style.ParagraphFormat.SpaceBefore = "5mm";
            style.ParagraphFormat.SpaceAfter = "5mm";
            style.ParagraphFormat.TabStops.AddTabStop("16cm", TabAlignment.Right);
        }

        void CreatePage()
        {
            // Each MigraDoc document needs at least one section.
            section = this.document.AddSection();

            // marges
            this.section.PageSetup.TopMargin = Unit.FromCentimeter(opt.PdfPropert.TopMargin);
            this.section.PageSetup.BottomMargin = Unit.FromCentimeter(opt.PdfPropert.BottomMargin);
            this.section.PageSetup.LeftMargin = Unit.FromCentimeter(opt.PdfPropert.LeftMargin);
            this.section.PageSetup.RightMargin = Unit.FromCentimeter(opt.PdfPropert.RightMargin);

            this.section.PageSetup.PageFormat = pageFormat;
            this.section.PageSetup.Orientation = orientation;

            // logo & titre
            /*if (picturePath != "")
            {
                Image image = section.Headers.Primary.AddImage(picturePath);
                image.Height = opt.PdfPropert.PictureHeight_Cm + "cm";
                image.LockAspectRatio = true;
                image.RelativeVertical = RelativeVertical.Line;
                image.RelativeHorizontal = RelativeHorizontal.Margin;
                image.Top = ShapePosition.Top;
                image.Left = ShapePosition.Left;
                image.WrapFormat.Style = WrapStyle.TopBottom;
            }

            if (title != "")
            {
                Paragraph title_Paragr = section.Headers.Primary.AddParagraph(title);
                title_Paragr.Format.Alignment = ParagraphAlignment.Justify;
            }*/

            // Create the item table
            this.table = section.AddTable();
            this.table.Style = "Table";

            // épaisseur des bordures
            this.table.Borders.Color = TableBorder;
            this.table.Borders.Width = 0.25;
            this.table.Borders.Left.Width = 0.5;
            this.table.Borders.Right.Width = 0.5;
            this.table.Rows.LeftIndent = 0;

            // Before you can add a row, you must define the columns
            Column column;
            double widthTable = 0;
            double widthCell = 0;

            // Création des colonnes
            string groupBy_colName_DGV = ColNameDGV_by_colNameFR(groupBy_colName);

            for (int i = 0; i < dgv.ColumnCount; i++)
            {
                if (Convert.ToBoolean(dgv.Rows[0].Cells[i].Value))
                {
                    string dgv_colName = dgv.Columns[i].Name;

                    if (groupBy_colName_DGV != dgv_colName)
                    {
                        switch (dgv_colName)
                        {
                            case "dgc_namePdf":
                                widthCell = 5;
                                column = this.table.AddColumn(widthCell + "cm");
                                column.Format.Alignment = ParagraphAlignment.Left;
                                break;
                            case "dgc_partnerTypePdf":
                                widthCell = 2.5;
                                column = this.table.AddColumn(widthCell + "cm");
                                column.Format.Alignment = ParagraphAlignment.Left;
                                break;
                            case "dgc_contactPdf":
                                widthCell = 3;
                                column = this.table.AddColumn(widthCell + "cm");
                                column.Format.Alignment = ParagraphAlignment.Left;
                                break;
                            case "dgc_commentPdf":
                                widthCell = 3;
                                column = this.table.AddColumn(widthCell + "cm");
                                column.Format.Alignment = ParagraphAlignment.Left;
                                break;
                            case "dgc_telPdf":
                                widthCell = 3;
                                column = this.table.AddColumn(widthCell + "cm");
                                column.Format.Alignment = ParagraphAlignment.Left;
                                break;
                            case "dgc_emailPdf":
                                widthCell = 6;
                                column = this.table.AddColumn(widthCell + "cm");
                                column.Format.Alignment = ParagraphAlignment.Left;
                                break;
                            case "dgc_addressPdf":
                                widthCell = 5;
                                column = this.table.AddColumn(widthCell + "cm");
                                column.Format.Alignment = ParagraphAlignment.Left;
                                break;
                        }

                        widthTable += widthCell;
                    }
                }
            }

            double width = 0;
            // changement d'orientation, si la table dépasse en largeur
            if (this.section.PageSetup.Orientation == Orientation.Portrait)
            {
                width = opt.PdfPropert.WidthPage;
            }
            else
            {
                width = opt.PdfPropert.HeightPage;
            }

            double totalWidth = width - (this.section.PageSetup.LeftMargin.Centimeter + this.section.PageSetup.RightMargin.Centimeter + widthTable);
            // la largeur du format actuel n'est pas suffisante, proposition du changement d'orientation
            if (totalWidth < 0)
            {
                if(this.section.PageSetup.Orientation == Orientation.Portrait)
                {
                    System.Windows.Forms.DialogResult dr = System.Windows.Forms.MessageBox.Show(
                        "Le contenu que vous souhaitez générer dépasse des marges définies ou de la page.\n" +
                        "Souhaitez-vous passer votre document au format \"Paysage\" ?",
                        "",
                        System.Windows.Forms.MessageBoxButtons.YesNoCancel,
                        System.Windows.Forms.MessageBoxIcon.Exclamation
                        );

                    if (dr == System.Windows.Forms.DialogResult.Yes)
                    {
                        this.section.PageSetup.Orientation = Orientation.Landscape;
                        totalWidth = opt.PdfPropert.HeightPage - (this.section.PageSetup.LeftMargin.Centimeter + this.section.PageSetup.RightMargin.Centimeter + widthTable);
                    }
                    else if (dr == System.Windows.Forms.DialogResult.Cancel)
                    {
                        throw new Exception("Arrêt décidé par l'utilisateur.");
                    }
                }

                /*
                if (opt.PdfPropert.HorizontalCentering)
                {
                    this.section.PageSetup.LeftMargin = Unit.FromCentimeter(this.section.PageSetup.LeftMargin.Centimeter + (totalWidth / 2));
                    this.section.PageSetup.RightMargin = Unit.FromCentimeter(this.section.PageSetup.RightMargin.Centimeter + (totalWidth / 2));
                }*/
            }
            
            this.section.PageSetup.LeftMargin = Unit.FromCentimeter(this.section.PageSetup.LeftMargin.Centimeter + (totalWidth / 2));
            this.section.PageSetup.RightMargin = Unit.FromCentimeter(this.section.PageSetup.RightMargin.Centimeter + (totalWidth / 2));

            // on créer les en-têtes de colonnes d'après l'ordre d'affichage
            Row row = table.AddRow();
            row.HeadingFormat = true;
            row.Format.Alignment = ParagraphAlignment.Center;
            row.Shading.Color = headerBackColor;

            string fontStyleHeader = opt.PdfPropert.FontHeaderProps.Name;
            bool fontIsBold = opt.PdfPropert.FontHeaderProps.IsBold;
            bool fontIsItalic = opt.PdfPropert.FontHeaderProps.IsItalic;
            bool fontIsUnderline = opt.PdfPropert.FontHeaderProps.IsUnderline;
            int j = 0;
            double sizeStyleHeader = opt.PdfPropert.FontHeaderProps.Size;

            for (int i = 0; i < colsNames.Count; i++)
            {
                if (groupBy_colName != colsNames[i].ColName_FR)
                {
                    //row.Cells[i].AddParagraph(colsNames[i].ColName_FR);
                    row.Cells[j].AddParagraph(dgv.Columns[colsNames[i].ColName_DGV].HeaderText);
                    row.Cells[j].Format.Font.Name = fontStyleHeader;
                    row.Cells[j].Format.Font.Size = sizeStyleHeader;
                    row.Cells[j].Format.Font.Color = headerForeColor;

                    if (fontIsBold)
                    {
                        row.Cells[j].Format.Font.Bold = true;
                    }

                    if (fontIsItalic)
                    {
                        row.Cells[j].Format.Font.Italic = true;
                    }

                    if (fontIsUnderline)
                    {
                        row.Cells[j].Format.Font.Underline = Underline.Single;
                    }

                    row.Cells[j].Format.Alignment = ParagraphAlignment.Center;
                    row.Cells[j].VerticalAlignment = VerticalAlignment.Center;
                    row.Cells[j].MergeDown = 0;

                    j++;
                }
            }

            try
            {
                this.table.SetEdge(0, 0, this.table.Columns.Count, 1, Edge.Box, BorderStyle.Single, 0.75, Color.Empty);
                // col row cols rows
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        void FillContent()
        {
            // on stocke dans un dictionnaire pour pouvoir y faire appel dans l'ordre des colonnes
            string fontCell = opt.PdfPropert.FontCellProps.Name;
            bool fontIsBold = opt.PdfPropert.FontCellProps.IsBold;
            bool fontIsItalic = opt.PdfPropert.FontCellProps.IsItalic;
            bool fontIsUnderline = opt.PdfPropert.FontCellProps.IsUnderline;
            double sizeStyleCell = opt.PdfPropert.FontCellProps.Size;
            Dictionary<string, string> partnerDict = new Dictionary<string, string>();

            this.table.Style = "Table";

            partnerDict.Add("NamePartner", "");
            partnerDict.Add("PartnerType", "");
            partnerDict.Add("Contact", "");
            partnerDict.Add("Comment", "");
            partnerDict.Add("Tel", "");
            partnerDict.Add("Email", "");
            partnerDict.Add("Address", "");

            string groupBy_lastValueInDGV = "";

            foreach (System.Windows.Forms.DataGridViewRow row in dgv.Rows)
            {
                if (row.Cells[0].GetType() != typeof(System.Windows.Forms.DataGridViewCheckBoxCell))
                {
                    // on récupère la valeur qui sert au groupement et on l'isole sur une nouvelle ligne
                    if(groupBy_colName != "")
                    {
                        string groupBy_DGV_value = row.Cells[ColNameDGV_by_colNameFR(groupBy_colName)].Value.ToString();

                        // si la valeur actuelle est différente de la valeur précédente, on crée une ligne au nom de cette valeur
                        if (
                            groupBy_lastValueInDGV == "" ||
                            groupBy_lastValueInDGV != groupBy_DGV_value
                            )
                        {
                            if (groupBy_lastValueInDGV != "") // utilisé pour séparer les groupes entre eux. Créer une ligne vide sans bordure.
                            {
                                this.table.AddRow().Borders.Visible = false;
                            }

                            Row rowPartnerType = this.table.AddRow();

                            rowPartnerType.Cells[0].AddParagraph(groupBy_DGV_value);
                            rowPartnerType.Cells[0].MergeRight = this.table.Columns.Count - 1; // on fusionne avec toutes les autres cellules - la cellule de base

                            if (opt.PdfPropert.FontHeaderProps.IsBold)
                            {
                                rowPartnerType.Cells[0].Format.Font.Bold = true;
                            }
                            if (opt.PdfPropert.FontHeaderProps.IsItalic)
                            {
                                rowPartnerType.Cells[0].Format.Font.Italic = true;
                            }
                            if (opt.PdfPropert.FontHeaderProps.IsUnderline)
                            {
                                rowPartnerType.Cells[0].Format.Font.Underline = Underline.Single;
                            }

                            groupBy_lastValueInDGV = groupBy_DGV_value;
                        }
                    }

                    Row rowPartner = this.table.AddRow();
                    int j = 0;

                    for (int i = 0; i < colsNames.Count; i++)
                    {
                        if (groupBy_colName != colsNames[i].ColName_FR)
                        {
                            //rowPartner.Cells[i].AddParagraph(partnerDict[colsNames[i].ColName_XML]);
                            rowPartner.Cells[j].AddParagraph(row.Cells[colsNames[i].ColName_DGV].Value.ToString());
                            rowPartner.Cells[j].Format.Font.Name = fontCell;
                            rowPartner.Cells[j].Format.Font.Size = sizeStyleCell;

                            if (fontIsBold)
                            {
                                rowPartner.Cells[j].Format.Font.Bold = true;
                            }
                            if (fontIsItalic)
                            {
                                rowPartner.Cells[j].Format.Font.Italic = true;
                            }
                            if (fontIsUnderline)
                            {
                                rowPartner.Cells[j].Format.Font.Underline = Underline.Single;
                            }

                            j++;
                        }
                    }
                }
            }

            /*                
                // Each item fills two rows
                Row row1 = this.table.AddRow();
                Row row2 = this.table.AddRow();
                row1.TopPadding = 1.5;
                row1.Cells[0].Shading.Color = TableGray;
                row1.Cells[0].VerticalAlignment = VerticalAlignment.Center;
                row1.Cells[0].MergeDown = 1;
                row1.Cells[1].Format.Alignment = ParagraphAlignment.Left;
                row1.Cells[1].MergeRight = 3;
                row1.Cells[5].Shading.Color = TableGray;
                row1.Cells[5].MergeDown = 1;

                row1.Cells[0].AddParagraph(GetValue(item, "itemNumber"));
                paragraph = row1.Cells[1].AddParagraph();
                paragraph.AddFormattedText(GetValue(item, "title"), TextFormat.Bold);
                paragraph.AddFormattedText(" by ", TextFormat.Italic);
                paragraph.AddText(GetValue(item, "author"));
                row2.Cells[1].AddParagraph(GetValue(item, "quantity"));
                row2.Cells[2].AddParagraph(price.ToString("0.00") + " €");
                row2.Cells[3].AddParagraph(discount.ToString("0.0"));
                row2.Cells[4].AddParagraph();
                row2.Cells[5].AddParagraph(price.ToString("0.00"));
                double extendedPrice = quantity * price;
                extendedPrice = extendedPrice * (100 - discount) / 100;
                row1.Cells[5].AddParagraph(extendedPrice.ToString("0.00") + " €");
                row1.Cells[5].VerticalAlignment = VerticalAlignment.Bottom;
                totalExtendedPrice += extendedPrice;
                
                this.table.SetEdge(0, this.table.Rows.Count - 2, 6, 2, Edge.Box, BorderStyle.Single, 0.75);
            }*/

            // Add an invisible row as a space line to the table
            /*Row row = this.table.AddRow();
            row.Borders.Visible = false;*/

            try
            {
                this.table.SetEdge(0, 0, this.table.Columns.Count, this.table.Rows.Count, Edge.Box, BorderStyle.Single, 0.75);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        /// <summary>
        /// Retursn the DGV column name from GUI name
        /// </summary>
        /// <param name="colNameFR"></param>
        /// <returns></returns>
        private string ColNameDGV_by_colNameFR(string colNameFR)
        {
            string colNameDGV = "";

            foreach (ColumnsProp cp in colsNames)
            {
                if (cp.ColName_FR == colNameFR)
                {
                    colNameDGV = cp.ColName_DGV;
                    break;
                }
            }

            return colNameDGV;
        }
    }
}
