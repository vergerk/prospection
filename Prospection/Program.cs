﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Prospection
{
    static class Program
    {
        /// <summary>
        /// Point d'entrée principal de l'application.
        /// </summary>
        [STAThread]
        static void Main(string[] args)
        {
            string file = "";
            string ext = "." + Options.ExtensionFile;

            // https://stackoverflow.com/questions/4908955/how-do-i-open-a-file-with-my-application
            // http://www.ubercode.com/write-programs-for-windows-api.html

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            FormList l = new FormList();

            // vérifie si l'application a été ouverte par un fichier et envoie le chemin à FormList
            foreach (string f in args)
            {
                if (File.Exists(f))
                {
                    file = f;

                    if (f.Contains(ext))
                    {
                        file = @f.Replace(ext, "");
                    }

                    l.DefaultFileName = file;
                    break;
                }
            }

            Application.Run(l);
        }
    }
}
