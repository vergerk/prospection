﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prospection
{
    public class ContactList_partner
    {
        private string idContact;
        private bool isPublic;
        private string contact;
        private string comment;
        private string tel;
        private string email;
        private string address;
        
        #region Accesseurs

        public string IdContact
        {
            get { return idContact; }
            set { idContact = value; }
        }

        public bool IsPublic
        {
            get { return isPublic; }
            set { isPublic = value; }
        }

        public string Contact
        {
            get { return contact; }
            set { contact = value; }
        }

        public string Comment
        {
            get { return comment; }
            set { comment = value; }
        }

        public string Tel
        {
            get { return tel; }
            set { tel = value; }
        }

        public string Email
        {
            get { return email; }
            set { email = value; }
        }

        public string Address
        {
            get { return address; }
            set { address = value; }
        }

        #endregion

        public ContactList_partner()
        {
            idContact = "";
            this.isPublic = false;
            contact = "";
            comment = "";
            tel = "";
            email = "";
            address = "";
        }

        public ContactList_partner(bool isPublic)
        {
            idContact = Commons.RandomId();
            this.isPublic = isPublic;
            contact = "";
            comment = "";
            tel = "";
            email = "";
            address = "";
        }

        public ContactList_partner(bool isPublic, string contact, string comment, string tel, string email, string address)
        {
            idContact = Commons.RandomId();
            this.isPublic = isPublic;
            this.contact = contact;
            this.comment = comment;
            this.tel = tel;
            this.email = email;
            this.address = address;
        }

        public ContactList_partner(string idContact, bool isPublic, string contact, string comment, string tel, string email, string address)
        {
            this.idContact = idContact;
            this.isPublic = isPublic;
            this.contact = contact;
            this.comment = comment;
            this.tel = tel;
            this.email = email;
            this.address = address;
        }
    }
}
