﻿namespace Prospection
{
    partial class FormPartner
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormPartner));
            this.label1 = new System.Windows.Forms.Label();
            this.tb_namePartner = new System.Windows.Forms.TextBox();
            this.btn_savePartner = new System.Windows.Forms.Button();
            this.btn_clear = new System.Windows.Forms.Button();
            this.dgv_listeContact = new System.Windows.Forms.DataGridView();
            this.dgc_idContact = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgc_isPublic = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.dgc_contact = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgc_comment = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgc_tel = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgc_email = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgc_address = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btn_addPartner = new System.Windows.Forms.Button();
            this.cb_partnerType = new System.Windows.Forms.ComboBox();
            this.lb_partnerType = new System.Windows.Forms.Label();
            this.btn_addPartnerType = new System.Windows.Forms.Button();
            this.btn_addPartnerType_def = new System.Windows.Forms.Button();
            this.lb_confirmSavePartner = new System.Windows.Forms.Label();
            this.tm_Partner = new System.Windows.Forms.Timer(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_listeContact)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(29, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Nom";
            // 
            // tb_namePartner
            // 
            this.tb_namePartner.Location = new System.Drawing.Point(79, 12);
            this.tb_namePartner.MaxLength = 30;
            this.tb_namePartner.Name = "tb_namePartner";
            this.tb_namePartner.Size = new System.Drawing.Size(212, 20);
            this.tb_namePartner.TabIndex = 0;
            // 
            // btn_savePartner
            // 
            this.btn_savePartner.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_savePartner.Location = new System.Drawing.Point(817, 397);
            this.btn_savePartner.Name = "btn_savePartner";
            this.btn_savePartner.Size = new System.Drawing.Size(104, 23);
            this.btn_savePartner.TabIndex = 6;
            this.btn_savePartner.Text = "Enregistrer";
            this.btn_savePartner.UseVisualStyleBackColor = true;
            this.btn_savePartner.Click += new System.EventHandler(this.btn_savePartner_Click);
            // 
            // btn_clear
            // 
            this.btn_clear.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btn_clear.Location = new System.Drawing.Point(12, 397);
            this.btn_clear.Name = "btn_clear";
            this.btn_clear.Size = new System.Drawing.Size(104, 23);
            this.btn_clear.TabIndex = 7;
            this.btn_clear.Text = "Effacer la saisie";
            this.btn_clear.UseVisualStyleBackColor = true;
            this.btn_clear.Click += new System.EventHandler(this.ClearData);
            // 
            // dgv_listeContact
            // 
            this.dgv_listeContact.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgv_listeContact.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgv_listeContact.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dgv_listeContact.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_listeContact.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dgc_idContact,
            this.dgc_isPublic,
            this.dgc_contact,
            this.dgc_comment,
            this.dgc_tel,
            this.dgc_email,
            this.dgc_address});
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgv_listeContact.DefaultCellStyle = dataGridViewCellStyle1;
            this.dgv_listeContact.Location = new System.Drawing.Point(12, 38);
            this.dgv_listeContact.Name = "dgv_listeContact";
            this.dgv_listeContact.Size = new System.Drawing.Size(909, 353);
            this.dgv_listeContact.TabIndex = 4;
            this.dgv_listeContact.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this.dgv_listeContact_EditingControlShowing);
            this.dgv_listeContact.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dgv_listeContact_KeyDown);
            // 
            // dgc_idContact
            // 
            this.dgc_idContact.HeaderText = "idContact";
            this.dgc_idContact.Name = "dgc_idContact";
            this.dgc_idContact.ReadOnly = true;
            this.dgc_idContact.Visible = false;
            // 
            // dgc_isPublic
            // 
            this.dgc_isPublic.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.dgc_isPublic.FalseValue = "false";
            this.dgc_isPublic.HeaderText = "Public ?";
            this.dgc_isPublic.Name = "dgc_isPublic";
            this.dgc_isPublic.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dgc_isPublic.TrueValue = "true";
            this.dgc_isPublic.Width = 60;
            // 
            // dgc_contact
            // 
            this.dgc_contact.HeaderText = "Contact";
            this.dgc_contact.Name = "dgc_contact";
            // 
            // dgc_comment
            // 
            this.dgc_comment.HeaderText = "Commentaire";
            this.dgc_comment.Name = "dgc_comment";
            // 
            // dgc_tel
            // 
            this.dgc_tel.HeaderText = "Téléphone";
            this.dgc_tel.Name = "dgc_tel";
            // 
            // dgc_email
            // 
            this.dgc_email.HeaderText = "Courriel";
            this.dgc_email.Name = "dgc_email";
            // 
            // dgc_address
            // 
            this.dgc_address.HeaderText = "Adresse";
            this.dgc_address.Name = "dgc_address";
            // 
            // btn_addPartner
            // 
            this.btn_addPartner.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_addPartner.Location = new System.Drawing.Point(707, 397);
            this.btn_addPartner.Name = "btn_addPartner";
            this.btn_addPartner.Size = new System.Drawing.Size(104, 23);
            this.btn_addPartner.TabIndex = 5;
            this.btn_addPartner.Text = "Ajouter";
            this.btn_addPartner.UseVisualStyleBackColor = true;
            this.btn_addPartner.Click += new System.EventHandler(this.btn_addPartner_Click);
            // 
            // cb_partnerType
            // 
            this.cb_partnerType.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cb_partnerType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cb_partnerType.FormattingEnabled = true;
            this.cb_partnerType.Location = new System.Drawing.Point(722, 12);
            this.cb_partnerType.Name = "cb_partnerType";
            this.cb_partnerType.Size = new System.Drawing.Size(168, 21);
            this.cb_partnerType.TabIndex = 2;
            // 
            // lb_partnerType
            // 
            this.lb_partnerType.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lb_partnerType.AutoSize = true;
            this.lb_partnerType.Location = new System.Drawing.Point(666, 15);
            this.lb_partnerType.Name = "lb_partnerType";
            this.lb_partnerType.Size = new System.Drawing.Size(50, 13);
            this.lb_partnerType.TabIndex = 36;
            this.lb_partnerType.Text = "Structure";
            // 
            // btn_addPartnerType
            // 
            this.btn_addPartnerType.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_addPartnerType.Enabled = false;
            this.btn_addPartnerType.Location = new System.Drawing.Point(722, 12);
            this.btn_addPartnerType.Name = "btn_addPartnerType";
            this.btn_addPartnerType.Size = new System.Drawing.Size(199, 21);
            this.btn_addPartnerType.TabIndex = 1;
            this.btn_addPartnerType.Text = "Ajouter une structure";
            this.btn_addPartnerType.UseVisualStyleBackColor = true;
            this.btn_addPartnerType.Visible = false;
            this.btn_addPartnerType.Click += new System.EventHandler(this.btn_addPartnerType_Click);
            // 
            // btn_addPartnerType_def
            // 
            this.btn_addPartnerType_def.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_addPartnerType_def.Enabled = false;
            this.btn_addPartnerType_def.Location = new System.Drawing.Point(896, 12);
            this.btn_addPartnerType_def.Name = "btn_addPartnerType_def";
            this.btn_addPartnerType_def.Size = new System.Drawing.Size(25, 21);
            this.btn_addPartnerType_def.TabIndex = 3;
            this.btn_addPartnerType_def.Text = "+";
            this.btn_addPartnerType_def.UseVisualStyleBackColor = true;
            this.btn_addPartnerType_def.Visible = false;
            this.btn_addPartnerType_def.Click += new System.EventHandler(this.btn_addPartnerType_Click);
            // 
            // lb_confirmSavePartner
            // 
            this.lb_confirmSavePartner.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lb_confirmSavePartner.AutoEllipsis = true;
            this.lb_confirmSavePartner.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.lb_confirmSavePartner.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lb_confirmSavePartner.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb_confirmSavePartner.ForeColor = System.Drawing.SystemColors.ControlText;
            this.lb_confirmSavePartner.Location = new System.Drawing.Point(122, 397);
            this.lb_confirmSavePartner.Name = "lb_confirmSavePartner";
            this.lb_confirmSavePartner.Size = new System.Drawing.Size(579, 23);
            this.lb_confirmSavePartner.TabIndex = 38;
            this.lb_confirmSavePartner.Text = "Partenaire modifié";
            this.lb_confirmSavePartner.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lb_confirmSavePartner.Visible = false;
            // 
            // tm_Partner
            // 
            this.tm_Partner.Tick += new System.EventHandler(this.tm_Partner_Tick);
            // 
            // FormPartner
            // 
            this.AcceptButton = this.btn_savePartner;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ClientSize = new System.Drawing.Size(933, 432);
            this.Controls.Add(this.lb_confirmSavePartner);
            this.Controls.Add(this.btn_addPartnerType);
            this.Controls.Add(this.btn_addPartnerType_def);
            this.Controls.Add(this.lb_partnerType);
            this.Controls.Add(this.cb_partnerType);
            this.Controls.Add(this.dgv_listeContact);
            this.Controls.Add(this.btn_clear);
            this.Controls.Add(this.btn_addPartner);
            this.Controls.Add(this.btn_savePartner);
            this.Controls.Add(this.tb_namePartner);
            this.Controls.Add(this.label1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FormPartner";
            this.Text = "Fiche partenaire";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormPartner_FormClosing);
            this.Load += new System.EventHandler(this.FormPartner_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_listeContact)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tb_namePartner;
        private System.Windows.Forms.Button btn_savePartner;
        private System.Windows.Forms.Button btn_clear;
        private System.Windows.Forms.DataGridView dgv_listeContact;
        private System.Windows.Forms.Button btn_addPartner;
        private System.Windows.Forms.ComboBox cb_partnerType;
        private System.Windows.Forms.Label lb_partnerType;
        private System.Windows.Forms.Button btn_addPartnerType;
        private System.Windows.Forms.Button btn_addPartnerType_def;
        private System.Windows.Forms.Label lb_confirmSavePartner;
        private System.Windows.Forms.Timer tm_Partner;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgc_idContact;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dgc_isPublic;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgc_contact;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgc_comment;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgc_tel;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgc_email;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgc_address;
    }
}