﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Serialization;
using System.Reflection;
using System.Xml;

namespace Prospection
{
    public partial class FormList : Form
    {
        private OgepartFile ogpFile = new OgepartFile();
        private FormPartner partner = null;

        private string idRemovePartner = null;
        private string defaultFileName = "";

        public Commons comm;
        public Options opt = new Options();

        private string dataContext = null;
        private string orderBy_columnName = "";

        #region Accesseurs

        public string DefaultFileName
        {
            set { defaultFileName = value; }
        }

        internal OgepartFile OgpFile
        {
            get { return ogpFile; }
            set { ogpFile = value; }
        }

        #endregion        

        #region code formulaire

        public FormList()
        {
            InitializeComponent();

            comm = new Commons(this);
        }

        private void FormList_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (defaultFileName == "")
            {
                if (saveNewFile.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    SaveFiles(saveNewFile.FileName);
                }
            }
            else
            {
                SaveFiles(defaultFileName);
            }
        }

        private void FormList_Load(object sender, EventArgs e)
        {
            opt = Commons.LoadFile(typeof(Options), Application.StartupPath + "\\" + "config", opt, "xml") as Options;
            //mainList_partner = Commons.LoadFile(typeof(List<Partner>), defaultFileName, mainList_partner, Options.ExtensionFile) as List<Partner>;

            //this.Text = string.Format("{0} - {1}", Application.ProductName, defaultFileName);
            titleForm();

            StretchSearchTextBox();

            orderBy_columnName = comm.DataGridViewColumnName_By_HeaderText(dgv_partners, opt.Dgv_orderBy);

            ManageRecentsFiles();
            /*TsmPartnersTypes();
            ManageDgv();
            EntriesCount();*/
        }

        /// <summary>
        /// Set title
        /// </summary>
        /// <param name="title"></param>
        private void titleForm(string title = "")
        {
            if (title == "")
            {
                this.Text = Application.ProductName;
            }
            else
            {
                this.Text = string.Format("{0} - {1}", Application.ProductName, title);
            }
        }

        #endregion

        #region FormPartner
        /// <summary>
        /// Add partner in DataGridView (used by FormPartner)
        /// </summary>
        /// <param name="aAjouter"></param>
        public void AddingPartner(Partner aAjouter)
        {
            OgpFile.MainList_partner.Add(aAjouter);

            for (int i = 0; i < aAjouter.ListContact.Count; i++)
            {
                dgv_partners.Rows.Add(
                    aAjouter.NamePartner,
                    aAjouter.IdPartner,
                    aAjouter.PartnerType,
                    aAjouter.ListContact[i].IdContact,
                    aAjouter.ListContact[i].IsPublic,
                    aAjouter.ListContact[i].Contact,
                    aAjouter.ListContact[i].Comment,
                    aAjouter.ListContact[i].Tel,
                    aAjouter.ListContact[i].Email,
                    aAjouter.ListContact[i].Address
                    );
            }

            EntriesCount();
        }

        /// <summary>
        /// Edit partner in DataGridView (used by FormPartner)
        /// </summary>
        /// <param name="aModifier"></param>
        public void EditingPartner(Partner aModifier)
        {
            OgpFile.MainList_partner.Remove(SelectPartner(aModifier.IdPartner));
            OgpFile.MainList_partner.Add(aModifier);

            ManageDgv();
        }
        
        /// <summary>
        /// Resize search bar for stretch
        /// </summary>
        public void StretchSearchTextBox()
        {
            int width = tsm_menu.DisplayRectangle.Width;

            for (int i = 0; i < tsm_menu.Items.Count; i++)
            {
                if (tsm_menu.Items[i].GetType() == typeof(ToolStripMenuItem))
                {
                    width -= tsm_menu.Items[i].Width;
                }
            }

            // -1 car sinon le champ ne s'affiche pas
            tst_search.Width = width - 1;
        }

        /// <summary>
        /// For resize the search bar (event)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FormList_SizeChanged(object sender, EventArgs e)
        {
            StretchSearchTextBox();
        }

        #endregion

        #region DataGridView
        /// <summary>
        /// Manage DataGridView
        /// </summary>
        public void ManageDgv()
        {
            if (ogpFile.MainList_partner.Count > 0)
            {
                int visibleCount = 0;
                bool visiblePartner = true;
                bool visibleContact = true;
                bool? partnerTypeReturned = false;

                bool publicChecked = (bool)ogpFile.CheckControlStatePT("tsm_showPublicData");
                bool privateChecked = (bool)ogpFile.CheckControlStatePT("tsm_showPrivateData");

                string searchContent = "";

                List<Partner> list = new List<Partner>();
                PartnerType pt;

                // recherche d'éléments
                if (
                    !string.IsNullOrWhiteSpace(tst_search.Text) &&
                    tst_search.Text != opt.PlaceHolder_searchBar)
                {
                    searchContent = tst_search.Text;
                }

                dgv_partners.Rows.Clear();

                if (searchContent != "") // recherche les partenaires correspondant à la recherche et retourne la liste
                {
                    list = Commons.SearchInPartners(searchContent, OgpFile.MainList_partner);

                    int listCount = list.Count;
                    tslb_elementsFound.Visible = true;

                    switch (listCount)
                    {
                        case 0:
                            tslb_elementsFound.Text = "La recherche n'a retourné aucun élément.";
                            break;
                        case 1:
                            tslb_elementsFound.Text = "1 partenaire affiché.";
                            break;
                        default:
                            tslb_elementsFound.Text = listCount + " partenaires affichés.";
                            break;
                    }
                }
                else
                {
                    tslb_elementsFound.Visible = false;
                    list = OgpFile.MainList_partner;
                }

                // https://stackoverflow.com/questions/10063770/how-to-add-a-new-row-to-datagridview-programmatically
                foreach (Partner p in list)
                {
                    partnerTypeReturned = ogpFile.CheckControlStatePT("tsm_" + p.PartnerType);

                    if (partnerTypeReturned == null)
                    {
                        // on regarde si le type de partenaire (qui a pas été crée) a déjà été ajouté
                        if (!opt.CheckComboBoxPT(p.PartnerType, tsm_sortDGV))
                        {
                            AddItemMenu("(" + p.PartnerType + ")", p.PartnerType, Color.Black, Color.White, false);
                        }
                        partnerTypeReturned = true;
                    }

                    visiblePartner = (bool)partnerTypeReturned;

                    // traitement de la liste, issue de la recherche ou non
                    foreach (ContactList_partner l in p.ListContact)
                    {
                        dgv_partners.Rows.Add(
                            p.NamePartner,
                            p.IdPartner,
                            p.PartnerType,
                            l.IdContact,
                            l.IsPublic,
                            Commons.ReplaceCarriageReturn(l.Contact),
                            Commons.ReplaceCarriageReturn(l.Comment),
                            Commons.ReplaceCarriageReturn(l.Tel),
                            Commons.ReplaceCarriageReturn(l.Email),
                            Commons.ReplaceCarriageReturn(l.Address)
                            );

                        if (l.IsPublic)
                        {
                            if (!publicChecked)
                            {
                                visibleContact = false;
                            }
                        }
                        else
                        {
                            if (!privateChecked)
                            {
                                visibleContact = false;
                            }
                        }

                        dgv_partners.Rows[dgv_partners.Rows.GetLastRow(DataGridViewElementStates.Visible)].Visible = (visiblePartner && visibleContact);

                        if (!visiblePartner || !visibleContact)
                        {
                            visibleCount++;
                        }


                        pt = SelectPartnerType(p.PartnerType);
                        if (pt != null)
                        {
                            dgv_partners.Rows[dgv_partners.RowCount - 1].DefaultCellStyle.BackColor = Color.FromArgb(pt.Backcolor);
                            dgv_partners.Rows[dgv_partners.RowCount - 1].DefaultCellStyle.ForeColor = Color.FromArgb(pt.Forecolor);
                        }

                        visibleContact = true;
                    }

                    visiblePartner = true;
                }

                VisibleRows(visibleCount);
                EntriesCount();

                if (orderBy_columnName != "")
                {
                    ListSortDirection sortDir = ListSortDirection.Ascending;

                    if (opt.Dgv_orderByASC == SortOrder.Descending)
                    {
                        sortDir = ListSortDirection.Descending;
                    }

                    dgv_partners.Sort(dgv_partners.Columns[orderBy_columnName], sortDir);
                }
            }
        }

        /// <summary>
        /// Returns the received partner in argument to FormPartner
        /// </summary>
        /// <param name="p"></param>
        private void EditPartner(Partner p)
        {
            partner = new FormPartner(p, this);
            partner.Show();

            this.Hide();
        }

        /// <summary>
        /// Remove partner to DGV + list of partners
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void RemovePartner(object sender, EventArgs e)
        {
            string name = null;
            bool isValid = false;

            // cas où l'utilisateur appuie sur SUPPR, l'id du partner figure dans le sender
            if (sender.GetType().Name == "DataGridView")
            {
                idRemovePartner = ((DataGridView)sender).CurrentRow.Cells["dgc_idPartner"].Value.ToString();
                name = ((DataGridView)sender).CurrentRow.Cells["dgc_name"].Value.ToString();
                isValid = true;
            }

            if (idRemovePartner != null)
            {
                if (name == null)
                {
                    foreach (DataGridViewRow r in dgv_partners.Rows)
                    {
                        if (r.Cells["dgc_idPartner"].Value.ToString() == idRemovePartner)
                        {
                            name = r.Cells["dgc_name"].Value.ToString();
                            isValid = true;

                            break;
                        }
                    }
                }

                if (isValid)
                {
                    DialogResult choix = MessageBox.Show(
                        "Voulez-vous vraiment supprimer " + name + " ?\n" +
                        "Toute suppression est définitive !\n\n" +

                        "Si vous souhaitez supprimer une ligne de contact, annulez cette opération et éditez ce partenaire (double-clic)."
                        , "Confirmation de suppression", MessageBoxButtons.OKCancel, MessageBoxIcon.Question);

                    if (choix == DialogResult.OK)
                    {
                        OgpFile.MainList_partner.Remove(SelectPartner(idRemovePartner));

                        ManageDgv();
                    }
                }

                EntriesCount();
            }
        }

        #endregion

        #region Number of partners

        /// <summary>
        /// Display count of partners recorded
        /// </summary>
        private void EntriesCount()
        {
            int partnersCount = OgpFile.MainList_partner.Count;

            switch (partnersCount)
            {
                case 0:
                    tslb_partnersCount.Text = "Aucun partenaire.";
                    break;
                case 1:
                    tslb_partnersCount.Text = "1 partenaire enregistrés.";
                    break;
                default:
                    tslb_partnersCount.Text = partnersCount.ToString() + " partenaires enregistrés.";
                    break;
            }
        }

        /// <summary>
        /// Display count of partners visible
        /// </summary>
        /// <param name="visibleCount"></param>
        private void VisibleRows(int visibleCount)
        {
            switch (visibleCount)
            {
                case 0:
                    tslb_elementsVisible.Text = "";
                    break;
                case 1:
                    tslb_elementsVisible.Text = "1 entrée masquée.";
                    break;
                default:
                    tslb_elementsVisible.Text = visibleCount + " entrées masquées.";
                    break;
            }
        }

        #endregion

        #region Sorting partners

        /// <summary>
        /// Add/refresh a type of partner
        /// </summary>
        /// <param name="partnerTypeName"></param>
        /// <param name="c"></param>
        public void TsmPartnersTypes()
        {
            tsm_sortDGV.Enabled = true;

            tsm_sortDGV.DropDownItems.Clear();

            // données publiques
            AddItemMenu("Afficher les entrées publiques", "showPublicData");

            // données privées
            AddItemMenu("Afficher les entrées privées", "showPrivateData");

            tsm_sortDGV.DropDownItems.Add(new ToolStripSeparator());

            // tri par défaut
            AddItemMenu("Tri par défaut des entrées", "defaultSort");

            foreach (PartnerType p in ogpFile.ListPartnersTypes)
            {
                AddItemMenu(p.Name, p.Name, Color.FromArgb(p.Backcolor), Color.FromArgb(p.Forecolor));
            }
        }

        /// <summary>
        /// Add an item in menu
        /// </summary>
        /// <param name="text"></param>
        /// <param name="name"></param>
        /// <param name="backcolor"></param>
        /// <param name="forecolor"></param>
        private void AddItemMenu(string text, string name, Color? backcolor = null, Color? forecolor = null, bool toSave = true)
        {
            if (backcolor == null)
            {
                backcolor = SystemColors.Window;
            }

            if (forecolor == null)
            {
                forecolor = Color.Black;
            }

            ToolStripMenuItem item = new ToolStripMenuItem();

            if (name != "defaultSort")
            {
                // coché par défaut, permet le changement
                item.CheckState = CheckState.Checked;
                item.CheckOnClick = true;

                item.Checked = true;
                bool? checkedInSave = ogpFile.CheckControlStatePT("tsm_" + name);

                // on regarde l'état de l'élément sauvegardé auparavant. Si inexistant, on le rajoute
                if (checkedInSave != null)
                {
                    item.Checked = (bool)checkedInSave;
                }
                else
                {
                    // cas des types de partenaires qui existent en entrée mais pas en type. Ils ne sont pas sauvegardés ni décochables.
                    if (toSave)
                    {
                        ogpFile.AddControlPT("tsm_" + name, true);
                    }
                    else
                    {
                        item.CheckOnClick = false;
                    }
                }
            }

            item.Text = text;
            item.Name = "tsm_" + name;

            item.BackColor = (Color)backcolor;
            item.ForeColor = (Color)forecolor;

            item.Click += new EventHandler(tsm_sortRows_Click);
            item.MouseLeave += new EventHandler(tsm_sortRowClose);

            tsm_sortDGV.DropDownItems.Add(item);
        }

        /// <summary>
        /// Refreshs dgv_partners
        /// </summary>
        public void RefreshDGV()
        {
            ManageDgv();
        }

        #endregion

        #region Management of partners

        /// <summary>
        /// Returns a partner
        /// </summary>
        /// <param name="idPartner"></param>
        /// <returns></returns>
        private Partner SelectPartner(string idPartner)
        {
            Partner p = null;

            foreach (Partner p_a in OgpFile.MainList_partner)
            {
                if (p_a.IdPartner == idPartner)
                {
                    p = p_a;
                    break;
                }
            }

            return p;
        }

        /// <summary>
        /// Returns a partner type
        /// </summary>
        /// <param name="partnerType"></param>
        /// <returns></returns>
        private PartnerType SelectPartnerType(string partnerType)
        {
            PartnerType pt = null;

            foreach (PartnerType list_pt in ogpFile.ListPartnersTypes)
            {
                if (list_pt.Name == partnerType)
                {
                    pt = list_pt;
                    break;
                }
            }

            return pt;
        }

        #endregion

        #region Events
        /// <summary>
        /// Show FormPartner
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tsm_addPartner_Click(object sender, EventArgs e)
        {
            partner = new FormPartner(this);
            partner.Show();

            this.Hide();
        }

        /// <summary>
        /// Retrieve location of double-clicked partner to EditPartner
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dg_partnersList_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0)
            {
                EditPartner(SelectPartner(dgv_partners.Rows[e.RowIndex].Cells["dgc_idPartner"].Value.ToString()));
            }
        }

        /// <summary>
        /// For send email
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tsm_sendEmail_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start("mailto:" + tsm_content.Text);
        }

        /// <summary>
        /// Copy in clipboard
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cms_cell_Click(object sender, EventArgs e)
        {
            if (dataContext.Length > 0)
            {
                Clipboard.SetText(dataContext);
            }
        }

        /// <summary>
        /// Retrieve and stock pointed data by cursor coords
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dg_partnersList_MouseDown(object sender, MouseEventArgs e)
        {
            var click = dgv_partners.HitTest(e.X, e.Y);
            int col = click.ColumnIndex;
            int row = click.RowIndex;

            tsm_sendEmail.Visible = false;

            // on vérifie qu'on a bien cliqué sur une colonne et une ligne
            if (
                ((col >= 0) && (col <= dgv_partners.ColumnCount)) &&
                ((row >= 0) && (row <= dgv_partners.RowCount))
                )
            {
                // sélectionne la cellule cliquée
                dgv_partners.Rows[row].Cells[col].Selected = true;

                // si clic droit
                if (e.Button == MouseButtons.Right)
                {
                    dataContext = dgv_partners.Rows[row].Cells[col].Value.ToString();

                    if (dataContext.Length > 0)
                    {
                        RegexUtilities util = new RegexUtilities();

                        if (util.IsValidEmail(dataContext))
                        {
                            tsm_sendEmail.Visible = true;
                        }

                        tsm_content.Text = dataContext;
                    }
                    else
                    {
                        tsm_content.Text = "(aucun contenu)";
                    }

                    idRemovePartner = dgv_partners.Rows[row].Cells["dgc_idPartner"].Value.ToString();
                }
            }
        }

        /// <summary>
        /// Show / hide DataGridViewRow
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tsm_sortRows_Click(object sender, EventArgs e)
        {
            ToolStripMenuItem o = (ToolStripMenuItem)sender;

            if (o.Text == "Tri par défaut des entrées")
            {
                dgv_partners.Sort(dgv_partners.Columns["dgc_name"], ListSortDirection.Ascending);

                for (int i = 0; i < tsm_sortDGV.DropDownItems.Count; i++)
                {
                    if (tsm_sortDGV.DropDownItems[i].GetType() == typeof(ToolStripMenuItem))
                    {
                        if (((ToolStripMenuItem)tsm_sortDGV.DropDownItems[i]).CheckOnClick)
                        {
                            ((ToolStripMenuItem)tsm_sortDGV.DropDownItems[i]).Checked = true;
                            ogpFile.ModifyState(tsm_sortDGV.DropDownItems[i].Name, true);
                        }
                    }
                }

                ManageDgv();
            }
            else if (o.CheckOnClick == true)
            {
                // sauvegarde la modification de coche
                if (o.GetType() == typeof(ToolStripMenuItem))
                {
                    ogpFile.ModifyState(o.Name, o.Checked);
                }

                bool isPublic;
                bool partnerTypeChecked = true;
                bool? partnerTypeExist = true;
                bool showPublicData = (bool)ogpFile.CheckControlStatePT("tsm_showPublicData");
                bool showPrivateData = (bool)ogpFile.CheckControlStatePT("tsm_showPrivateData");
                int visibleCount = 0;

                foreach (DataGridViewRow p in dgv_partners.Rows)
                {
                    // on contrôle que le type de partenaire existe dans la liste. Si non (null), on l'affiche
                    partnerTypeExist = ogpFile.CheckControlStatePT("tsm_" + p.Cells["dgc_partnerType"].Value.ToString());
                    if (partnerTypeExist == null)
                    {
                        partnerTypeChecked = true;
                    }
                    else 
                    {
                        partnerTypeChecked = (bool)partnerTypeExist;
                    }

                    // si l'élément cliqué est du type du partenaire et que c'est décoché, on masque la ligne et on passe à la prochaine entrée
                    if (!partnerTypeChecked)
                    {
                        p.Visible = false;
                    }
                    else
                    {
                        isPublic = Convert.ToBoolean(p.Cells["dgc_isPublic"].Value);

                        /*
                         * la ligne est masquée si et seulement si :
                         * - la ligne est publique et que "afficher les entrées publiques" est décochée (isPublic = true + "Afficher les entrées publiques" = false)
                         * - la ligne est privée est que "afficher les entrées privées" est décochée (isPublic = false + "Afficher les entrées privées" = false)
                         */

                        switch (isPublic)
                        {
                            case true:
                                p.Visible = showPublicData;
                                break;
                            case false:
                                p.Visible = showPrivateData;
                                break;
                        }
                    }

                    // selon si la ligne est masquée ou non, on incrémente le compteur des entrées masquées
                    if (!p.Visible)
                    {
                        visibleCount++;
                    }
                }
                VisibleRows(visibleCount);
                EntriesCount();
            }
        }

        /// <summary>
        /// Deny close sort menu after click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tsm_sortRow_DropDownItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            tsm_sortDGV.DropDown.AutoClose = false;
        }

        /// <summary>
        /// Allow close menu only if cursor is not above
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tsm_sortRowClose(object sender, EventArgs e)
        {
            tsm_sortDGV.DropDown.AutoClose = true;
        }

        /// <summary>
        /// Defaut sort (partner name)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tsm_sortRowDefault(object sender, EventArgs e)
        {
            dgv_partners.Sort(dgv_partners.Columns["dgc_name"], ListSortDirection.Ascending);
            TsmPartnersTypes();
            tsm_sortRowClose(sender, e);
        }

        /// <summary>
        /// Manage types of partners
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tsm_manageTypes_Click(object sender, EventArgs e)
        {
            FormPartnerType pt = new FormPartnerType(this);

            pt.ShowDialog();
        }

        /// <summary>
        /// If user press DEL key
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dg_partnersList_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Delete)
            {
                RemovePartner(sender, e);
            }
        }

        /// <summary>
        /// Show "About this"
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tsm_aboutThis_Click(object sender, EventArgs e)
        {
            new FormAboutThis().ShowDialog();
        }

        /// <summary>
        /// Show "Options"
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tsm_options_Click(object sender, EventArgs e)
        {
            new FormOptions(this).ShowDialog();
        }

        /// <summary>
        /// Clear search bar if hover
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tst_search_MouseEnter(object sender, EventArgs e)
        {
            if (tst_search.Text == "Que souhaitez-vous chercher ?")
            {
                tst_search.Clear();
                tst_search.Font = new Font(tst_search.Font.FontFamily, 9, FontStyle.Italic);
                tst_search.BackColor = Color.FromArgb(192, 255, 255);
                tst_search.ForeColor = Color.Black;
            }
        }

        /// <summary>
        /// Default comportment of search bar
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tst_search_MouseLeave(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(tst_search.Text) && !tst_search.Focused)
            {
                tst_search.Text = opt.PlaceHolder_searchBar;
                tst_search.Font = new Font(tst_search.Font.FontFamily, 9, FontStyle.Italic);
                tst_search.BackColor = SystemColors.Window;
                tst_search.ForeColor = Color.Gray;
            }
        }

        /// <summary>
        /// Default behavior of search bar, when input cursor is absent
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tst_search_Leave(object sender, EventArgs e)
        {
            tst_search_MouseLeave(sender, e);
        }

        /// <summary>
        /// Event for search in partners
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tst_search_KeyUp(object sender, KeyEventArgs e)
        {
            ManageDgv();
        }

        /// <summary>
        /// Export in PDF
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tsm_pdf_Click(object sender, EventArgs e)
        {
            if (dgv_partners.SortedColumn != null)
            {
                opt.Dgv_orderBy = dgv_partners.SortedColumn.HeaderText;
                opt.Dgv_orderByASC = dgv_partners.SortOrder;
            }

            FormPDF f = new FormPDF(this, tst_search.Text);

            f.ShowDialog();
        }

        /// <summary>
        /// Set order of DGV
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgv_partners_ColumnHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            opt.Dgv_orderBy = ((DataGridView) sender).SortedColumn.HeaderText;
            opt.Dgv_orderByASC = ((DataGridView)sender).SortOrder;

            orderBy_columnName = comm.DataGridViewColumnName_By_HeaderText(dgv_partners, opt.Dgv_orderBy);
        }

        /// <summary>
        /// Creates a new file or open an another
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tsm_newOrOpenFile_Click(object sender, EventArgs e)
        {
            bool fileExist = File.Exists(sender.ToString());
            string fileName = "";

            if (fileExist)
            {
                fileName = sender.ToString();
            }

            DialogResult dr = System.Windows.Forms.DialogResult.Cancel;

            if (defaultFileName != "" && OgpFile.MainList_partner.Count > 0)
            {
                // demande de sauvegarde du fichier actuel
                dr = MessageBox.Show("Voulez-vous sauvegarder le fichier actuel ?", "", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);

                if (dr == System.Windows.Forms.DialogResult.Yes)
                {
                    if (saveFileDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                    {
                        SaveFiles(saveFileDialog.FileName);
                    }
                }
            }

            // si l'utilisateur a pas annulé l'opération
            if (
                dr != System.Windows.Forms.DialogResult.Cancel ||
                ((ToolStripMenuItem)sender).Text == "Ouvrir un fichier" ||
                fileExist
                )
            {
                // nettoyage des variables - "nouveau fichier"
                ogpFile.MainList_partner.Clear();
                dgv_partners.Rows.Clear();
                tsm_sortDGV.DropDownItems.Clear();

                // ouverture d'un autre fichier si l'utilisateur a cliqué sur le bon ToolStripMenuItem
                if (((ToolStripMenuItem)sender).Text != "Nouveau fichier")
                {
                    tslb_partnersCount.Visible = false;
                    tslb_elementsVisible.Visible = false;
                    tsm_sortDGV.Enabled = false;

                    dr = System.Windows.Forms.DialogResult.OK;

                    if (!fileExist)
                    {
                        dr = openFileDialog.ShowDialog();
                        fileName = openFileDialog.FileName;
                    }

                    if (dr == DialogResult.OK)
                    {
                        try
                        {
                            // on charge le document pour vérifier sa version
                            XmlDocument doc = new XmlDocument();
                            doc.Load(fileName);

                            XmlNode ogepartNode = doc.SelectSingleNode(@"/OgepartFile");

                            // cas où le fichier est trop ancien (première version du fichier, sans datation)
                            if (ogepartNode == null)
                            {
                                string path = Application.StartupPath + "\\config.xml";

                                /*
                                 * données déplacées vers OgepartFile :
                                 * 
                                 * avant déplacement                => après déplacement
                                 * 
                                 * FormList mainlist_Partner        => ogpFile.MainList_partner (fichier *.ogepart)
                                 * Options listPartnersTypes        => ogpFile.ListPartnersTypes (fichier config.xml)
                                 * Options listControlPartnersSort  => ogpFile.ListControlPartnersSort (fichier config.xml)
                                 * 
                                 */
                                
                                // mainList_partner
                                ogpFile.MainList_partner = Commons.LoadFile(typeof(List<Partner>), fileName, ogpFile.MainList_partner) as List<Partner>;

                                // listPartnersTypes
                                ogpFile.ListPartnersTypes = Commons.ConvertFromOldVersion(
                                    typeof(List<PartnerType>), 
                                    ogpFile.ListPartnersTypes,
                                    Commons.ExtractXmlNode("ListPartnersTypes", path, typeof(List<PartnerType>))
                                    ) as List<PartnerType>;

                                // listControlPartnersSort
                                ogpFile.ListControlPartnersSort = Commons.ConvertFromOldVersion(
                                    typeof(List<DictCheckedTypes>),
                                    ogpFile.ListControlPartnersSort,
                                    Commons.ExtractXmlNode("ListControlPartnersSort", path, typeof(List<DictCheckedTypes>))
                                    ) as List<DictCheckedTypes>;
                            }
                            else
                            {
                                // on regarde la version du fichier
                                string v = ogepartNode.Attributes["Version"].Value;

                                if (v == "1.0.0.1")
                                {
                                    ogpFile = Commons.LoadFile(typeof(OgepartFile), fileName, ogpFile) as OgepartFile;
                                }
                            }

                            defaultFileName = fileName;

                            titleForm(defaultFileName);

                            tslb_partnersCount.Visible = true;
                            tslb_elementsVisible.Visible = true;
                            tsm_sortDGV.Enabled = true;

                            TsmPartnersTypes();
                            ManageDgv();
                            EntriesCount();
                        }
                        catch (Exception err)
                        {
                            MessageBox.Show(
                                "Erreur lors du chargement du fichier XML :\n" +
                                err.InnerException.Message
                                ,
                                ""
                                ,
                                MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1
                                );
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Save file
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tsm_saveList_Click(object sender, EventArgs e)
        {
            if (saveNewFile.ShowDialog() == DialogResult.OK)
            {
                if (defaultFileName == "")
                {
                    defaultFileName = saveNewFile.FileName;
                }

                SaveFiles(defaultFileName);
            }
        }

        #endregion

        #region Misc

        /// <summary>
        /// Manage recents files (adding, ...)
        /// </summary>
        /// <param name="fileName"></param>
        private void ManageRecentsFiles(string fileName = "")
        {
            // ajout du nom de fichier dans la liste
            if (
                fileName != "" && 
                !opt.RecentsFiles.Exists(x => x == defaultFileName)
                )
            {
                opt.RecentsFiles.Add(fileName);
            }

            // on parcourt la liste des fichiers récents dans la classe Options
            for (int i = 0; i < opt.RecentsFiles.Count; i++)
            {
                if (i <= Options.MaxRecentsFiles)
                {
                    ToolStripMenuItem tsm_file = new ToolStripMenuItem();
                    tsm_file.Text = opt.RecentsFiles[i];
                    tsm_file.Name = "tsm_recentsFiles" + i;
                    tsm_file.Visible = true;

                    tsm_file.Click += new EventHandler(tsm_newOrOpenFile_Click);

                    tsm_recentsFiles.DropDownItems.Add(tsm_file);
                }
                else
                {
                    opt.RecentsFiles.RemoveAt(i);
                }
            }

            // si la liste n'est pas vide, retrait de la mention "(aucun fichier récent)"
            if (tsm_recentsFiles.DropDownItems.Count > 1)
            {
                tsm_emptyRecentsFiles.Visible = false;
            }
        }

        /// <summary>
        /// Saves conf file and list
        /// </summary>
        /// <param name="path"></param>
        private void SaveFiles(string path = "")
        {
            string fileName = defaultFileName;
            string ext = "";

            if (path != "")
            {
                fileName = path;
            }

            // liste
            if (!fileName.Contains(Options.ExtensionFile))
            {
                ext = Options.ExtensionFile;
            }

            Commons.SaveInFile(typeof(OgepartFile), fileName, ogpFile, ext);

            titleForm(fileName);

            ManageRecentsFiles(fileName);

            // fichier de config
            Commons.SaveInFile(typeof(Options), Application.StartupPath + "\\" + "config", opt, "xml");
        }

        #endregion
    }
}
