﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using System.Drawing;

namespace Prospection
{
    public partial class FormPartnerType : Form
    {
        private FormList refInstance = null;
        private int indexModifyType = -1; // uniquement si un type est en cours de modification

        #region Form

        public FormPartnerType(FormList refInst)
        {
            InitializeComponent();

            this.refInstance = refInst;

            RefreshComboBox();
            EnableListType();
        }

        /// <summary>
        /// Refreshs list of partnersTypes (FormList)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FormPartnerType_FormClosing(object sender, FormClosingEventArgs e)
        {
            refInstance.TsmPartnersTypes();
            refInstance.RefreshDGV();
        }

        #endregion

        #region Buttons

        /// <summary>
        /// Show colors palette
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_color_Click(object sender, EventArgs e)
        {
            if (colorDialog_partnerType.ShowDialog() == DialogResult.OK)
            {
                Color background = colorDialog_partnerType.Color;

                tb_nameType.BackColor = background;
                tb_nameType.ForeColor = Commons.ForeColorFromBackColor(background);
            }
        }

        /// <summary>
        /// Add or modify a type
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_addOrModify_Click(object sender, EventArgs e)
        {
            int index = -1;

            if (indexModifyType >= 0) // modification
            {
                PartnerType pt = refInstance.comm.SelectPartnerType(cb_listType.SelectedItem.ToString());
                index = refInstance.OgpFile.ListPartnersTypes.IndexOf(pt);

                if (index > -1)
                {
                    pt.Name = tb_nameType.Text;
                    pt.Backcolor = tb_nameType.BackColor.ToArgb();
                    pt.Forecolor = tb_nameType.ForeColor.ToArgb();

                    refInstance.OgpFile.ListPartnersTypes[index] = pt;
                }
                
                indexModifyType = -1;
            }
            else // ajout
            {
                refInstance.comm.AddPartnerType(new PartnerType(tb_nameType.Text, tb_nameType.BackColor.ToArgb(), tb_nameType.ForeColor.ToArgb()));
            }

            RefreshComboBox();
            EnableListType();
        }

        #endregion

        #region Manage methods

        /// <summary>
        /// Refreshs list in combobox
        /// </summary>
        private void RefreshComboBox()
        {
            cb_listType.Items.Clear();

            foreach (PartnerType p in refInstance.OgpFile.ListPartnersTypes)
            {
                cb_listType.Items.Add(p.Name);
            }

            if (cb_listType.Items.Count > 0)
            {
                cb_listType.SelectedIndex = 0;
            }
        }

        /// <summary>
        /// Enable combobox
        /// </summary>
        private void EnableListType()
        {
            if (cb_listType.Items.Count > 0)
            {
                btn_modify.Enabled = true;
                btn_delete.Enabled = true;
            }
            else
            {
                btn_modify.Enabled = false;
                btn_delete.Enabled = false;

                lb_preview.BackColor = Color.White;
                lb_preview.ForeColor = Color.Black;
            }

            btn_add.Enabled = true;
            cb_listType.Enabled = true;

            tb_nameType.BackColor = Color.White;
            tb_nameType.ForeColor = Color.Black;
            tb_nameType.Enabled = false;
            tb_nameType.Clear();
            btn_color.Enabled = false;
            btn_addOrModify.Enabled = false;
            btn_cancelMod.Enabled = false;
        }

        /// <summary>
        /// Allow modification
        /// </summary>
        private void EnableModification()
        {
            btn_add.Enabled = false;
            btn_modify.Enabled = false;
            btn_delete.Enabled = false;
            cb_listType.Enabled = false;

            tb_nameType.Enabled = true;
            tb_nameType.Focus();

            btn_color.Enabled = true;
            btn_addOrModify.Enabled = true;
            btn_cancelMod.Enabled = true;
        }

        #endregion

        #region Events

        /// <summary>
        /// Change selected index for preview
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cb_listType_SelectedIndexChanged(object sender, EventArgs e)
        {
            PartnerType p = refInstance.comm.SelectPartnerType(((ComboBox)sender).SelectedItem.ToString());
            lb_preview.BackColor = Color.FromArgb(p.Backcolor);
            lb_preview.ForeColor = Color.FromArgb(p.Forecolor);
        }

        /// <summary>
        /// Allow add type
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_add_Click(object sender, EventArgs e)
        {
            EnableModification();
        }

        /// <summary>
        /// Cancel modification
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_cancelMod_Click(object sender, EventArgs e)
        {
            EnableListType();
        }
        /// <summary>
        /// Preparation for modification
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_modify_Click(object sender, EventArgs e)
        {
            tb_nameType.Text = cb_listType.SelectedItem.ToString();
            tb_nameType.BackColor = lb_preview.BackColor;
            tb_nameType.ForeColor = lb_preview.ForeColor;
            tb_nameType.SelectionStart = 0;

            EnableModification();

            indexModifyType = cb_listType.SelectedIndex;
        }

        /// <summary>
        /// Delete a type of partners
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_delete_Click(object sender, EventArgs e)
        {
            string name = "";

            DialogResult r = MessageBox.Show(
                "Voulez-vous vraiment supprimer ce type de partenaire ?",
                "",
                MessageBoxButtons.YesNo,
                MessageBoxIcon.Question
                );

            if (r == DialogResult.Yes)
            {
                name = cb_listType.Text;

                refInstance.OgpFile.ListPartnersTypes.Remove(refInstance.comm.SelectPartnerType(name));
                cb_listType.Items.Remove(name);
                RefreshComboBox();
                EnableListType();
            }
        }

        #endregion
    }
}
