﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Prospection
{
    public static class Tests
    {
        public static void T_filterTel()
        {
            char sep = ' ';
            string natTel = string.Format("01{0}02{0}03{0}04{0}05", sep);
            string intTel = string.Format("+331{0}02{0}03{0}04{0}05", sep);
            string finalTel = "";
            string[] telTests = { "0102030405", "01.02+03/04*05", "01 02 03 04 05", "01_02_03_04_05", "Tel : 0102030405", "+Fax : 0102030405", "Tel 0102030405 + Fax 0102030405" };

            foreach (string t in telTests)
            {
                finalTel = Commons.FilterTel(t, sep);

                System.Windows.Forms.MessageBox.Show(
                    Equals(finalTel, natTel) + "\n" +
                    "Entrée : " + t + "\n" +

                    "Sortie : " + finalTel + "\n" +

                    "Format attendu : \n" +
                    natTel + "\n" +
                    intTel
                    );
            }
        }

        public static void T_controlCheckByOpt(DataGridView dgv, ToolStripMenuItem tsm, List<DictCheckedTypes> dict, FormList refInstance)
        {
            bool partnerTypeChecked_TSM = true;
            bool partnerTypeChecked_OPT = true;

            bool showPublicData_TSM = true;
            bool showPublicData_OPT = true;

            bool showPrivateData_TSM = false;
            bool showPrivateData_OPT = false;

            List<string> errors = new List<string>();
            int success = 0;

            foreach (DataGridViewRow d in dgv.Rows)
            {
                for (int i = 0; i < tsm.DropDownItems.Count; i++)
                {
                    if (tsm.DropDownItems[i].Text == d.Cells["dgc_partnerType"].Value.ToString())
                    {
                        partnerTypeChecked_TSM = ((ToolStripMenuItem)tsm.DropDownItems[i]).Checked;
                        break;
                    }

                    switch (tsm.DropDownItems[i].Text)
                    {
                        case "Afficher les entrées publiques":
                            showPublicData_TSM = ((ToolStripMenuItem)tsm.DropDownItems[i]).Checked;
                            break;

                        case "Afficher les entrées privées":
                            showPrivateData_TSM = ((ToolStripMenuItem)tsm.DropDownItems[i]).Checked;
                            break;
                    }
                }

                partnerTypeChecked_OPT = (bool)refInstance.opt.CheckControlStatePT("tsm_" + d.Cells["dgc_partnerType"].Value.ToString());
                showPublicData_OPT = (bool)refInstance.opt.CheckControlStatePT("tsm_showPublicData");
                showPrivateData_OPT = (bool)refInstance.opt.CheckControlStatePT("tsm_showPrivateData");

                if (
                    (partnerTypeChecked_TSM == partnerTypeChecked_OPT) &&
                    (showPublicData_TSM == showPublicData_OPT) &&
                    (showPrivateData_TSM == showPrivateData_OPT)
                    )
                {
                    success++;
                }
                else
                {
                    errors.Add(d.Cells["dgc_partnerType"].Value.ToString());
                }
            }

            MessageBox.Show(
                "Succès : " + success.ToString() + "\n" +
                "Erreurs : " + errors.Count.ToString() + "\n"
                );
        }
    }
}
