﻿using MigraDoc.DocumentObjectModel;
using PdfSharp;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Prospection
{
    [Serializable]
    public class PdfProperties
    {
        private string title = "";
        
        private string picturePath = "";
        private double pictureHeight_Cm = 0;

        private bool isGrouped = false;
        private string groupBy_value = "";

        private double topMargin = 0;
        private double bottomMargin = 0;
        private double leftMargin = 0;
        private double rightMargin = 0;

        private PageFormat pageFormat;
        private PageOrientation orientation;
        private double heightPage = 0;
        private double widthPage = 0;

        private bool horizontalCentering = false;

        private FontPropertiesPdf fontHeaderProps = new FontPropertiesPdf();
        private int foreColorHeader = 0;
        private int backColorHeader = 0;

        private List<ColumnsProp> colsNames = new List<ColumnsProp>();

        private FontPropertiesPdf fontCellProps = new FontPropertiesPdf();

        #region Accesseurs

        // Misc ///////////////////////////////////////////////////////////////////
        [XmlIgnore]
        public string Title
        {
            get { return title; }
            set { title = value; }
        }

        [XmlIgnore]
        public string PicturePath
        {
            get { return picturePath; }
            set { picturePath = value; }
        }

        [XmlIgnore]
        public double PictureHeight_Cm
        {
            get { return pictureHeight_Cm; }
            set { pictureHeight_Cm = value; }
        }

        [XmlIgnore]
        public bool IsGrouped
        {
            get { return isGrouped; }
            set { isGrouped = value; }
        }

        // Margins ////////////////////////////////////////////////////////////////
        public double TopMargin
        {
            get { return topMargin; }
            set { topMargin = value; }
        }

        public double BottomMargin
        {
            get { return bottomMargin; }
            set { bottomMargin = value; }
        }

        public double LeftMargin
        {
            get { return leftMargin; }
            set { leftMargin = value; }
        }

        public double RightMargin
        {
            get { return rightMargin; }
            set { rightMargin = value; }
        }

        [XmlIgnore]
        public string GroupBy_value
        {
            get { return groupBy_value; }
            set { groupBy_value = value; }
        }

        // Format /////////////////////////////////////////////////////////////////
        public PageFormat PageFormat
        {
            get { return pageFormat; }
            set { pageFormat = value; }
        }

        public PageOrientation Orientation
        {
            get { return orientation; }
            set { orientation = value; }
        }

        public double HeightPage
        {
            get { return heightPage; }
            set { heightPage = value; }
        }

        public double WidthPage
        {
            get { return widthPage; }
            set { widthPage = value; }
        }

        public bool HorizontalCentering
        {
            get { return horizontalCentering; }
            set { horizontalCentering = value; }
        }


        // Header /////////////////////////////////////////////////////////////////

        public FontPropertiesPdf FontHeaderProps
        {
            get { return fontHeaderProps; }
            set { fontHeaderProps = value; }
        }

        public int ForeColorHeader
        {
            get { return foreColorHeader; }
            set { foreColorHeader = value; }
        }

        public int BackColorHeader
        {
            get { return backColorHeader; }
            set { backColorHeader = value; }
        }

        [XmlIgnore]
        public List<ColumnsProp> ColsNames
        {
            get { return colsNames; }
            set { colsNames = value; }
        }

        // Cell ///////////////////////////////////////////////////////////////////

        public FontPropertiesPdf FontCellProps
        {
            get { return fontCellProps; }
            set { fontCellProps = value; }
        }

        #endregion

        public PdfProperties()
        {
            //Commons.AssemblyDll(this, typeof(PdfProperties));

            pageFormat = MigraDoc.DocumentObjectModel.PageFormat.A4;
            orientation = PageOrientation.Portrait;
        }

        #region Méthodes

        /// <summary>
        /// Check and return property name, by datagrid column name
        /// </summary>
        /// <param name="datagrid_colName"></param>
        /// <returns></returns>
        public string FindColName(string datagrid_colName)
        {
            string r = "";

            foreach (ColumnsProp cp in colsNames)
            {
                if (cp.ColName_DGV == datagrid_colName)
                {
                    r = cp.ColName_XML;
                    break;
                }
            }

            return r;
        }

        #endregion
    }
}
