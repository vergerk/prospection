﻿namespace Prospection
{
    partial class FormAboutThis
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormAboutThis));
            this.rtb_credits = new System.Windows.Forms.RichTextBox();
            this.lb_name = new System.Windows.Forms.Label();
            this.btn_exit = new System.Windows.Forms.Button();
            this.btn_license = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pb_rectangle = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_rectangle)).BeginInit();
            this.SuspendLayout();
            // 
            // rtb_credits
            // 
            this.rtb_credits.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.rtb_credits.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.rtb_credits.Location = new System.Drawing.Point(12, 92);
            this.rtb_credits.Name = "rtb_credits";
            this.rtb_credits.ReadOnly = true;
            this.rtb_credits.Size = new System.Drawing.Size(644, 208);
            this.rtb_credits.TabIndex = 2;
            this.rtb_credits.Text = "";
            this.rtb_credits.LinkClicked += new System.Windows.Forms.LinkClickedEventHandler(this.rtb_credits_LinkClicked);
            // 
            // lb_name
            // 
            this.lb_name.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lb_name.AutoEllipsis = true;
            this.lb_name.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.lb_name.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb_name.Location = new System.Drawing.Point(9, 11);
            this.lb_name.Name = "lb_name";
            this.lb_name.Size = new System.Drawing.Size(580, 65);
            this.lb_name.TabIndex = 3;
            this.lb_name.Text = "lb_name";
            this.lb_name.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // btn_exit
            // 
            this.btn_exit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_exit.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btn_exit.Location = new System.Drawing.Point(581, 306);
            this.btn_exit.Name = "btn_exit";
            this.btn_exit.Size = new System.Drawing.Size(75, 23);
            this.btn_exit.TabIndex = 4;
            this.btn_exit.Text = "OK";
            this.btn_exit.UseVisualStyleBackColor = true;
            this.btn_exit.Click += new System.EventHandler(this.btn_exit_Click);
            // 
            // btn_license
            // 
            this.btn_license.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btn_license.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btn_license.Location = new System.Drawing.Point(13, 306);
            this.btn_license.Name = "btn_license";
            this.btn_license.Size = new System.Drawing.Size(130, 23);
            this.btn_license.TabIndex = 5;
            this.btn_license.Text = "Licence GNU GPL v2";
            this.btn_license.UseVisualStyleBackColor = true;
            this.btn_license.Click += new System.EventHandler(this.btn_license_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.InitialImage = null;
            this.pictureBox1.Location = new System.Drawing.Point(592, 11);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(64, 64);
            this.pictureBox1.TabIndex = 6;
            this.pictureBox1.TabStop = false;
            // 
            // pb_rectangle
            // 
            this.pb_rectangle.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pb_rectangle.BackgroundImage = global::Prospection.Properties.Resources.aboutThis_Pic;
            this.pb_rectangle.InitialImage = null;
            this.pb_rectangle.Location = new System.Drawing.Point(0, 0);
            this.pb_rectangle.Name = "pb_rectangle";
            this.pb_rectangle.Size = new System.Drawing.Size(671, 83);
            this.pb_rectangle.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pb_rectangle.TabIndex = 7;
            this.pb_rectangle.TabStop = false;
            // 
            // FormAboutThis
            // 
            this.AcceptButton = this.btn_exit;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btn_exit;
            this.ClientSize = new System.Drawing.Size(668, 341);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.lb_name);
            this.Controls.Add(this.btn_license);
            this.Controls.Add(this.btn_exit);
            this.Controls.Add(this.rtb_credits);
            this.Controls.Add(this.pb_rectangle);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(684, 379);
            this.Name = "FormAboutThis";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "A propos de ...";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormAboutThis_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_rectangle)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.RichTextBox rtb_credits;
        private System.Windows.Forms.Label lb_name;
        private System.Windows.Forms.Button btn_exit;
        private System.Windows.Forms.Button btn_license;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pb_rectangle;
    }
}