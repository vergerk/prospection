﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prospection
{
    // Remplace le dictionnaire, qui n'est pas sérialisable
    public class DictCheckedTypes
    {
        private string name;
        private bool state;

        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        public bool State
        {
            get { return state; }
            set { state = value; }
        }

        public DictCheckedTypes() { }

        public DictCheckedTypes(string n, bool s)
        {
            name = n;
            state = s;
        }
    }
}
