﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Prospection
{
    [Serializable]
    public class OgepartFile
    {
        //Dictionary<string, Color> partnerType = new Dictionary<string, Color>();; -> pas possible de serialiser
        private List<PartnerType> listPartnersTypes = new List<PartnerType>();
        
        private List<DictCheckedTypes> listControlPartnersSort = new List<DictCheckedTypes>();

        private List<Partner> mainList_partner = new List<Partner>();

        private string version = "1.0.0.1";

        #region Accesseurs

        public List<PartnerType> ListPartnersTypes
        {
            get { return listPartnersTypes; }
            set { listPartnersTypes = value; }
        }

        public List<DictCheckedTypes> ListControlPartnersSort
        {
            get { return listControlPartnersSort; }
            set { listControlPartnersSort = value; }
        }

        public List<Partner> MainList_partner
        {
            get { return mainList_partner; }
            set { mainList_partner = value; }
        }

        [XmlAttribute()]
        public string Version
        {
            get { return version; }
            set { version = value; }
        }

        #endregion

        public OgepartFile() {}

        #region Méthodes

        /// <summary>
        /// Add ToolStripMenuItem control (fullname & checked state only) in listControlPartnersSort
        /// </summary>
        /// <param name="name"></param>
        /// <param name="state"></param>
        public void AddControlPT(string fullname, bool state)
        {
            listControlPartnersSort.Add(new DictCheckedTypes(fullname, state));
        }

        /// <summary>
        /// Update saved control in list
        /// </summary>
        /// <param name="name"></param>
        /// <param name="state"></param>
        public void ModifyState(string fullname, bool state)
        {
            foreach (DictCheckedTypes d in listControlPartnersSort)
            {
                if (d.Name == fullname)
                {
                    d.State = state;
                    break;
                }
            }
        }

        /// <summary>
        /// Returns state of a control
        /// </summary>
        /// <param name="fullname"></param>
        /// <returns></returns>
        public bool? CheckControlStatePT(string fullname)
        {
            bool? r = null;

            foreach (DictCheckedTypes d in listControlPartnersSort)
            {
                if (d.Name == fullname)
                {
                    r = d.State;
                    break;
                }
            }

            return r;
        }

        #endregion
    }
}
