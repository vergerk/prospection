﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prospection
{
    public class Partner
    {
        private string namePartner;
        private string idPartner;

        private string partnerType;
        private List<ContactList_partner> listContact = new List<ContactList_partner>();

        #region Accesseurs

        public string NamePartner
        {
            get { return namePartner; }
            set { namePartner = value; }
        }

        public string IdPartner
        {
            get { return idPartner; }
            set { idPartner = value; }
        }

        public string PartnerType
        {
            get { return partnerType; }
            set { partnerType = value; }
        }

        public List<ContactList_partner> ListContact
        {
            get { return listContact; }
            set { listContact = value; }
        }

        #endregion

        #region Constructeurs

        public Partner()
        {
            idPartner = "";
            namePartner = "";
            partnerType = "";
            listContact = null;
        }
        
        public Partner(string namePartner, string partnerType, List<ContactList_partner> listP)
        {
            idPartner = Commons.RandomId();
            this.namePartner = namePartner;
            this.partnerType = partnerType;
            this.listContact = listP;
        }

        public Partner(string idPartner, string namePartner, string partnerType)
        {
            this.idPartner = idPartner;
            this.namePartner = namePartner;
            this.partnerType = partnerType;
        }

        public Partner(string idPartner, string namePartner, string partnerType, List<ContactList_partner> listP)
        {
            this.idPartner = idPartner;
            this.namePartner = namePartner;
            this.partnerType = partnerType;
            this.listContact = listP;
        }
        
        #endregion 

        /// <summary>
        /// Add a list of ContactList_partner in Partner
        /// </summary>
        /// <param name="list"></param>
        public void AddList(List<ContactList_partner> list)
        {
            listContact = list;
        }

        /// <summary>
        /// Returns a list with partner data and ContactList_partner, by listContactIndex
        /// </summary>
        /// <param name="listContactIndex"></param>
        /// <returns></returns>
        public List<string> ArrayPartner(int listContactIndex)
        {
            ContactList_partner cl = listContact[listContactIndex];
            List<string> l = new List<string>();

            l.Add(namePartner);
            l.Add(partnerType);
            l.Add(cl.Contact);
            l.Add(cl.Comment);
            l.Add(cl.Tel);
            l.Add(cl.Email);
            l.Add(cl.Address);

            return l;
        }
    }
}
