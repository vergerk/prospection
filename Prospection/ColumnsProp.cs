﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prospection
{
    public class ColumnsProp
    {
        int i;
        string colName_DGV;
        string colName_FR;
        string colName_XML;
        double colWidth;

        #region Accesseurs

        public int I
        {
            get { return i; }
        }

        public string ColName_DGV
        {
            get { return colName_DGV; }
        }

        public string ColName_FR
        {
            get { return colName_FR; }
        }

        public string ColName_XML
        {
            get { return colName_XML; }
        }

        public double ColWidth
        {
            get { return colWidth; }
        }

        #endregion

        public ColumnsProp()
        {
            this.i = 0;
            this.colName_DGV = "";
            this.colName_FR = "";
            this.colName_XML = "";
            this.colWidth = 0;
        }

        public ColumnsProp(int i, string colName_DGV, string colName_FR, string colName_XML, double colWidth)
        {
            this.i = i;
            this.colName_DGV = colName_DGV;
            this.colName_FR = colName_FR;
            this.colName_XML = colName_XML;
            this.colWidth = colWidth;
        }
    }
}
