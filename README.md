OGePart (pour Outil de Gestion des Partenaires) permet de gérer une liste de partenaires ainsi que d'exporter ces données dans un fichier PDF.
L'utilisateur peut saisir des partenaires et effectuer un classement par type de partenaire, par type d'entrées et rechercher un terme précis.
L'export se base sur les données affichées à l'utilisateur mais aussi le formatage des cellules, la mise en page sélectionné, l'ordre des colonnes cochées et les documents qui précèdent/suivent les données exportées.

Exemple d'exportation : http://www.vergerk.fr/docs/ogepart_resultatExport.pdf

PDFsharp / Migradoc 1.32 (empira Software GmbH) sous licence MIT : http://www.pdfsharp.net/Licensing.ashx
Connecteur/ADO.Net MySQL (Oracle) sous licence GPL : https://dev.mysql.com/downloads/connector/net/

Licence GNU GPL 2 : https://www.gnu.org/licenses/old-licenses/gpl-2.0.html